# Analysis of Environmental Treaty Design: A Data Science Approach
## PhD reproducibility archive

This is the code and data repository of my PhD research on international environmental agreements at the University of Cambridge's [Centre for Environment, Energy and Natural Resource Governance](http://www.ceenrg.landecon.cam.ac.uk/). A zip archive of the final version and a container image with additional sources are available from Zenodo at https://doi.org/10.5281/zenodo.10078710. 

The thesis source file (`thesis.org`) uses the literate programming paradigm implemented in [Emacs Orgmode](https://orgmode.org/) and contains much of the code used in this research project. Tables and figures are generated in place to avoid any copy-pasting errors. To facilitate reuse, I have also extracted the main components and included them in the `materials` directory. Additionally, to encourage incremental development and collaboration, I have published several key tools separately in the [legal informatics workspace](https://gitlab.com/legalinformatics) on Gitlab with documentation on installation and customization. The README files and usage samples for those tools are not included in this archive. In case of interest, please visit the links added to the outline below (URLs are subject to change depending on the offer of the hosting provider and the needs of PhD spin-out projects).

The contents of this repository are:
- `thesis`: thesis source files and pdf
- `materials`:
  - `treaty-selection`: scripts and data used for treaty sample selection (Chapter 2.2 and Appendix C)
  - `data-collection`: custom bots for treaty data collection
    - `unts-searchbot` for automated search and retrieval of UNTS index data, see https://gitlab.com/legalinformatics/unts-searchbot
    - `unts-crawler` for collecting information contained in UNTS treaty pages, see https://gitlab.com/legalinformatics/unts-crawler
    - `austlii-scraper` for fetching treaty texts from three official Australian treaty series, see https://gitlab.com/legalinformatics/austlii-scraper
    - `ieadb-scraper` for gathering treaty texts from the University of Oregon's International Environmental Agreements Database, see https://gitlab.com/legalinformatics/ieadb-scraper
  - `data`: treaty texts and UNTS treaty records
    - `treatytexts`:
      - `scraped`: treaty text files exactly as saved by custom bots
      - `cleaned`: treaty text files exactly as cleaned by cleaning script
      - `treaty_cleaner.py`: treaty text cleaning script
    - `unts-treatyrecords`: UNTS treaty records exactly as saved to file by the `unts-crawler`
  - `pilo`: data integration tool, see Appendix E and https://gitlab.com/legalinformatics/pilo
  - `java-pipeline`:
     - `UNTStreatyrecordsIE.7z`:[^1] see https://gitlab.com/legalinformatics/unts-treatyrecords-ie
     - `GATE-treatytextsIE.7z`: see https://gitlab.com/legalinformatics/treatytexts-ie
  - `treatytextsIE-python`: code and data for Chapters 3 and 4
  - `treaty-references`: see https://gitlab.com/martinakunz/treaty-references
  - `treaty-profiles`: source code for treaty profile in Appendix G with additional examples
  - `treaty-EIF-predictions`: source code for analysis in Chapter 5, adapted from Appendix H

In addition, a number of files in this directory support computational reproducibility:
- `Pipfile` specifies Python dependencies and `Pipfile.lock` enables deterministic builds (the exact Python packages installed in the virtual environment) -- to be used with [pipenv](https://pypi.org/project/pipenv/)
- `requirements.txt`: output of `pipenv requirements`, for use with `pip install`
- `Dockerfile` and `.dockerignore`: Instructions for building a Docker/Podman image[^2]
- `replication.sh`: bash script to replicate core parts of the data science pipeline

The two versions of this PhD repository archived on [Zenodo](https://doi.org/10.5281/zenodo.10078710) are a ZIP file produced by `git archive -o mkphd.zip HEAD`, and a [Podman](https://podman.io/) image (compatible with Docker) produced with Podman version 3.0.1 from the Debian package manager. The Podman image contains additional third-party software necessary for end-to-end reproducibility. Due to small differences between the PyPI and Debian versions of Matplotlib, only the Debian version produces the exact same figures as those appearing in the submitted thesis. Thus, the Dockerfile installs `python3-matplotlib` for the Debian distribution, but if exact replication is not needed, installing missing dependencies into an existing system with another package manager should suffice. The Podman image was built and saved for upload to Zenodo with the commands:[^3]

~~~sh
podman build --tag mkphd .
podman save mkphd | gzip > mkphd_image.tar.gz
~~~

`mkphd_image.tar.gz` can be downloaded from Zenodo and loaded into a podman installation with:

~~~sh
podman load --input mkphd_image.tar.gz
~~~

A container can be run interactively with:

~~~sh
podman run -it --name mkphd_container mkphd
~~~

This opens a bash shell, where individual components can be reproduced following instructions below, or the high-level replication script can be run with:

~~~sh
bash replication.sh
~~~

Code execution times were measured on a laptop with a quad core Intel i7 CPU, GTX 970M NVIDIA GPU, and 32GB RAM (but the GPU is not strictly necessary and less RAM should work as well). Most parts of the pipeline take no more than a few seconds to run. Only execution times of over a minute are mentioned hereafter. The `replication.sh` script completes in about 3 minutes 20 seconds.

For more explanation, see the sections below:

1. [Treaty selection](#treaty-selection)
2. [Data collection and cleaning](#data-collection-and-cleaning)
3. [Information extraction and integration](#information-extraction-and-integration)
4. [Treaty referencing](#treaty-referencing)
5. [Treaty profiles](#treaty-profiles)
6. [Entry into force predictions](#entry-into-force-predictions)
7. [Thesis figures and tables](#thesis-figures-and-tables)

## Treaty selection
The process is described in Chapter 2.2 of the thesis and the relevant files can be found in the directory `treaty-selection`.

The selection starts from the full version of the University of Oregon's International Environmental Agreements Database (IEAdb) version 2018.1. The publishers do not provide a version-controlled download option, so this part of the data collection is not automated. The CSV file `treaties_full.csv` is exactly as downloaded from the IEAdb [website](https://iea.uoregon.edu/base-agreement-list/csv-all?attach=page) on 13 September 2019. The script `treaty_selection.py` narrows down the list from 5665 to 319 agreements and saves the result to the file `treaties_subset.csv` (removing spaces from column names for better code readability). This in turn provides the basis for a manual selection process, which also involved adding more agreements to the list.

The selection script can be run in an interactive session or on the command line with:

~~~sh
cd treaty-selection
python3 treaty_selection.py
~~~

The spreadsheet `IEA_Dataset_Agreements_332_MK.csv` represents the output of the manual selection process and contains additional information used for the analysis in Chapter 2 and for downstream processing. Specifically, the columns I added are the following:

- `shortTitle` and `treatyLabel` set out abbreviated versions of the treaty title for selected treaties, to generate more compact tables and graphs
- `selected` specifies whether the treaty is selected for analysis or not (`True` or `False`)
- `reason` specifies a reason for exclusion for all treaties that were not selected
- columns `nonTreatyEvidence`, `isAmendingAgreement`, `isAnnex`, `participation`, `geo_scope`, `species_scope`, `envFocus`, `envRelevance`, `healthFocus`, `econFocus`, `topic`, and `comment` provide further information regarding the selection decision, but note that this is far from exhaustive, it is merely an attempt to come up with categorical variables that can support the decision-making process; the final decision was based on detailed research notes that are available upon request
  - the ordinal variables `envFocus`, `envRelevance`, `healthFocus`, and `econFocus` are graded on a scale from 0-5 with 5 as the maximum
  - `comment` is a free-form comment about selection or comparison with other classification schemes
- `envtlDiscrepancy` is `True` if there is a difference between my selection (`selected==True`) and the environmental classification of the IEAdb (`Inclusion` not set to 'MEA' or 'MEN'), the [Ecolex](https://www.ecolex.org) index (`Ecolex==False`), or Scott Barrett's list of multilateral environmental agreements in his 2007 book _Environment and Statecraft_ (columns starting with 'SB_' are empty) -- this is not thorough assessment, only what was useful for Chapter 2
- `Ecolex` is set to `True` if the treaty can be found in the Ecolex database, and `Ecolex_global` is `True` if it has its Ecolex 'Field of application' set to [Global](https://www.ecolex.org/result/?q=&type=treaty&xdate_min=&xdate_max=&tr_field_of_application=Global)
- The five columns starting with 'SB_' contain information from Scott Barret's abovementioned book, in particular:
  - `SB_theme`: the theme assigned to the treaty
  - `SB_open`: the column 'Membership open (O) or restricted (R)'
  - `SB_minRatif`: 'Minimum no. of ratifications'
  - `SB_tradeRestr`: 'Provisions for trade restrictions'
  - `SB_adminOrg`: 'Created an administrative organization'
- The final 9 columns facilitate the identification and referencing of the treaties, as well as text selection for those available in more than one database, as follows:
  - `UNTStreatyRecordURL`, `IEAdbUrl` and `AustLIIurl`: relevant treaty URLs
  - `IEAdbFn` and `AustLIIfn`: file names of downloaded agreement texts
  - `IEAdbProblem` and `AustLIIproblem`: reasons for disregarding the source in question
  - `ILMref` and `OrgRef`: reference to publication in _International Legal Materials_ or an organizational document series for selected treaties not included in UNTS

This manually edited file becomes the input to the treaty URL list generation script, which saves the lists into the treaty data harvester folders. Three files are generated with this shell command:

~~~sh
python3 generate_treaty_urls.py
~~~


## Data collection and cleaning

UNTS metadata and treaty texts were collected with the four custom scrapers in the `data-collection` directory. However, as URLs and web content may have changed since the time of data collection, I can only guarantee reproducibility of my PhD project starting from scraped data (saved in the `data` folder).

That said, I plan to continue maintaining and updating these treaty data collection tools beyond the PhD as they will certainly continue to be useful to me and perhaps to others as well, for as long as source data export options are limited. The links to the continually updated tools are included in the repository outline above.

Thesis Appendices B and D briefly introduce the data collection bots and include the main scripts. The README files in the external repositories contain a lot more detailed information, instructions and advice for prospective users.

UNTS treaty records saved by the `unts-crawler` are used directly as input for the next stage, whereas treaty texts undergo pre-processing with the `treaty_cleaner.py` script located in the `treatytexts` directory.
If the remote sources had remained the same, the files in the `data` directory could be reproduced with the following commands:

~~~sh
cd data-collection/unts-crawler
scrapy crawl unts -a urlfile=UNTSurls.txt -a targetdir=../../data/unts-treatyrecords/
cd ../austlii-scraper
scrapy crawl austlii
cd ../ieadb-scraper
python3 ieadb_scraper.py
cd ../../data/treatytexts
python3 treaty_cleaner.py
~~~

Of these, the `replication.sh` script only runs the `treaty_cleaner.py` which takes the scraped texts as input, not relying on remote data sources.

Note that the `data` directory only includes the datasets analyzed in the central chapters of the thesis. I also collected UNTS index data on all treaties adopted between June 1945 and June 2022 with the `unts-searchbot` and all multilateral treaty pages for which a URL was found in this index. However, as explained in Appendix B.1, it seems that searching UNTS by treaty adoption month returns incomplete results, possibly missing around 20% of the existing treaty records. This did not matter for the purposes of this doctoral research project since collecting the larger dataset merely served to gauge the scale of the epistemic challenge. Future work will focus on addressing this issue of data access. 


## Information extraction and integration
It may seem a bit confusing to have two different NLP pipelines, one in Python and one in Java/GATE. As explained in the thesis, they are complementary: the Python pipeline is better for fast iteration and immediate data analysis, while GATE is better for visual verification and collaboration with less tech-savvy lawyers. Moreover, I include examples of both in the thesis to show that the regular expressions at the core of them can be specified in much the same way, with minor differences due to the underlying matching engine and the information extraction methods.

UNTS data is only processed by the GATE pipeline, whereas treaty texts are processed by both. The Python pipeline additionally transforms the extracted data for quantitative analysis. Such transformations could also be implemented in the Java pipeline, but this was not deemed necessary for the thesis.

### PILO
The Public International Law Ontology (PILO) is a proof-of-concept data integration tool to combine treaty data from different sources and publish them in a semantic web / linked open data / FAIR data format. Appendix E presents an overview and source code excerpts in Turtle format. The `pilo` directory contains two folders with their respective ontology files: `pilo.ttl` in the `core` folder defines the classes/universals and properties/relations, as well as a few helper instances, whereas the `populated` folder contains a version of PILO that was automatically populated with instance data from UNTS treaty pages and treaty texts with the GATE apps described below (`pilo_phd-treaties.ttl`). 

The core ontology was developed (manually) in the [Protégé](https://protege.stanford.edu/) ontology development software with the Basic Formal Ontology [BFO-2020](https://github.com/BFO-ontology/BFO-2020), also published in [ISO/IEC 21838-2:2021](https://www.iso.org/standard/74572.html), as top-level ontology. It is expressed in the [OWL 2](https://www.w3.org/TR/owl2-primer/) Web Ontology Language, and the RDF syntax chosen is [Turtle](https://www.w3.org/TR/turtle/) for its readability, widespread use and compact size.

### Java pipeline
As explained in Appendix F, there are two GATE apps, each designed to work with a specific type of document, UNTS treaty records in one (`UNTStreatyRecordsIE.7z`) and treaty texts in the other (`GATE-treatytextsIE.7z`). The external repositories referenced in the outline above contain a lot more information; here the focus is primarily on reproducibility.

The materials in the `java-pipeline` directory were produced with a free and open source implementation of the Java programming language and platform, [OpenJDK](https://openjdk.org/) 11, as well as the third party software in the top-level `java` folder (only in the container image, not in the git repository). These prerequisites can be installed from the command line with:

~~~sh
apt update && apt install openjdk-11
java -jar java/gate-developer-9.0.1-installer.jar
~~~

After installation, the two GATE applications can be imported into GATE Developer by extracting the 7zip archives and then clicking on 'File' -> 'Restore Application from File', selecting the respective `application.xgapp` file in the extracted archives. This loads the document corpus and processing resources, as well as the searchable datastore. The applications can be reproduced by double-clicking on the application name to open the menu, and then clicking on 'Run this Application' at the bottom of the screen (see the GATE User Guide or the legal informatics repositories referenced above for more information and screenshots).

The file `urlMap.tsv` in the `java-pipeline` directory contains the mapping between source URLs and treaty identifiers, such that metadata and texts can be linked to the same entity in the ontology population process. The path to this file may need adjusting depending on the directory structure of the user's computer. Any text editor can be used to adjust this path in `UNTSontopop.jape` and `treatyIE.jape` if need be.

Running GATE applications is not automated because GATE is primarily used for its graphical user interface in this project, and automating it would have required additional development effort. As mentioned in the thesis, the GATE treaty text IE app is not equivalent to the Python treaty text IE pipeline. Instead, it showcases the potential of a visually informative NLP pipeline that allows for interdisciplinary collaboration.

The final part of the Java pipeline uses the [Apache Jena ARQ](https://jena.apache.org/documentation/query/cmds.html) command line tool to run a SPARQL query. This step is reproduced in the `replication.sh` bash script with the following commands (executed from within the `java-pipeline` directory):

~~~sh
export JENA_HOME=../../java/apache-jena-3.4.0
$JENA_HOME/bin/sparql --data=pilo_UNTS.ttl --query=UNTSmeta.rq --results=CSV > UNTSmeta.csv
~~~

The tool takes `pilo_UNTS.ttl`, a Turtle file with UNTS metadata procesesd by GATE Developer, and the SPARQL query file `UNTSmeta.rq` as inputs, and produces the CSV file `UNTSmeta.csv` as output.


### Python pipeline
To prepare for treaty text IE, the script `treatydata_prep.py` combines the outputs of treaty selection (`IEA_Dataset_Agreements_332_MK.csv`) and UNTS metadata transformation (`UNTSmeta.csv`) and saves the result as `treatymeta.csv` after further processing. A subset of `treatymeta.csv` is saved as `treatyrefs.csv` (within the `treaty-references` folder) for generating bibliographic data. 

The treaty text IE pipeline is presented in two versions and three formats, to suit different user needs and interests. There is a full version (`treatytextIE.org`) and a minimal version (`treatytextIE_minimal.org`) with executable code blocks and results embedded in an Emacs Orgmode file. The minimal version is also exported to PDF (`treatytextIE_minimal.pdf`) for viewing and as a Python script (`treatytextIE_minimal.py`) for running by computers which have Python but not Emacs installed. The difference between the full and the minimal version is that the full version develops, tests and then saves the regular expressions to the two dataframes `provdf.csv` (for treaty provisions IE) and `smdf.csv` (for subject matter IE), whereas the minimal version merely applies these regular expressions to the treaty texts at hand. Both versions produce `textdf.csv` as the key output analyzed in Chapters 3-5.

The `replication.sh` bash script runs the minimal version, which also displays the time elapsed and whether `textdf` was successfully reproduced upon completion. The minimal version takes about 1 minute to run whereas the full version requires around 25 minutes due to its in-depth search for false negatives and false positives. The relevant commands in the bash script are:

~~~sh
cd ../treatytextsIE-python
python3 treatydata_prep.py
python3 treatytextIE_minimal.py
~~~

To reproduce the full version from the command line, one can use Emacs batch processing, loading the initiation file in the `emacs` subdirectory like so:

~~~sh
cd ../..
emacs --batch --load ./emacs/init.el --visit ./materials/treatytextsIE-python/treatytextIE.org --funcall org-babel-execute-buffer
~~~

As for the PDF of the minimal version, the command to reproduce it is:

~~~sh
emacs --batch --load ./emacs/init.el --visit ./materials/treatytextsIE-python/treatytextIE_minimal.org --funcall org-latex-export-to-pdf
~~~

Note that `init.el` disables confirmation of code evaluation for automated reproducibility, which introduces a security risk if the source is not trustworthy or the files have been tampered with. 


## Treaty referencing
Automating treaty references is a minor code & data output of my PhD research that has already been useful in a number of other projects. A detailed README is available in the separate code repository linked to in the outline above, so I will only mention the reproducibility aspect here. In short, the folder contains two Python scripts, `treatyrefbib.py` generates the bibliographic database `treatyrefs.bib` for use with the [`oscola`](https://www.ctan.org/pkg/oscola) Biblatex package, while `treatyreflist.py` generates two plain text files with OSCOLA-compliant treaty references, one in alphabetical order (`treatyrefs_oscola_alphabetical.txt`) and the other in chronological order (`treatyrefs_oscola_chronological.txt`) for researchers who do not use LaTeX. The scripts can be run on the command line with:

~~~sh
cd materials/treaty-references
python3 treatyrefbib.py
python3 treatyreflist.py
~~~

Both scripts use the `treatyrefs.csv` spreadsheet as data input, which is a subset of `treatymeta.csv`, as mentioned above. The purpose of this intermediate dataset is to make the treaty references component more self-contained and thus more easily reusable. 

The bibliographic database is added to `thesis.org` in this header line:

~~~
#+LATEX_HEADER: \addbibresource{thesis.bib}
~~~

The treaty index is generated with this command (as part of a LaTeX export block in `thesis.org`):

~~~tex
\printindexearly[treaties]
~~~


## Treaty profiles
This directory contains the code for the treaty profile included in Appendix G of the thesis. Similar tables occur in the main thesis chapters (e.g. Figure 4.2 on subject matter results for major MEAs). The rationale for adding this appendix and source code folder is that it provides a self-contained example of a compact overview of treaty content and of an automated data-based 'report' in some sense. Due to space constraints, the PDF is generated in A5 format and then embedded in the main thesis PDF, but changing the page size to A4 would only require editing the `LATEX_CLASS_OPTIONS` in the header section from `a5paper` to `a4paper` (in which case portrait format may be better than the current landscape format). More advanced customization would require some knowledge of Emacs Orgmode and its LaTeX export functions, which are well documented [online](https://orgmode.org/manual/LaTeX-Export.html).

The data used to populate the treaty profile is loaded from the CSV files in the `treatytextsIE-python` directory (i.e. the outputs of the Python pipeline). 
Exporting to PDF can be done within Emacs by clicking on Org (with orgmode installed and activated for treaty-profile.org) and then selecting 'Export/Publish...' in the dropdown menu. This opens the Org Export Dispatcher menu, where one would select 'l' for LaTeX followed by 'p' for pdf. Or, more efficiently, the keybindings `Ctrl-c Ctrl-c l p` produce the same result. This can be further automated by invoking Emacs from the command line with:

~~~sh
emacs --batch --load ./emacs/init.el --visit ./materials/treaty-profiles/treaty-profile.org --funcall org-latex-export-to-pdf
~~~

Changing the treaty is fairly simple and makes use of the treaty labels/acronyms used throughout the project. As this label occurs in six places, an Emacs Lisp function is used to replace all the occurrences of the old label with the new label. For instance, changing the targeted treaty from the Paris Agreement to the CBD would require editing the respective function call to read `(replace-string "ParisAgreement" "CBD")` and then running the code with `Ctrl-c Ctrl-c` (while the cursor is within the code block). 


## Entry into force predictions
The main code to reproduce Chapter 5 results can be found in Appendix H. I ran this in the thesis.org file itself, but to facilitate reproduction and reuse, a separate Python file `predict_eif.py` in the `treaty-EIF-predictions` folder contains a merged version of the five code blocks. Putting everything into one script entailed moving all the import statements to the top of the file and converting code block captions to commented lines. Other minor edits include making file paths conform to the directory structure of this code archive. `textdf_chap5.csv` is a subset of a prior version of `textdf.csv` (output of the Python NLP pipeline) that corresponds to the snapshot of the data analyzed for Chapter 5.

Output files saved by the script are:
- `resdf.csv` contains the main results of the nested crossvalidation runs
- `testmlps.csv` stores the feature permutation results for all test MLPs
- `RidgeTestModels.csv` sets out the feature permutation results for all Ridge test models

Training and testing the models takes over 10 hours, so this is not part of the `replication.sh` bash script.


## Thesis figures and tables
As mentioned at the outset, most tables and figures are generated in `thesis.org` itself and the code, its results or both are exported to PDF via LaTeX. This constitutes the last step of the `replication.sh` script, which includes making a copy of the existing `thesis.pdf` file and then comparing the reproduced version with the backup. The relevant commands are as follows:

~~~sh
cp ./thesis/thesis.pdf ./thesis/thesis_backup.pdf
emacs --batch --load ./emacs/init.el --visit ./thesis/thesis.org --funcall org-latex-export-to-pdf
comparepdf --verbose=2 --compare=appearance ./thesis/thesis.pdf ./thesis/thesis_backup.pdf
~~~

The [comparepdf](https://packages.debian.org/bullseye/comparepdf) command on the third line compares the appearance of the two PDF files, detecting even minor differences in fonts and graphics, not just in text (to compare text alone use the `--compare=text` option instead). If a difference is found, the output of the comparison tool is "Files look different" and otherwise it is "No differences detected". To investigate differences, I would recommend using the [diffpdf](https://packages.debian.org/bullseye/diffpdf) tool by the same author.


## Footnotes
[^1]: The two GATE apps are compressed in [7zip](https://7-zip.org/) format instead of ZIP (as produced by the 'Export for GATE Cloud' function in GATE Developer) because this makes the archives small enough to push to Gitlab (< 100 MB) without having to install additional software for large file storage.

[^2]: `Containerfile` and `.containerignore` are the vendor-agnostic equivalents to `Dockerfile` and `.dockerignore`, but the version of Podman I used did not properly support `.containerignore` yet.

[^3]: Podman commands are identical to Docker commands except that `podman` is substituted for `docker`.

import numpy as np
import pandas as pd

# load manually edited dataset
tm = pd.read_csv('IEA_Dataset_Agreements_332_MK.csv', encoding='utf-8').dropna(how='all')

# generate useful columns
tm['AustLIIfn'] = tm.AustLIIurl.str.split("dfat/").str[1].str.replace('/','-', regex=False).str.replace('.html','.txt', regex=False)
tm.IEAdbUrl.fillna('https://iea.uoregon.edu/treaty-text/' + tm.IEAdbFn.str.lower().str.replace('.','', regex=False), inplace=True)
tm['fn'] = tm.IEAdbFn
tm.loc[tm.AustLIIurl != 'x', 'fn'] = tm.AustLIIfn
tm.to_csv('IEA_Dataset_Agreements_332_MK.csv', encoding='utf-8', index=False)

# AustLII URLs
urls = tm.AustLIIurl[(tm.selected == True) & (tm.AustLIIurl != 'x')]
urls.to_csv('../data-collection/austlii-scraper/treatyurls.txt', encoding='utf-8', index=False, header=False)
print(f'Saved {len(urls)} AustLII URLs to file for the austlii-scraper.')

# IEAdb URLs
urls = tm.loc[(tm.AustLIIurl == 'x') & ((tm.selected == True) | tm.fn.str.contains('^1973.+PollutionFromShips', regex=True)), ['fn','IEAdbUrl']]
urls.IEAdbUrl.fillna('https://iea.uoregon.edu/treaty-text/' + urls.fn.str.lower().str.replace('.','', regex=False), inplace=True)
urls = urls.IEAdbUrl
urls.to_csv('../data-collection/ieadb-scraper/ieadb_urls.txt', encoding='utf-8', index=False, header=False)
print(f'Saved {len(urls)} IEAdb URLs to file for the ieadb-scraper.')

# UNTS URLs
urls = tm[tm.selected==True].sort_values(by=['fn']).UNTStreatyRecordURL.dropna()
urls.to_csv('../data-collection/unts-crawler/UNTSurls.txt', encoding='utf-8', index=False, header=False)
print(f'Saved {len(urls)} UNTS URLs to file for the unts-crawler.')

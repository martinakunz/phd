import numpy as np
import pandas as pd

# load the full IEAdb treaty list version 2018.1 (n=5665)
ieadb = pd.read_csv('treaties_full.csv', encoding='utf-8').dropna(how='all')
initnb = len(ieadb)
# cleaning up column names
ieadb.columns = ieadb.columns.str.replace(' ','')
ieadb.rename({"IEA#(clickforadd'linfo)":'ieaid'}, axis='columns', inplace=True)
# removing entries without treaty text
ieadb = ieadb[ieadb.TreatyText.notna()]
# only multilateral agreements
ieadb = ieadb[ieadb.Inclusion.str.startswith('M')]
# only English texts (or not yet classified but likely English)
ieadb = ieadb[ieadb.Lang1.isna() | ieadb.Lang1.str.contains('EN')]
# excluding amendments
ieadb = ieadb[~ieadb.TreatyName.str.contains('(?i)^Amend|^Adjustment|Amending|^(?:\w+ ){,3}Amend')]
# restricted agreements with named parties
pattern = r'(?i)(?:Between|\bGovernment[s]?\b|Kingdom|Republic|Commonwealth|Federation|\WStates\b(?!.+the Moon))'
ieadb = ieadb[~(ieadb.TreatyName.str.contains(pattern)|ieadb.AlternativeTreatyNames.str.contains(pattern))]
pattern = r'(?:German|Kuwait|Norway|Niger|Chad|Congo|Senegal|Gambia|Cambodia|Nauru|Palau)'
ieadb = ieadb[~(ieadb.TreatyName.str.contains(pattern)|ieadb.AlternativeTreatyNames.str.contains(pattern))]
# named inland waters and mountains
pattern = r'(?:Rhine|Elbe|Danube|Meuse|Scheldt|Prespa|Mosel|Oder|Skagerrak|Mekong|Limpopo|Zambezi|Lake|River|Basin|Amazon\w*|Carpathian[s]?|Andean|Alps)\b'
ieadb = ieadb[~(ieadb.TreatyName.str.contains(pattern)|ieadb.AlternativeTreatyNames.str.contains(pattern))]
# regional seas, straits and islands
pattern = r'(?:(?:Aral|Baltic|Barents|Bering|Black|Caspian|North|Red|Wadden) Sea[s]?|Mediterranean|Caribbean|Gulf|Bay|Jan Mayen|Faeroe)\b'
ieadb = ieadb[~(ieadb.TreatyName.str.contains(pattern)|ieadb.AlternativeTreatyNames.str.contains(pattern))]
# oceans
pattern = r'(?:Arctic|Antarctic|Pacific|Atlantic|Indian Ocean|Benguela)\b'
ieadb = ieadb[~(ieadb.TreatyName.str.contains(pattern)|ieadb.AlternativeTreatyNames.str.contains(pattern))]
# continents
pattern = r'(?i)(?:Asia[n]?|Europe\w*|(?<!particularly in )Africa\w*|America\w*)\b'
ieadb = ieadb[~(ieadb.TreatyName.str.contains(pattern)|ieadb.AlternativeTreatyNames.str.contains(pattern))]
# regional orgs
pattern = r'(?:Arab|ASEAN|Benelux|EEC|OECD|Nordic|North|East|South|West|Tripartite|\bRegional)'
ieadb = ieadb[~(ieadb.TreatyName.str.contains(pattern)|ieadb.AlternativeTreatyNames.str.contains(pattern))]
# derivative acts
pattern = r'(?i)(?:Council Decision|Declaration|(?:Plan|Programme) of Action|Resolution(?:s| \d+)|Directive|Agenda|Code of Conduct|Mandate)'
ieadb = ieadb[~(ieadb.TreatyName.str.contains(pattern)|ieadb.AlternativeTreatyNames.str.contains(pattern))]
# save to file
ieadb.to_csv(f'treaties_subset.csv', encoding='utf-8', index=False)
print(f'Narrowed down dataset from {initnb} to {len(ieadb)} agreements.')

# -*- coding: utf-8 -*-

import requests, re, os
from lxml import html
from datetime import datetime

with open('ieadb_urls.txt', 'rt') as f:
  urls = [url.strip() for url in f.readlines()]

for url in urls:
  res = requests.get(url)
  doc = html.fromstring(res.content)
  textel = doc.xpath('//div[@id="block-system-main"]')[0]
  for el in textel.xpath('//p'):
    if (el.tail is not None):
      el.tail += '\n'  
  textstr = html.tostring(textel, method='text', encoding='unicode').strip()
  textstr = re.sub('\n(?:Source|SRC): ?(?:\w.+|(?=\n))', '', textstr)
  fullstr = (textstr +'\n\n-----\n[Source: '+ url +' (last retrieved '+
             str(datetime.utcnow().isoformat().split("T")[0]) +')]')
  filename = doc.xpath('//span[preceding-sibling::strong[text()="Filename: "]]/text()')[0]
  d = '../../data/treatytexts/scraped/'
  if os.path.exists(d + filename):
    with open(d + filename, "r", encoding='utf-8') as f:
      oldt = f.read().split('\n\n-----\n[Source')[0].strip()
    if (textstr != oldt):
      print(filename, 'newt len: ' + str(len(textstr)) + ' oldt len: ' + str(len(oldt)))
      with open(d + filename, 'w', encoding='utf-8') as f:
        f.write(fullstr)          
  else:            
    with open(d + filename, 'w', encoding='utf-8') as f:
      f.write(fullstr)

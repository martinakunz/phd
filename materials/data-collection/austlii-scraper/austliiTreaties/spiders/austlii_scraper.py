import scrapy
from lxml import html
from datetime import datetime
import re, os

class austliiTreaties(scrapy.Spider):
    name = "austlii"
    allowed_domains = ["austlii.edu.au"]
    with open('treatyurls.txt', 'rt') as f:
        start_urls = [url.strip() for url in f.readlines()]

    def parse(self, response):
        # passing response body to lxml
        tree = html.fromstring(response.text)
        tree = tree.xpath('/html/body')[0]
        # discarding copyrighted footnotes and footer (multiple versions)
        for el in tree.xpath('//*[preceding::a[@name="fn0" or @name="fn1"] and parent::*]'):
            el.drop_tree()
        for el in tree.xpath('//a[@name="fn0" or @name="fn1"] | //*[child::a[@name="fn0" or @name="fn1"] and parent::p]'):
            el.getparent().remove(el)
        for el in tree.xpath('//b[text()="AustLII:"] | //b[text()="AustLII:"]/following::*'):
            el.getparent().remove(el)
        # drop in-text footnote anchors without removing tail
        for el in tree.xpath('//a[@name] | //a[@href]'):
            el.drop_tree()
        # remove anything prior to treaty title
        for el in tree.xpath('//*[text()[contains(.,"Commonwealth of Australia")]] | //*[text()[contains(.,"Commonwealth of Australia")]]/preceding::*'):
            el.getparent().remove(el)
        for el in tree.xpath('//p[contains(normalize-space(.), "National Interest Analysis")] | //p[contains(normalize-space(.), "National Interest Analysis")]/preceding::*'):
            el.getparent().remove(el)
        for el in tree.xpath('//*[text()[contains(., "] ATS ")]] | //*[text()[contains(., "] ATS ")]]/preceding::* | //center[child::p[text()[contains(., "] ATS ")]]]'):
            el.getparent().remove(el)
        for el in tree.xpath('//*[text()[contains(.,"Australian Treaty Series")]]'):
            el.getparent().remove(el)
        # tags
        for el in tree.xpath('//i | //u | //b | //strong | //sub | //sup | //hr'):
            if (el.tail is not None and ((el.text is None and not len(el)) or re.match('^\s+$', el.tail) is not None) and not(el.tail.startswith(' '))):
                el.tail = el.tail.strip()
            if (el.tag == 'hr'):
                if (el.tail is not None):
                    el.tail += '\n'
            if (el.tag == 'u' and el.text is not None and re.match('^\[\d\]$', el.text) is not None):
                el.drop_tree()
            else:   
                el.drop_tag()
        for el in tree.xpath('//blockquote'):
            el.tag = 'p'
        # empty elements
        for el in tree.iter('*'):
            if ((el.text is None or el.text.strip() == '') and (el.tail is None or el.tail.strip() == '') and (el.getchildren() == []) and (el.tag != 'br')):
                el.getparent().remove(el)
        # remnants from original doc
        for el in tree.xpath('//*[text() and not(child::*)]'):
            if (el.text is not None and re.match('^\s?Page \d+\s?$', el.text) is not None):
                el.drop_tree()
        for el in tree.xpath('//*[text()[contains(., "Page ")]]'):
            if (el.text is not None):
                el.text = re.sub('(?<=\s)Page \d+\s', '', el.text)
            if (el.tail is not None):
                el.tail = re.sub('(?<=\s)Page \d+\s', '', el.tail)                
        # table formatting
        for el in tree.xpath('//table//br | //table//p'):
            el.drop_tag()
        for el in tree.xpath('//table//*'):
            if (el.text is not None):
                el.text = el.text.strip()
            if (el.tail is not None):
                el.tail = el.tail.strip()
        for el in tree.xpath('//table//tr | //table//div[preceding-sibling::div or following-sibling::div]'): el.tail = '\n'
        for el in tree.xpath('//table//tr/td[position() < last()]'):
            el.tail += 'replacewithtabchar'
        # ordered lists to paras
        for el in tree.xpath('//ol/li'):
            if (el.text is not None and el.attrib['value'] is not None):
                el.text = el.attrib['value'] + '. ' + el.text.strip()
            el.tag = 'p'
            el.getparent().drop_tag()
                
        # text formatting   
        for el in tree.iter('*'):
            if (el.text is not None):
                el.text = re.sub('(?:\n\r? +|\n\n)','\n', el.text)                     
                el.text = re.sub('(?<!^)(?: ?(?:\n?\r|\n|  ) *)(?!$)',' ', el.text)
            if (el.tail is not None):
                el.tail = re.sub('(?:\n\r? +|\n\n)','\n', el.tail)   
                el.tail = re.sub('(?<!^)(?: ?(?:\n?\r|\n|  ) *)(?!$)',' ', el.tail)
                
        newpart = ('article ', 'annex', 'appendix', 'preamble', 'part', 'chapter', 'section', 'regulation', 'schedule', 'certificate', '[',)
        for el in tree.xpath('//p[@align="center"] | //div[@align="center" and not(ancestor::table)]'):
            el.tag = 'center'
        for el in tree.xpath('//center/p[text()]'):
            el.text = el.text.strip(' ')
            if (el.getparent().text is not None):
                el.getparent().text = el.getparent().text.strip(' ')
            if (not el.text.strip().lower().startswith(newpart)):
                el.text = re.sub('\n(?=[A-z])','  ', el.text)
            else:
                el.text = re.sub('^\n?(?=[A-Z])', '\n\n', el.text)
            el.drop_tag()
        for el in tree.xpath('//h1 | //h2 | //h3 | //h4 | //center'):
            for e in el.iter('br'):
                if (e.tail is not None):
                    e.tail = e.tail.strip()
                    if (e.tail.lower().startswith(newpart)):
                        e.tail = '\n\n' + e.tail
                    else:
                        e.tail = ' ' + e.tail
                e.drop_tag()
            if (el.text is not None):
                el.text = el.text.strip()
                el.text = re.sub('(?:(?<!\w)\n  ?)','\n', el.text)
                el.text = re.sub('(?:(?<=[A-z0-9,:])\n *(?=\w))',' ', el.text)
            if (el.tail is not None):
                el.tail = el.tail.strip()
            if (el.getnext() is not None and el.getnext().tag == el.tag and el.getnext().text is not None and not el.getnext().text.strip().lower().startswith(newpart)):
                if (el.tail is not None):
                    el.tail += '  '
                else:
                    el.tail = '  '  
            elif (not((el.text is None or el.text == '') and (el.tail is None or el.tail == '') and (el.getchildren() == []))):
                if (el.tail is not None):
                    el.tail += '\n\n'
                else:
                    el.tail = '\n\n'            

        for el in tree.xpath('//*[following::*[position()=1 and self::br]] | //p[text() and child::br]'):
            if (el.text is not None):
                el.text = el.text.strip() + '\n'
            elif (el.tail is not None):
                el.tail = el.tail.strip() + '\n'
            else:
                el.tail = '\n'
        for el in tree.xpath('//br'):
            el.drop_tag()
                
        for el in tree.iter('p'):
            if (el.text is not None):
                el.text = re.sub(' +', ' ', el.text)
                el.text = re.sub('\n\n+', '\n\n', el.text)
                if (re.match('^\s+$', el.text) is not None):
                    el.text = el.text.strip()
                else:
                    el.text = el.text.strip()
                    el.text += '\n\n'
            if (el.tail is not None):
                el.tail = el.tail.strip()

        ttext = html.tostring(tree, method='text', encoding='utf-8', pretty_print=True).decode('utf-8')
        ttext = ttext.replace('replacewithtabchar', '\t').strip()
        # adding source url
        fullstr = ttext +'\n\n-----\n[Source: '+ response.url +' (last retrieved '+ str(datetime.utcnow().isoformat().split("T")[0]) +')]\n[CC BY 3.0 AU Australian Government, Department of Foreign Affairs and Trade]'
        # save as txt file (if new version)
        filename = response.url.split("dfat/")[1].replace('/','-').replace('.html','.txt')
        d = '../../data/treatytexts/scraped/'
        if os.path.exists(d + filename):
            with open(d + filename, "r", encoding='utf-8') as f:
                oldt = f.read().split('\n\n-----\n[Source')[0]
            if (ttext != oldt):
                print(filename, 'ttext len: ' + str(len(ttext)) + ' oldt len: ' + str(len(oldt)))
                with open(d + filename, 'w', encoding='utf-8') as f:
                    f.write(fullstr)          
        else:            
            with open(d + filename, 'w', encoding='utf-8') as f:
                f.write(fullstr)

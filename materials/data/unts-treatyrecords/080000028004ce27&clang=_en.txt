[https://treaties.un.org/Pages/showDetails.aspx?objid=080000028004ce27&clang=_en]
[2022-08-14T20:12:53Z]


Registration Number	49196	
Title	International Convention on Arrest of Ships, 1999	
Participant(s)	
Submitter	ex officio	
Places/dates of conclusion	Place	Date	Geneva	12/03/1999	
EIF information	14 September 2011 , in accordance with article 14(1) which reads as follows: "1. This Convention shall enter into force six months following the date on which 10 States have expressed their consent to be bound by it." 	
Authentic texts	Spanish	Russian	French	English	Chinese	Arabic	
Attachments	
ICJ information	
Depositary	Secretary-General of the United Nations	
Registration Date	ex officio 1 December 2011	
Subject terms	Navigation	Maritime matters	Criminal matters	Claims-Debts	
Agreement type	Multilateral	
UNTS Volume Number	 2797 (p.3)	
Publication format	Full	
Certificate Of Registration	
Text document(s)	volume-2797-I-49196.pdf [https://treaties.un.org/doc/Publication/UNTS/Volume%202797/Part/volume-2797-I-49196.pdf]	
Volume In PDF	v2797.pdf [https://treaties.un.org/doc/Publication/UNTS/Volume%202797/v2797.pdf]	
Map(s)	
Corrigendum/Addendum

Participant	Action	Date of Notification/Deposit	Date of Effect
Albania [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002802c90a7&clang=_en]	Accession	14/03/2011	14/09/2011
Algeria [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028004ce46&clang=_en]	Accession	07/05/2004	14/09/2011
Benin [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028027a76f&clang=_en]	Accession	03/03/2010	14/09/2011
Bulgaria [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028004ce95&clang=_en]	Signature	27/07/2000	 
Bulgaria [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028004ce77&clang=_en]	Ratification	21/02/2001	14/09/2011
Congo [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002803c6df4&clang=_en]	Accession	11/06/2014	11/12/2014
Denmark [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028004ce8e&clang=_en]	Signature	10/08/2000	 
Ecuador [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028004ce9c&clang=_en]	Signature	13/07/2000	 
Ecuador [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002802a58b2&clang=_en]	Ratification	15/10/2010	14/09/2011
Estonia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028004ce70&clang=_en]	Accession	11/05/2001	14/09/2011
Finland [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028004ce7e&clang=_en]	Signature	31/08/2000	 
Latvia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028004ce69&clang=_en]	Accession	07/12/2001	14/09/2011
Liberia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028004ce36&clang=_en]	Accession	16/09/2005	14/09/2011
None [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028004ceb1&clang=_en]	Opening for signature	01/09/1999	 
None [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028004ceaa&clang=_en]	Issuance of certified true copies	12/03/1999	 
Norway [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028004ce86&clang=_en]	Signature	25/08/2000	 
Pakistan [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028004cea3&clang=_en]	Signature	11/07/2000	 
Peru [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002805db253&clang=_en]	Accession	22/03/2022	22/06/2022
Spain [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028004ce5d&clang=_en]	Accession	07/06/2002	14/09/2011
Syrian Arab Republic [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028004ce4d&clang=_en]	Accession	16/10/2002	14/09/2011
Turkey [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280569a89&clang=_en]	Accession	11/09/2019	11/12/2019
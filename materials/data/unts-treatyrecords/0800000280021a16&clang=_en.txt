[https://treaties.un.org/Pages/showDetails.aspx?objid=0800000280021a16&clang=_en]
[2022-08-14T20:20:01Z]


Registration Number	30822	
Title	Kyoto Protocol to the United Nations Framework Convention on Climate Change	
Participant(s)	
Submitter	ex officio	
Places/dates of conclusion	Place	Date	Kyoto	11/12/1997	
EIF information	16 February 2005 , in accordance with article 25(1)  and article 25 (3) which read as follows: 
"1. This Protocol shall enter into force on the ninetieth day after the date on which not less than 55 Parties to the Convention, incorporating Parties included in Annex I which accounted in total for at least 55 per cent of the total carbon dioxide emissions for 1990 of the Parties included in Annex I, have deposited their instruments of ratification, acceptance, approval or accession."
"3. For each State or regional economic integration organization that ratifies, accepts or approves this Protocol or accedes thereto after the conditions set out in paragraph 1 above for entry into force have been fulfilled, this Protocol shall enter into force on the ninetieth day following the date of deposit of its instrument of ratification acceptance, approval or accession" 	
Authentic texts	Spanish	Russian	French	English	Chinese	Arabic	
Attachments	with annexes	
ICJ information	
Depositary	Secretary-General of the United Nations	
Registration Date	ex officio 16 February 2005	
Subject terms	Environment	
Agreement type	Multilateral	
UNTS Volume Number	 2303 (p.162)	
Publication format	Full	
Certificate Of Registration	
Text document(s)	volume-2303-A-30822.pdf [https://treaties.un.org/doc/Publication/UNTS/Volume%202303/volume-2303-A-30822.pdf]	
Volume In PDF	v2303.pdf [https://treaties.un.org/doc/Publication/UNTS/Volume%202303/v2303.pdf]	
Map(s)	
Corrigendum/Addendum

Participant	Action	Date of Notification/Deposit	Date of Effect
Afghanistan [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028035fd7f&clang=_en]	Accession	25/03/2013	23/06/2013
Albania [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021ba5&clang=_en]	Accession	01/04/2005	30/06/2005
Algeria [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021bde&clang=_en]	Accession	16/02/2005	17/05/2005
Angola [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021aac&clang=_en]	Accession	08/05/2007	06/08/2007
Antigua and Barbuda [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800222b4&clang=_en]	Signature	16/03/1998	 
Antigua and Barbuda [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280022310&clang=_en]	Ratification	03/11/1998	16/02/2005
Argentina [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800222b0&clang=_en]	Signature	16/03/1998	 
Argentina [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021ec9&clang=_en]	Communication	27/03/2007	 
Argentina [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280022009&clang=_en]	Ratification	28/09/2001	16/02/2005
Armenia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021d30&clang=_en]	Accession	25/04/2003	16/02/2005
Australia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800222ac&clang=_en]	Signature	29/04/1998	 
Australia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801827e1&clang=_en]	Ratification	12/12/2007	11/03/2008
Austria [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800222a8&clang=_en]	Signature	29/04/1998	 
Austria [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021f71&clang=_en]	Ratification	31/05/2002	16/02/2005
Azerbaijan [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028002206e&clang=_en]	Accession	28/09/2000	16/02/2005
Bahamas [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280022150&clang=_en]	Accession	09/04/1999	16/02/2005
Bahrain [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021b29&clang=_en]	Accession	31/01/2006	01/05/2006
Bangladesh [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021ff6&clang=_en]	Accession	22/10/2001	16/02/2005
Barbados [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800220b3&clang=_en]	Accession	07/08/2000	16/02/2005
Belarus [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021b63&clang=_en]	Accession	26/08/2005	24/11/2005
Belgium [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800222a4&clang=_en]	Signature	29/04/1998	 
Belgium [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021f68&clang=_en]	Ratification	31/05/2002	16/02/2005
Belize [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021cc9&clang=_en]	Accession	26/09/2003	16/02/2005
Benin [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021fbd&clang=_en]	Accession	25/02/2002	16/02/2005
Bhutan [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021e2e&clang=_en]	Accession	26/08/2002	16/02/2005
Bolivia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800222a0&clang=_en]	Signature	09/07/1998	 
Bolivia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800220e9&clang=_en]	Ratification	30/11/1999	16/02/2005
Bosnia and Herzegovina [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021abe&clang=_en]	Accession	16/04/2007	15/07/2007
Botswana [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021cf7&clang=_en]	Accession	08/08/2003	16/02/2005
Brazil [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028002229c&clang=_en]	Signature	29/04/1998	 
Brazil [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021e25&clang=_en]	Ratification	23/08/2002	16/02/2005
Brunei Darussalam [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280253f98&clang=_en]	Accession	20/08/2009	18/11/2009
Bulgaria [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028002232a&clang=_en]	Signature	18/09/1998	 
Bulgaria [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021e4a&clang=_en]	Ratification	15/08/2002	16/02/2005
Burkina Faso [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021bae&clang=_en]	Accession	31/03/2005	29/06/2005
Burundi [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280022000&clang=_en]	Accession	18/10/2001	16/02/2005
Cambodia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021e0a&clang=_en]	Accession	22/08/2002	16/02/2005
Cameroon [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021dcc&clang=_en]	Accession	28/08/2002	16/02/2005
Canada [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280022298&clang=_en]	Signature	29/04/1998	 
Canada [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280300056&clang=_en]	Withdrawal	15/12/2011	15/12/2011
Canada [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021d78&clang=_en]	Ratification	17/12/2002	16/02/2005
Cape Verde [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021b1f&clang=_en]	Accession	10/02/2006	11/05/2006
Central African Republic [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801e289d&clang=_en]	Accession	18/03/2008	16/06/2008
Chad [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280265423&clang=_en]	Accession	18/08/2009	17/11/2009
Chile [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280022294&clang=_en]	Signature	17/06/1998	 
Chile [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021e53&clang=_en]	Ratification	26/08/2002	16/02/2005
China [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801caac1&clang=_en]	Communication	14/01/2008	 
China [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021ddc&clang=_en]	Communication	08/04/2003	 
China [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021ded&clang=_en]	Communication	30/08/2002	 
China [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280022290&clang=_en]	Signature	29/05/1998	 
China [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021dd6&clang=_en]	Approval	30/08/2002	16/02/2005
Colombia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021fd1&clang=_en]	Accession	30/11/2001	16/02/2005
Comoros [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801e0d85&clang=_en]	Accession	10/04/2008	09/07/2008
Congo [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021ac7&clang=_en]	Accession	12/02/2007	13/05/2007
Cook Islands [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280022283&clang=_en]	Signature	16/09/1998	 
Cook Islands [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280022012&clang=_en]	Ratification	27/08/2001	16/02/2005
Costa Rica [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028002227f&clang=_en]	Signature	27/04/1998	 
Costa Rica [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021e5c&clang=_en]	Ratification	09/08/2002	16/02/2005
Côte d'Ivoire [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021ab5&clang=_en]	Accession	23/04/2007	22/07/2007
Croatia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280022184&clang=_en]	Signature	11/03/1999	 
Croatia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021aa3&clang=_en]	Ratification	30/05/2007	28/08/2007
Cuba [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280022167&clang=_en]	Signature	15/03/1999	 
Cuba [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021f8d&clang=_en]	Ratification	30/04/2002	16/02/2005
Cyprus [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280022120&clang=_en]	Accession	16/07/1999	16/02/2005
Czech Republic [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800222e9&clang=_en]	Signature	23/11/1998	 
Czech Republic [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021fda&clang=_en]	Approval	15/11/2001	16/02/2005
Democratic People's Republic of Korea [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021b91&clang=_en]	Accession	27/04/2005	26/07/2005
Democratic Republic of the Congo [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021bb8&clang=_en]	Accession	23/03/2005	21/06/2005
Denmark [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028002227b&clang=_en]	Signature	29/04/1998	 
Denmark [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021e9d&clang=_en]	Territorial exclusion	31/05/2002	 
Denmark [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021ea6&clang=_en]	Ratification	31/05/2002	16/02/2005
Djibouti [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021fb3&clang=_en]	Accession	12/03/2002	16/02/2005
Dominica [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021bf7&clang=_en]	Accession	25/01/2005	25/04/2005
Dominican Republic [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021fc7&clang=_en]	Accession	12/02/2002	16/02/2005
Ecuador [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800221bb&clang=_en]	Signature	15/01/1999	 
Ecuador [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800220ce&clang=_en]	Ratification	13/01/2000	16/02/2005
Egypt [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280022160&clang=_en]	Signature	15/03/1999	 
Egypt [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021c09&clang=_en]	Ratification	12/01/2005	12/04/2005
El Salvador [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280022277&clang=_en]	Signature	08/06/1998	 
El Salvador [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800222e0&clang=_en]	Ratification	30/11/1998	16/02/2005
Equatorial Guinea [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800220a9&clang=_en]	Accession	16/08/2000	16/02/2005
Eritrea [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021b76&clang=_en]	Accession	28/07/2005	26/10/2005
Estonia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800222d9&clang=_en]	Signature	03/12/1998	 
Estonia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021d9f&clang=_en]	Ratification	14/10/2002	16/02/2005
Ethiopia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021b9b&clang=_en]	Accession	14/04/2005	13/07/2005
European Community [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280022273&clang=_en]	Signature	29/04/1998	 
European Community [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021eb0&clang=_en]	Approval	31/05/2002	16/02/2005
Fiji [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028002233a&clang=_en]	Signature	17/09/1998	 
Fiji [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280022331&clang=_en]	Ratification	17/09/1998	16/02/2005
Finland [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028002226f&clang=_en]	Signature	29/04/1998	 
Finland [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021f56&clang=_en]	Ratification	31/05/2002	16/02/2005
France [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028002226b&clang=_en]	Signature	29/04/1998	 
France [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021f49&clang=_en]	Approval	31/05/2002	16/02/2005
Gabon [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021acf&clang=_en]	Accession	12/12/2006	12/03/2007
Gambia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028002203d&clang=_en]	Accession	01/06/2001	16/02/2005
Georgia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028002213d&clang=_en]	Accession	16/06/1999	16/02/2005
Germany [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280022267&clang=_en]	Signature	29/04/1998	 
Germany [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021f5f&clang=_en]	Ratification	31/05/2002	16/02/2005
Ghana [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021d13&clang=_en]	Accession	30/05/2003	16/02/2005
Greece [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280022263&clang=_en]	Signature	29/04/1998	 
Greece [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021f40&clang=_en]	Ratification	31/05/2002	16/02/2005
Grenada [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021e66&clang=_en]	Accession	06/08/2002	16/02/2005
Guatemala [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028002225f&clang=_en]	Signature	10/07/1998	 
Guatemala [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028002210e&clang=_en]	Ratification	05/10/1999	16/02/2005
Guinea [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280022078&clang=_en]	Accession	07/09/2000	16/02/2005
Guinea-Bissau [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021b4d&clang=_en]	Accession	18/11/2005	16/02/2006
Guyana [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021d01&clang=_en]	Accession	05/08/2003	16/02/2005
Haiti [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021b89&clang=_en]	Accession	06/07/2005	04/10/2005
Honduras [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800221a3&clang=_en]	Signature	25/02/1999	 
Honduras [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800220bc&clang=_en]	Ratification	19/07/2000	16/02/2005
Hungary [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021e41&clang=_en]	Accession	21/08/2002	16/02/2005
Iceland [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021f83&clang=_en]	Accession	23/05/2002	16/02/2005
India [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021e1c&clang=_en]	Accession	26/08/2002	16/02/2005
Indonesia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028002225b&clang=_en]	Signature	13/07/1998	 
Indonesia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021c44&clang=_en]	Ratification	03/12/2004	03/03/2005
Iran (Islamic Republic of) [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021b6d&clang=_en]	Accession	22/08/2005	20/11/2005
Iraq [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028023bf7d&clang=_en]	Accession	28/07/2009	26/10/2009
Ireland [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280022253&clang=_en]	Signature	29/04/1998	 
Ireland [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021f37&clang=_en]	Ratification	31/05/2002	16/02/2005
Israel [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800222b8&clang=_en]	Signature	16/12/1998	 
Israel [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021ca7&clang=_en]	Ratification	15/03/2004	16/02/2005
Italy [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280022257&clang=_en]	Signature	29/04/1998	 
Italy [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021f2e&clang=_en]	Ratification	31/05/2002	16/02/2005
Jamaica [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028002212a&clang=_en]	Accession	28/06/1999	16/02/2005
Japan [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280022247&clang=_en]	Signature	28/04/1998	 
Japan [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021e8b&clang=_en]	Acceptance	04/06/2002	16/02/2005
Jordan [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021d4c&clang=_en]	Accession	17/01/2003	16/02/2005
Kazakhstan [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028002216e&clang=_en]	Signature	12/03/1999	 
Kazakhstan [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028023611e&clang=_en]	Ratification	19/06/2009	17/09/2009
Kenya [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021bd5&clang=_en]	Accession	25/02/2005	26/05/2005
Kiribati [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280022093&clang=_en]	Accession	07/09/2000	16/02/2005
Kuwait [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021bc1&clang=_en]	Accession	11/03/2005	09/06/2005
Kyrgyzstan [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021d1c&clang=_en]	Accession	13/05/2003	16/02/2005
Lao People's Democratic Republic [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021d43&clang=_en]	Accession	06/02/2003	16/02/2005
Latvia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800222bf&clang=_en]	Signature	14/12/1998	 
Latvia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021e81&clang=_en]	Ratification	05/07/2002	16/02/2005
Lebanon [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021ad7&clang=_en]	Accession	13/11/2006	11/02/2007
Lesotho [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280022081&clang=_en]	Accession	06/09/2000	16/02/2005
Liberia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021d95&clang=_en]	Accession	05/11/2002	16/02/2005
Libyan Arab Jamahiriya [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021af4&clang=_en]	Accession	24/08/2006	22/11/2006
Liechtenstein [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028002224f&clang=_en]	Signature	29/06/1998	 
Liechtenstein [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021c3b&clang=_en]	Ratification	03/12/2004	03/03/2005
Lithuania [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280022359&clang=_en]	Signature	21/09/1998	 
Lithuania [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021d60&clang=_en]	Ratification	03/01/2003	16/02/2005
Luxembourg [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028002224b&clang=_en]	Signature	29/04/1998	 
Luxembourg [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021f25&clang=_en]	Ratification	31/05/2002	16/02/2005
Madagascar [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021cc0&clang=_en]	Accession	24/09/2003	16/02/2005
Malawi [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021fec&clang=_en]	Accession	26/10/2001	16/02/2005
Malaysia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280022176&clang=_en]	Signature	12/03/1999	 
Malaysia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021dc3&clang=_en]	Ratification	04/09/2002	16/02/2005
Maldives [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280022243&clang=_en]	Signature	16/03/1998	 
Maldives [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800221d9&clang=_en]	Ratification	30/12/1998	16/02/2005
Mali [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800221b4&clang=_en]	Signature	27/01/1999	 
Mali [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021fa0&clang=_en]	Ratification	28/03/2002	16/02/2005
Malta [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028002223f&clang=_en]	Signature	17/04/1998	 
Malta [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021fe3&clang=_en]	Ratification	11/11/2001	16/02/2005
Marshall Islands [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028002223b&clang=_en]	Signature	17/03/1998	 
Marshall Islands [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021cee&clang=_en]	Ratification	11/08/2003	16/02/2005
Mauritania [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021b80&clang=_en]	Accession	22/07/2005	20/10/2005
Mauritius [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280022047&clang=_en]	Accession	09/05/2001	16/02/2005
Mexico [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280022237&clang=_en]	Signature	09/06/1998	 
Mexico [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800220a0&clang=_en]	Ratification	07/09/2000	16/02/2005
Micronesia (Federated States of) [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280022233&clang=_en]	Signature	17/03/1998	 
Micronesia (Federated States of) [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280022133&clang=_en]	Ratification	21/06/1999	16/02/2005
Monaco [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028002222f&clang=_en]	Signature	29/04/1998	 
Monaco [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021b12&clang=_en]	Ratification	27/02/2006	28/05/2006
Mongolia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800220d7&clang=_en]	Accession	15/12/1999	16/02/2005
Montenegro [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021a9a&clang=_en]	Accession	04/06/2007	02/09/2007
Morocco [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028002208a&clang=_en]	Accession	25/01/2002	16/02/2005
Mozambique [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021c11&clang=_en]	Accession	18/01/2005	18/04/2005
Myanmar [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021ce5&clang=_en]	Accession	13/08/2003	16/02/2005
Namibia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021cd2&clang=_en]	Accession	04/09/2003	16/02/2005
Nauru [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028002201c&clang=_en]	Accession	16/08/2001	16/02/2005
Nepal [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021b56&clang=_en]	Accession	16/09/2005	15/12/2005
Netherlands [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028002222b&clang=_en]	Signature	29/04/1998	 
Netherlands [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021f1c&clang=_en]	Acceptance	31/05/2002	16/02/2005
New Zealand [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280022227&clang=_en]	Signature	22/05/1998	 
New Zealand [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021d6a&clang=_en]	Ratification	19/12/2002	16/02/2005
Nicaragua [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280022223&clang=_en]	Signature	07/07/1998	 
Nicaragua [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800220f3&clang=_en]	Ratification	18/11/1999	16/02/2005
Niger [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028002231a&clang=_en]	Signature	23/10/1998	 
Niger [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021c79&clang=_en]	Ratification	30/09/2004	16/02/2005
Nigeria [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021c33&clang=_en]	Accession	10/12/2004	10/03/2005
Niue [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800222c6&clang=_en]	Signature	08/12/1998	 
Niue [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280022147&clang=_en]	Ratification	06/05/1999	16/02/2005
None [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021cb1&clang=_en]	Proposal of corrections	23/01/2004	 
None [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021c55&clang=_en]	Entry into force	18/11/2004	16/02/2005
Norway [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028002221f&clang=_en]	Signature	29/04/1998	 
Norway [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021f7a&clang=_en]	Ratification	30/05/2002	16/02/2005
Oman [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021be7&clang=_en]	Accession	19/01/2005	19/04/2005
Pakistan [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021c22&clang=_en]	Accession	11/01/2005	11/04/2005
Palau [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800220e0&clang=_en]	Accession	10/12/1999	16/02/2005
Panama [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028002221b&clang=_en]	Signature	08/06/1998	 
Panama [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028002218b&clang=_en]	Ratification	05/03/1999	16/02/2005
Papua New Guinea [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280022195&clang=_en]	Signature	02/03/1999	 
Papua New Guinea [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021f97&clang=_en]	Ratification	28/03/2002	16/02/2005
Paraguay [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280022217&clang=_en]	Signature	25/08/1998	 
Paraguay [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280022117&clang=_en]	Ratification	27/08/1999	16/02/2005
Peru [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280022304&clang=_en]	Signature	13/11/1998	 
Peru [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021db1&clang=_en]	Ratification	12/09/2002	16/02/2005
Philippines [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280022213&clang=_en]	Signature	15/04/1998	 
Philippines [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021cb7&clang=_en]	Ratification	20/11/2003	16/02/2005
Poland [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028002220f&clang=_en]	Signature	15/07/1998	 
Poland [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021d81&clang=_en]	Ratification	13/12/2002	16/02/2005
Portugal [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028002220b&clang=_en]	Signature	29/04/1998	 
Portugal [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021f13&clang=_en]	Approval	31/05/2002	16/02/2005
Qatar [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021c1a&clang=_en]	Accession	11/01/2005	11/04/2005
Republic of Korea [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028002234f&clang=_en]	Signature	25/09/1998	 
Republic of Korea [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021d8b&clang=_en]	Ratification	08/11/2002	16/02/2005
Republic of Moldova [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021d26&clang=_en]	Accession	22/04/2003	16/02/2005
Romania [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800221d2&clang=_en]	Signature	05/01/1999	 
Romania [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280022050&clang=_en]	Ratification	19/03/2001	16/02/2005
Russian Federation [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028002217d&clang=_en]	Signature	11/03/1999	 
Russian Federation [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021c5e&clang=_en]	Ratification	18/11/2004	16/02/2005
Rwanda [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021c8b&clang=_en]	Accession	22/07/2004	16/02/2005
Samoa [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800221ff&clang=_en]	Signature	16/03/1998	 
Samoa [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280022064&clang=_en]	Ratification	27/11/2000	16/02/2005
San Marino [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280284153&clang=_en]	Accession	28/04/2010	27/07/2010
Sao Tome and Principe [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801e3800&clang=_en]	Accession	25/04/2008	24/07/2008
Saudi Arabia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021bef&clang=_en]	Accession	31/01/2005	01/05/2005
Senegal [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028002202a&clang=_en]	Accession	20/07/2001	16/02/2005
Serbia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021a91&clang=_en]	Accession	19/10/2007	17/01/2008
Seychelles [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800221f3&clang=_en]	Signature	20/03/1998	 
Seychelles [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021e78&clang=_en]	Ratification	22/07/2002	16/02/2005
Sierra Leone [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021ae1&clang=_en]	Accession	10/11/2006	08/02/2007
Singapore [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021b08&clang=_en]	Accession	12/04/2006	11/07/2006
Slovakia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028002219c&clang=_en]	Signature	26/02/1999	 
Slovakia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021e94&clang=_en]	Ratification	31/05/2002	16/02/2005
Slovenia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280022321&clang=_en]	Signature	21/10/1998	 
Slovenia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021e37&clang=_en]	Ratification	02/08/2002	16/02/2005
Solomon Islands [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280022341&clang=_en]	Signature	29/09/1998	 
Solomon Islands [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021d39&clang=_en]	Ratification	13/03/2003	16/02/2005
Somalia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280298c7b&clang=_en]	Accession	26/07/2010	24/10/2010
South Africa [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021e6f&clang=_en]	Accession	31/07/2002	16/02/2005
Spain [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800221fb&clang=_en]	Signature	29/04/1998	 
Spain [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021f0a&clang=_en]	Ratification	31/05/2002	16/02/2005
Sri Lanka [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021dba&clang=_en]	Accession	03/09/2002	16/02/2005
St. Kitts and Nevis [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801df8bd&clang=_en]	Accession	08/04/2008	07/07/2008
St. Lucia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280022207&clang=_en]	Signature	16/03/1998	 
St. Lucia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021cdc&clang=_en]	Ratification	20/08/2003	16/02/2005
St. Vincent and the Grenadines [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280022203&clang=_en]	Signature	19/03/1998	 
St. Vincent and the Grenadines [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021c2a&clang=_en]	Ratification	31/12/2004	31/03/2005
Sudan [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021c70&clang=_en]	Accession	02/11/2004	16/02/2005
Suriname [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021aeb&clang=_en]	Accession	25/09/2006	24/12/2006
Swaziland [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021b44&clang=_en]	Accession	13/01/2006	13/04/2006
Sweden [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800221f7&clang=_en]	Signature	29/04/1998	 
Sweden [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021f03&clang=_en]	Ratification	31/05/2002	 
Switzerland [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800221ef&clang=_en]	Signature	16/03/1998	 
Switzerland [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021d0a&clang=_en]	Ratification	09/07/2003	16/02/2005
Syrian Arab Republic [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021b32&clang=_en]	Accession	27/01/2006	27/04/2006
Tajikistan [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280214cc6&clang=_en]	Accession	29/12/2008	29/03/2009
Thailand [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800221c3&clang=_en]	Signature	02/02/1999	 
Thailand [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021e01&clang=_en]	Ratification	28/08/2002	16/02/2005
The former Yugoslav Republic of Macedonia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021c4c&clang=_en]	Accession	18/11/2004	16/02/2005
Timor-Leste [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280204f74&clang=_en]	Accession	14/10/2008	12/01/2009
Togo [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021c95&clang=_en]	Accession	02/07/2004	16/02/2005
Tonga [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801ca316&clang=_en]	Accession	14/01/2008	13/04/2008
Trinidad and Tobago [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800221cb&clang=_en]	Signature	07/01/1999	 
Trinidad and Tobago [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800221ab&clang=_en]	Ratification	28/01/1999	16/02/2005
Tunisia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021d56&clang=_en]	Accession	22/01/2003	16/02/2005
Turkey [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280231b87&clang=_en]	Accession	28/05/2009	26/08/2009
Turkmenistan [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280022348&clang=_en]	Signature	28/09/1998	 
Turkmenistan [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800220fc&clang=_en]	Ratification	11/01/1999	16/02/2005
Tuvalu [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800222ff&clang=_en]	Signature	16/11/1998	 
Tuvalu [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800222f5&clang=_en]	Ratification	16/11/1998	16/02/2005
Uganda [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021fa9&clang=_en]	Accession	25/03/2002	16/02/2005
Ukraine [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280022159&clang=_en]	Signature	15/03/1999	 
Ukraine [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021c9e&clang=_en]	Ratification	12/04/2004	16/02/2005
United Arab Emirates [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021c00&clang=_en]	Accession	26/01/2005	26/04/2005
United Kingdom of Great Britain and Northern Ireland [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800221eb&clang=_en]	Signature	29/04/1998	 
United Kingdom of Great Britain and Northern Ireland [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021ef3&clang=_en]	Territorial application	04/04/2006	 
United Kingdom of Great Britain and Northern Ireland [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021ee7&clang=_en]	Territorial application	02/01/2007	 
United Kingdom of Great Britain and Northern Ireland [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021ec3&clang=_en]	Territorial application	07/03/2007	 
United Kingdom of Great Britain and Northern Ireland [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021ebd&clang=_en]	Ratification	31/05/2002	16/02/2005
United Republic of Tanzania [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021e13&clang=_en]	Accession	26/08/2002	16/02/2005
United States of America [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028002230a&clang=_en]	Signature	12/11/1998	 
Uruguay [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800221e7&clang=_en]	Signature	29/07/1998	 
Uruguay [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028002205a&clang=_en]	Ratification	05/02/2001	16/02/2005
Uzbekistan [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800222f0&clang=_en]	Signature	20/11/1998	 
Uzbekistan [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280022105&clang=_en]	Ratification	12/10/1999	16/02/2005
Vanuatu [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280022034&clang=_en]	Accession	17/07/2001	16/02/2005
Venezuela (Bolivarian Republic of) [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021bcb&clang=_en]	Accession	18/02/2005	19/05/2005
Viet Nam [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800222d2&clang=_en]	Signature	03/12/1998	 
Viet Nam [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021da8&clang=_en]	Ratification	25/09/2002	16/02/2005
Yemen [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021c82&clang=_en]	Accession	15/09/2004	16/02/2005
Zambia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800221e3&clang=_en]	Signature	05/08/1998	 
Zambia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280021afe&clang=_en]	Ratification	07/07/2006	05/10/2006
Zimbabwe [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280237bd5&clang=_en]	Accession	30/06/2009	28/09/2009
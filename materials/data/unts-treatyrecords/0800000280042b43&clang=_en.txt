[https://treaties.un.org/Pages/showDetails.aspx?objid=0800000280042b43&clang=_en]
[2022-08-14T20:11:47Z]


Registration Number	30673	
Title	Statutes of the International Centre for Genetic Engineering and Biotechnology	
Participant(s)	
Submitter	ex officio	
Places/dates of conclusion	Place	Date	Madrid	13/09/1983	
EIF information	3 February 1994 , in accordance with article 21(1)	
Authentic texts	Spanish	Russian	French	English	Chinese	Arabic	
Attachments	with protocol	
ICJ information	
Depositary	Secretary-General of the United Nations	
Registration Date	ex officio 3 February 1994	
Subject terms	Sciences	Engineering	Charters-Constitutions-Statutes	Scientific matters	
Agreement type	Multilateral	
UNTS Volume Number	 1763 (p.91)	
Publication format	Full	
Certificate Of Registration	
Text document(s)	volume-1763-I-30673-English.pdf [https://treaties.un.org/doc/Publication/UNTS/Volume%201763/volume-1763-I-30673-English.pdf]	volume-1763-I-30673-French.pdf [https://treaties.un.org/doc/Publication/UNTS/Volume%201763/volume-1763-I-30673-French.pdf]	volume-1763-I-30673-Other.pdf [https://treaties.un.org/doc/Publication/UNTS/Volume%201763/volume-1763-I-30673-Other.pdf]	
Volume In PDF	v1763.pdf [https://treaties.un.org/doc/Publication/UNTS/Volume%201763/v1763.pdf]	
Map(s)	
Corrigendum/Addendum

Participant	Action	Date of Notification/Deposit	Date of Effect
Afghanistan [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042eac&clang=_en]	Signature ad referendum (s)	13/09/1983	 
Afghanistan [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042cf4&clang=_en]	Confirmation of signature ad referendum	28/03/1984	 
Afghanistan [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042f50&clang=_en]	Ratification	06/07/1988	 
Algeria [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042ea7&clang=_en]	Signature	13/09/1983	 
Algeria [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042f4b&clang=_en]	Ratification	11/09/1987	 
Algeria [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042d66&clang=_en]	Notification under article 21 (1)	22/12/1992	03/02/1994
Angola [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280591a56&clang=_en]	Accession	16/11/2020	16/12/2020
Argentina [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042ea2&clang=_en]	Signature	13/09/1983	 
Argentina [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042f46&clang=_en]	Ratification	08/05/1990	 
Argentina [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042d61&clang=_en]	Notification under article 21 (1)	22/12/1992	03/02/1994
Bangladesh [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042d89&clang=_en]	Accession	18/07/1996	17/08/1996
Bhutan [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042e9d&clang=_en]	Signature	31/05/1984	 
Bhutan [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042f41&clang=_en]	Ratification	07/05/1985	 
Bhutan [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042d5c&clang=_en]	Notification under article 21 (1)	22/12/1992	03/02/1994
Bhutan [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280549a84&clang=_en]	Withdrawal	06/05/2019	06/05/2020
Bolivia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042e98&clang=_en]	Signature	13/09/1983	 
Bosnia and Herzegovina [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042c96&clang=_en]	Accession	01/02/2005	03/03/2005
Brazil [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042e93&clang=_en]	Signature ad referendum (s)	05/05/1986	 
Brazil [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042f2a&clang=_en]	Ratification	09/03/1990	 
Brazil [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042d16&clang=_en]	Notification under article 21 (1)	04/02/1993	03/02/1994
Brazil [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042f35&clang=_en]	Notification	09/05/2002	09/05/2002
Brazil [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042f2f&clang=_en]	Notification	15/05/2001	15/05/2002
Bulgaria [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042f25&clang=_en]	Acceptance	23/06/1986	 
Bulgaria [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042e8e&clang=_en]	Signature ad referendum (s)	13/09/1983	 
Burkina Faso [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002803e4a9a&clang=_en]	Accession	30/09/2014	30/10/2014
Burundi [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801fc581&clang=_en]	Accession	22/08/2008	21/09/2008
Cameroon [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042c79&clang=_en]	Accession	27/04/2006	27/05/2006
Chile [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042e89&clang=_en]	Signature	13/09/1983	 
Chile [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042db4&clang=_en]	Ratification	27/04/1994	27/05/1994
China [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042e84&clang=_en]	Signature	13/09/1983	 
China [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042f20&clang=_en]	Acceptance	13/04/1992	 
China [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801f8354&clang=_en]	Declaration	05/08/2008	 
China [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042d57&clang=_en]	Notification under article 21 (1)	22/12/1992	03/02/1994
Colombia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042e7f&clang=_en]	Signature	21/11/1986	 
Colombia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042d79&clang=_en]	Ratification	03/03/1997	02/04/1997
Congo [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042e7a&clang=_en]	Signature	13/09/1983	 
Costa Rica [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042e75&clang=_en]	Signature ad referendum (s)	14/08/1990	 
Costa Rica [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042d7f&clang=_en]	Ratification	11/10/1996	10/11/1996
Côte d'Ivoire [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042d6b&clang=_en]	Accession	22/01/1999	21/02/1999
Croatia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042cf0&clang=_en]	Signature	20/10/1992	 
Croatia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042f1b&clang=_en]	Acceptance	26/08/1993	 
Croatia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042d11&clang=_en]	Notification under article 21 (1)	20/09/1993	03/02/1994
Cuba [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042e70&clang=_en]	Signature	13/09/1983	 
Cuba [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042cec&clang=_en]	Notification under article 21 (1)	22/12/1992	 
Cuba [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042f15&clang=_en]	Ratification	30/06/1986	 
Ecuador [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042e6b&clang=_en]	Signature	13/09/1983	 
Ecuador [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042da4&clang=_en]	Ratification	26/10/1994	25/11/1994
Egypt [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042f10&clang=_en]	Ratification	13/01/1987	 
Egypt [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042e66&clang=_en]	Signature	13/09/1983	 
Egypt [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042d52&clang=_en]	Notification under article 21 (1)	22/12/1992	03/02/1994
Eritrea [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028027547a&clang=_en]	Accession	26/01/2010	25/02/2010
Ethiopia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280522ab5&clang=_en]	Accession	22/05/2019	21/06/2019
Greece [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042e61&clang=_en]	Signature	13/09/1983	 
Hungary [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042f0b&clang=_en]	Acceptance	13/01/1987	 
Hungary [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042e5c&clang=_en]	Signature	13/01/1987	 
Hungary [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042d0c&clang=_en]	Notification under article 21 (1)	31/08/1993	03/02/1994
India [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042f06&clang=_en]	Ratification	09/07/1985	 
India [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042e57&clang=_en]	Signature	13/09/1983	 
India [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042d4d&clang=_en]	Notification under article 21 (1)	22/12/1992	03/02/1994
Indonesia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042e52&clang=_en]	Signature	13/09/1983	 
Iran (Islamic Republic of) [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042e48&clang=_en]	Signature ad referendum (s)	29/04/1988	 
Iran (Islamic Republic of) [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042cc0&clang=_en]	Ratification	18/12/2001	17/01/2002
Iraq [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042f01&clang=_en]	Ratification	19/02/1985	 
Iraq [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042e4d&clang=_en]	Signature	28/02/1984	 
Iraq [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042d48&clang=_en]	Notification under article 21 (1)	22/12/1992	03/02/1994
Italy [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042efc&clang=_en]	Ratification	20/09/1990	 
Italy [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042e42&clang=_en]	Signature	13/09/1983	 
Italy [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042d43&clang=_en]	Notification under article 21 (1)	22/12/1992	03/02/1994
Jordan [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042cb7&clang=_en]	Accession	08/11/2002	08/12/2002
Kenya [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042ce4&clang=_en]	Accession	30/07/2010	29/08/2010
Kuwait [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042ef2&clang=_en]	Ratification	21/10/1986	 
Kuwait [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042e37&clang=_en]	Signature	13/09/1983	 
Kyrgyzstan [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042c9f&clang=_en]	Accession	07/10/1994	06/11/1994
Liberia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042c8d&clang=_en]	Accession	22/11/2005	22/12/2005
Libyan Arab Jamahiriya [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801ee690&clang=_en]	Accession	30/06/2008	30/07/2008
Malaysia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801caa8b&clang=_en]	Accession	11/12/2007	10/01/2008
Mauritania [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042e32&clang=_en]	Signature	13/09/1983	 
Mauritius [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042eed&clang=_en]	Ratification	05/01/1989	 
Mauritius [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042e2d&clang=_en]	Signature	19/09/1984	 
Mauritius [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042d07&clang=_en]	Notification under article 21 (1)	11/05/1993	03/02/1994
Mexico [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042e27&clang=_en]	Confirmation of signature ad referendum	21/05/1984	 
Mexico [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042ce8&clang=_en]	Signature ad referendum (s)	13/09/1983	 
Mexico [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042ee8&clang=_en]	Ratification	21/01/1988	 
Montenegro [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028032ec12&clang=_en]	Accession	06/08/2012	05/09/2012
Morocco [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042e3d&clang=_en]	Signature	19/10/1984	 
Morocco [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042ef7&clang=_en]	Ratification	28/06/1990	 
Morocco [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042d3e&clang=_en]	Notification under article 21 (1)	22/12/1992	03/02/1994
Namibia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028033c86b&clang=_en]	Accession	02/11/2012	02/12/2012
Nigeria [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042e22&clang=_en]	Signature	13/09/1983	 
Nigeria [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042da9&clang=_en]	Notification under article 21 (1)	27/04/1994	 
Nigeria [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042ee3&clang=_en]	Ratification	13/03/1991	 
Pakistan [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042e1d&clang=_en]	Signature	04/11/1983	 
Pakistan [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042dbc&clang=_en]	Ratification	05/04/1994	05/05/1994
Panama [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042e18&clang=_en]	Signature	11/12/1984	 
Panama [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042ede&clang=_en]	Ratification	12/08/1986	 
Panama [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042d39&clang=_en]	Notification under article 21 (1)	22/12/1992	03/02/1994
Peru [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042e13&clang=_en]	Signature	22/03/1984	 
Peru [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042d9a&clang=_en]	Ratification	06/01/1995	05/02/1995
Poland [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042e0e&clang=_en]	Signature	01/08/1990	 
Poland [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042d84&clang=_en]	Ratification	09/09/1996	09/10/1996
Poland [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280498dc8&clang=_en]	Withdrawal	30/12/2016	30/12/2017
Qatar [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801cb602&clang=_en]	Accession	16/01/2008	15/02/2008
Republic of Moldova [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280536738&clang=_en]	Accession	04/02/2019	06/03/2019
Romania [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042d94&clang=_en]	Accession	05/12/1995	04/01/1996
Russian Federation [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042ed9&clang=_en]	Acceptance	30/11/1992	 
Russian Federation [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042d34&clang=_en]	Notification under article 21 (1)	22/12/1992	03/02/1994
Saudi Arabia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042c84&clang=_en]	Accession	02/01/2006	01/02/2006
Senegal [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042e04&clang=_en]	Signature	29/06/1984	 
Senegal [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042ed4&clang=_en]	Ratification	04/05/1985	 
Senegal [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042d02&clang=_en]	Notification under article 21 (1)	23/12/1993	03/02/1994
Serbia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028055019a&clang=_en]	Succession	07/06/2019	03/02/1994
Slovakia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042d74&clang=_en]	Accession	13/01/1998	12/02/1998
Slovenia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042d9f&clang=_en]	Accession	28/12/1994	27/01/1995
South Africa [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042cc9&clang=_en]	Accession	06/02/2004	07/03/2004
Spain [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042dfe&clang=_en]	Signature	13/09/1983	 
Sri Lanka [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042df9&clang=_en]	Signature	12/11/1991	 
Sri Lanka [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042ecf&clang=_en]	Ratification	01/10/1993	 
Sri Lanka [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042cfd&clang=_en]	Notification under article 21 (1)	03/02/1994	03/02/1994
Sudan [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042df4&clang=_en]	Signature	13/09/1983	 
Sudan [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042eca&clang=_en]	Ratification	21/10/1991	 
Sudan [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042d2f&clang=_en]	Notification under article 21 (1)	22/12/1992	03/02/1994
Syrian Arab Republic [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042e09&clang=_en]	Signature	17/10/1991	 
Syrian Arab Republic [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042cdb&clang=_en]	Ratification	18/04/2001	18/05/2001
Thailand [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042def&clang=_en]	Signature	13/09/1983	 
The former Yugoslav Republic of Macedonia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042dae&clang=_en]	Accession	27/04/1994	27/05/1994
Trinidad and Tobago [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042dee&clang=_en]	Signature	13/09/1983	 
Trinidad and Tobago [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042cae&clang=_en]	Ratification	13/10/2003	12/11/2003
Tunisia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042ec5&clang=_en]	Ratification	20/09/1990	 
Tunisia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042dda&clang=_en]	Signature	27/10/1983	 
Tunisia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042d2a&clang=_en]	Notification under article 21 (1)	22/12/1992	03/02/1994
Turkey [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042ec0&clang=_en]	Ratification	10/01/1989	 
Turkey [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042dd5&clang=_en]	Signature	22/09/1987	 
Turkey [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042d25&clang=_en]	Notification under article 21 (1)	22/12/1992	03/02/1994
United Arab Emirates [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042ca5&clang=_en]	Accession	22/03/2004	21/04/2004
United Republic of Tanzania [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042cd2&clang=_en]	Accession	01/05/2001	31/05/2001
Uruguay [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042d8e&clang=_en]	Accession	05/12/1995	04/01/1996
Venezuela [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042dd0&clang=_en]	Signature	13/09/1983	 
Venezuela [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042ebb&clang=_en]	Ratification	15/10/1985	 
Venezuela [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042d20&clang=_en]	Notification under article 21 (1)	22/12/1992	03/02/1994
Viet Nam [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042dcb&clang=_en]	Signature	17/09/1984	 
Viet Nam [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042eb6&clang=_en]	Acceptance	15/04/1993	 
Viet Nam [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042cf8&clang=_en]	Notification under article 21 (1)	15/04/1993	03/02/1994
Yugoslavia (Socialist Federal Republic of) [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042dc6&clang=_en]	Signature	13/09/1983	 
Yugoslavia (Socialist Federal Republic of) [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042eb1&clang=_en]	Ratification	18/03/1987	 
Zaire [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280042dc1&clang=_en]	Signature	13/09/1983	 
Zimbabwe [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028052e5f8&clang=_en]	Accession	20/12/2018	19/01/2019
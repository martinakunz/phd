[https://treaties.un.org/Pages/showDetails.aspx?objid=0800000280066b19&clang=_en]
[2022-08-14T20:19:32Z]


Registration Number	1963	
Title	International Plant Protection Convention (new revised text)	
Participant(s)	
Submitter	Food and Agriculture Organization of the United Nations	
Places/dates of conclusion	Place	Date	Rome	17/11/1997	
EIF information	2 October 2005 	
Authentic texts	Spanish	French	English	Chinese	Arabic	
Attachments	with annex	
ICJ information	
Depositary	Director-General of the Food and Agriculture Organization of the United Nations	
Registration Date	Food and Agriculture Organization of the United Nations 20 April 2006	
Subject terms	Plants	
Agreement type	Multilateral	
UNTS Volume Number	 2367 (p.223)	
Publication format	Full	
Certificate Of Registration	COR-Reg-1963-Sr-53764.pdf [https://treaties.un.org/doc/Treaties/2006/04/20060420%2001-03%20AM/Other%20Documents/COR-Reg-1963-Sr-53764.pdf]	
Text document(s)	
Volume In PDF	v2367.pdf [https://treaties.un.org/doc/Publication/UNTS/Volume%202367/v2367.pdf]	
Map(s)	
Corrigendum/Addendum

Participant	Action	Date of Notification/Deposit	Date of Effect
Afghanistan [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002803b6afa&clang=_en]	Adherence	05/06/2013	05/06/2013
Albania [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066e09&clang=_en]	Adherence	29/07/1999	02/10/2005
Algeria [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066e03&clang=_en]	Acceptance	10/03/2003	02/10/2005
Antigua and Barbuda [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066bdd&clang=_en]	Adherence	24/01/2006	24/01/2006
Argentina [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066dfd&clang=_en]	Acceptance	05/04/2000	02/10/2005
Armenia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066b55&clang=_en]	Adherence	09/06/2006	09/06/2006
Australia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066df7&clang=_en]	Acceptance	13/06/2000	02/10/2005
Austria [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066df1&clang=_en]	Acceptance	29/08/2005	02/10/2005
Azerbaijan [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066deb&clang=_en]	Adherence	18/08/2000	02/10/2005
Bahamas [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002803b6aff&clang=_en]	Entry into force	 	02/10/2005
Bahrain [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002803b6b5c&clang=_en]	Entry into force	 	02/10/2005
Bangladesh [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066de5&clang=_en]	Acceptance	24/11/1998	02/10/2005
Barbados [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066ddf&clang=_en]	Acceptance	10/08/1998	02/10/2005
Belarus [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066dd9&clang=_en]	Adherence	21/02/2005	02/10/2005
Belgium [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002803b6b60&clang=_en]	Entry into force	 	02/10/2005
Belize [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066dd3&clang=_en]	Acceptance	15/04/2005	02/10/2005
Benin [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002802d11c8&clang=_en]	Adherence	12/10/2010	12/10/2010
Bhutan [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002803b6b6a&clang=_en]	Entry into force	 	02/10/2005
Bolivia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066dcd&clang=_en]	Acceptance	02/09/2005	02/10/2005
Bosnia and Herzegovina [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066dc7&clang=_en]	Adherence	30/07/2003	02/10/2005
Botswana [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280262fa6&clang=_en]	Adherence	24/06/2009	24/06/2009
Brazil [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066bd7&clang=_en]	Acceptance	15/11/2005	02/10/2005
Bulgaria [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066dc1&clang=_en]	Acceptance	21/06/2005	02/10/2005
Burkina Faso [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066dbb&clang=_en]	Acceptance	14/07/2005	02/10/2005
Burundi [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066b8f&clang=_en]	Adherence	03/04/2006	03/04/2006
Cambodia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066db5&clang=_en]	Acceptance	19/08/2005	02/10/2005
Cameroon [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066b89&clang=_en]	Adherence	05/04/2006	05/04/2006
Canada [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066daf&clang=_en]	Acceptance	22/10/2001	02/10/2005
Cape Verde [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066da9&clang=_en]	Acceptance	21/12/2004	02/10/2005
Central African Republic [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066da3&clang=_en]	Adherence	27/10/2004	02/10/2005
Chad [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066d9d&clang=_en]	Adherence	15/03/2004	02/10/2005
Chile [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066d97&clang=_en]	Acceptance	19/08/2004	02/10/2005
China [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002803b6b7f&clang=_en]	Adherence	20/10/2005	20/10/2005
Colombia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002803b6b83&clang=_en]	Entry into force	 	02/10/2005
Comoros [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066b41&clang=_en]	Adherence	17/01/2007	17/01/2007
Congo [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066d91&clang=_en]	Adherence	14/12/2004	02/10/2005
Cook Islands [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066d8b&clang=_en]	Adherence	02/12/2004	02/10/2005
Costa Rica [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066d85&clang=_en]	Acceptance	23/08/1999	02/10/2005
Côte d'Ivoire [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066d7f&clang=_en]	Adherence	17/12/2004	02/10/2005
Croatia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066d79&clang=_en]	Adherence	14/05/1999	02/10/2005
Cuba [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066d73&clang=_en]	Acceptance	18/02/2002	02/10/2005
Cyprus [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066d6d&clang=_en]	Adherence	11/02/1999	02/10/2005
Czech Republic [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066d67&clang=_en]	Acceptance	04/04/2001	02/10/2005
Democratic People's Republic of Korea [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066d5b&clang=_en]	Adherence	25/08/2003	02/10/2005
Democratic Republic of the Congo [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028043eb3c&clang=_en]	Adherence	04/05/2015	04/05/2015
Denmark [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066d54&clang=_en]	Acceptance	08/07/2002	02/10/2005
Djibouti [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801e6aa4&clang=_en]	Adherence	25/03/2008	25/03/2008
Dominica [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066b95&clang=_en]	Adherence	30/03/2006	30/03/2006
Dominican Republic [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066d61&clang=_en]	Acceptance	11/08/2005	02/10/2005
Ecuador [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066bd1&clang=_en]	Acceptance	04/04/2006	02/10/2005
Egypt [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002803b6b8a&clang=_en]	Entry into force	 	02/10/2005
El Salvador [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066bcb&clang=_en]	Acceptance	04/11/2005	02/10/2005
Equatorial Guinea [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002803b6b8e&clang=_en]	Entry into force	 	02/10/2005
Eritrea [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066d4e&clang=_en]	Adherence	06/04/2001	02/10/2005
Estonia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066d48&clang=_en]	Adherence	07/12/2000	02/10/2005
Ethiopia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066bc5&clang=_en]	Acceptance	19/09/2005	02/10/2005
European Union [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002803b6b95&clang=_en]	Adherence	06/10/2005	06/10/2005
Fiji [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066d42&clang=_en]	Adherence	10/08/2005	02/10/2005
Finland [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066bbf&clang=_en]	Acceptance	04/04/2006	02/10/2005
France [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066d3c&clang=_en]	Acceptance	11/07/2005	02/10/2005
Gabon [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801f8c31&clang=_en]	Adherence	23/04/2008	23/04/2008
Gambia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002804a9789&clang=_en]	Adherence	17/11/2016	17/11/2016
Georgia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066b37&clang=_en]	Adherence	08/03/2007	08/03/2007
Germany [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066d36&clang=_en]	Acceptance	06/07/2005	02/10/2005
Ghana [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066d30&clang=_en]	Acceptance	01/12/2004	02/10/2005
Greece [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066b4b&clang=_en]	Acceptance	14/11/2006	02/10/2005
Grenada [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066bb9&clang=_en]	Acceptance	28/10/2005	02/10/2005
Guatemala [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002803b6b9d&clang=_en]	Entry into force	 	02/10/2005
Guinea [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002803b6bb9&clang=_en]	Entry into force	 	02/10/2005
Guinea-Bissau [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028017ed11&clang=_en]	Adherence	24/10/2007	24/10/2007
Guyana [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002803b6bbd&clang=_en]	Entry into force	 	02/10/2005
Haiti [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002803b6bc7&clang=_en]	Entry into force	 	02/10/2005
Honduras [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066d2a&clang=_en]	Adherence	30/07/2003	02/10/2005
Hungary [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066d24&clang=_en]	Acceptance	28/06/2001	02/10/2005
Iceland [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066d1e&clang=_en]	Adherence	11/04/2005	02/10/2005
India [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002803b6bcb&clang=_en]	Entry into force	 	02/10/2005
Indonesia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002803b6bcf&clang=_en]	Entry into force	 	02/10/2005
Iran (Islamic Republic of) [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002803b6bdb&clang=_en]	Entry into force	 	02/10/2005
Iraq [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002803b6bdf&clang=_en]	Entry into force	 	02/10/2005
Ireland [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002803b6be7&clang=_en]	Entry into force	 	02/10/2005
Israel [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002803b6beb&clang=_en]	Entry into force	 	02/10/2005
Italy [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002803b6bef&clang=_en]	Entry into force	 	02/10/2005
Jamaica [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066bb3&clang=_en]	Acceptance	25/11/2005	02/10/2005
Japan [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002803b6bf7&clang=_en]	Entry into force	 	02/10/2005
Jordan [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066d18&clang=_en]	Acceptance	13/03/2002	02/10/2005
Kazakhstan [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002802d11c0&clang=_en]	Adherence	13/09/2010	13/09/2010
Kenya [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066d12&clang=_en]	Acceptance	10/09/2003	02/10/2005
Kuwait [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028016d0ee&clang=_en]	Adherence	12/09/2007	12/09/2007
Kyrgyzstan [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066d06&clang=_en]	Adherence	11/12/2003	02/10/2005
Lao People's Democratic Republic [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002803b6c05&clang=_en]	Entry into force	 	02/10/2005
Latvia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066d00&clang=_en]	Acceptance	05/11/2003	02/10/2005
Lebanon [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066cfa&clang=_en]	Acceptance	27/03/2002	02/10/2005
Lesotho [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002803c430a&clang=_en]	Adherence	24/10/2013	24/10/2013
Liberia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066cf4&clang=_en]	Acceptance	16/06/2005	02/10/2005
Libyan Arab Jamahiriya [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066cee&clang=_en]	Acceptance	12/04/2005	02/10/2005
Lithuania [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066ce8&clang=_en]	Adherence	12/01/2000	02/10/2005
Luxembourg [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002803b6c09&clang=_en]	Entry into force	 	02/10/2005
Madagascar [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066b6b&clang=_en]	Adherence	24/05/2006	24/05/2006
Malawi [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066ce2&clang=_en]	Acceptance	14/06/2004	02/10/2005
Malaysia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002803b6c0d&clang=_en]	Entry into force	 	02/10/2005
Maldives [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066b50&clang=_en]	Adherence	03/10/2006	03/10/2006
Mali [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066cdc&clang=_en]	Acceptance	10/02/2005	02/10/2005
Malta [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002803b6c18&clang=_en]	Entry into force	 	02/10/2005
Mauritania [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066cd6&clang=_en]	Adherence	29/04/2002	02/10/2005
Mauritius [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066cd0&clang=_en]	Acceptance	13/12/2000	02/10/2005
Mexico [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066cca&clang=_en]	Acceptance	28/06/2000	02/10/2005
Micronesia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066b32&clang=_en]	Adherence	06/07/2007	06/07/2007
Mongolia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280298278&clang=_en]	Adherence	26/05/2009	26/05/2009
Montenegro [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280262faa&clang=_en]	Adherence	27/07/2009	27/07/2009
Morocco [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066cbe&clang=_en]	Acceptance	08/02/2000	02/10/2005
Mozambique [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002803b6c23&clang=_en]	Adherence	15/05/2008	15/05/2008
Myanmar [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066b71&clang=_en]	Adherence	26/05/2006	26/05/2006
Namibia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066b3c&clang=_en]	Adherence	23/02/2007	23/02/2007
Nepal [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066b77&clang=_en]	Adherence	08/05/2006	08/05/2006
Netherlands [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066cb8&clang=_en]	Acceptance	27/08/2001	02/10/2005
New Zealand [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066cb2&clang=_en]	Acceptance	22/06/1999	02/10/2005
Nicaragua [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066cac&clang=_en]	Acceptance	03/11/2004	02/10/2005
Niger [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066ca6&clang=_en]	Acceptance	18/11/2003	02/10/2005
Nigeria [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066ca0&clang=_en]	Acceptance	02/09/2003	02/10/2005
Niue [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066bad&clang=_en]	Adherence	27/10/2005	27/10/2005
Norway [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066c9a&clang=_en]	Acceptance	29/02/2000	02/10/2005
Oman [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066c94&clang=_en]	Acceptance	28/01/2000	02/10/2005
Pakistan [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066c8e&clang=_en]	Acceptance	01/09/2003	02/10/2005
Palau [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066b5f&clang=_en]	Adherence	23/06/2006	23/06/2006
Panama [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066c88&clang=_en]	Acceptance	21/06/2005	02/10/2005
Papua New Guinea [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066c82&clang=_en]	Acceptance	15/01/1999	02/10/2005
Paraguay [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066ba7&clang=_en]	Acceptance	05/01/2006	02/10/2005
Peru [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066c7c&clang=_en]	Acceptance	22/03/2000	02/10/2005
Philippines [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066c76&clang=_en]	Acceptance	11/04/2005	02/10/2005
Poland [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002803b6c2b&clang=_en]	Entry into force	 	02/10/2005
Portugal [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002803b6c40&clang=_en]	Entry into force	 	02/10/2005
Qatar [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066b5a&clang=_en]	Adherence	08/06/2006	08/06/2006
Republic of Korea [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066d0c&clang=_en]	Acceptance	09/11/2000	02/10/2005
Republic of Moldova [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066cc4&clang=_en]	Adherence	25/01/2001	02/10/2005
Romania [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066c70&clang=_en]	Acceptance	21/01/1999	02/10/2005
Russian Federation [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066c6a&clang=_en]	Acceptance	16/01/2002	02/10/2005
Rwanda [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280229e6f&clang=_en]	Adherence	26/08/2008	26/08/2008
Samoa [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066c64&clang=_en]	Adherence	02/03/2005	02/10/2005
Sao Tome and Principe [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066b7d&clang=_en]	Adherence	07/04/2006	07/04/2006
Saudi Arabia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066c5e&clang=_en]	Adherence	07/08/2000	02/10/2005
Senegal [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066c58&clang=_en]	Acceptance	04/01/2002	02/10/2005
Serbia and Montenegro [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066c52&clang=_en]	Acceptance	19/11/2004	02/10/2005
Seychelles [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066c4c&clang=_en]	Acceptance	14/12/2004	02/10/2005
Sierra Leone [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066c46&clang=_en]	Acceptance	15/04/2002	02/10/2005
Singapore [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002802d11bc&clang=_en]	Adherence	18/08/2010	18/08/2010
Slovakia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066b9b&clang=_en]	Adherence	24/03/2006	24/03/2006
Slovenia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066c40&clang=_en]	Acceptance	16/11/2000	02/10/2005
Solomon Islands [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002803b6c44&clang=_en]	Entry into force	 	02/10/2005
South Africa [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002803b6c51&clang=_en]	Entry into force	 	02/10/2005
South Sudan [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002803c430e&clang=_en]	Adherence	06/12/2013	06/12/2013
Spain [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066c3a&clang=_en]	Acceptance	05/06/2000	02/10/2005
Sri Lanka [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002803b6c65&clang=_en]	Entry into force	 	02/10/2005
St. Kitts and Nevis [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002803bad64&clang=_en]	Entry into force	 	02/10/2005
St. Lucia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002803b6c7b&clang=_en]	Entry into force	 	02/10/2005
St. Vincent and the Grenadines [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066c34&clang=_en]	Adherence	15/11/2001	02/10/2005
Sudan [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002803b6c7f&clang=_en]	Entry into force	 	02/10/2005
Suriname [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002803b6c83&clang=_en]	Entry into force	 	02/10/2005
Swaziland [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066c2e&clang=_en]	Adherence	12/07/2005	02/10/2005
Sweden [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066c28&clang=_en]	Acceptance	07/06/1999	02/10/2005
Switzerland [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002803b6c8e&clang=_en]	Entry into force	 	02/10/2005
Syrian Arab Republic [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066c22&clang=_en]	Adherence	05/11/2003	02/10/2005
Tajikistan [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002802d11c4&clang=_en]	Accession	04/10/2010	04/10/2010
Thailand [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002803b6c9f&clang=_en]	Entry into force	 	02/10/2005
The former Yugoslav Republic of Macedonia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066c1c&clang=_en]	Adherence	09/08/2004	02/10/2005
Togo [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066c16&clang=_en]	Acceptance	23/03/2005	02/10/2005
Tonga [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002803b6ca3&clang=_en]	Adherence	23/11/2005	23/11/2005
Trinidad and Tobago [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066c10&clang=_en]	Acceptance	05/08/2005	02/10/2005
Tunisia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066c0a&clang=_en]	Acceptance	08/02/1999	02/10/2005
Turkey [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002803b6cbd&clang=_en]	Entry into force	 	02/10/2005
Tuvalu [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066b46&clang=_en]	Adherence	15/12/2006	15/12/2006
Uganda [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066b28&clang=_en]	Adherence	29/08/2007	29/08/2007
Ukraine [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066b65&clang=_en]	Adherence	31/05/2006	31/05/2006
United Arab Emirates [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066c04&clang=_en]	Acceptance	05/01/2005	02/10/2005
United Kingdom of Great Britain and Northern Ireland [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066bfd&clang=_en]	Acceptance	18/03/2004	02/10/2005
United Republic of Tanzania [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066ba1&clang=_en]	Adherence	21/02/2005	02/10/2005
United States of America [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066bf5&clang=_en]	Acceptance	02/10/2001	02/10/2005
Uruguay [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066bef&clang=_en]	Acceptance	12/07/2001	02/10/2005
Uzbekistan [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280574bce&clang=_en]	Adherence	13/01/2020	13/01/2020
Vanuatu [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066b2d&clang=_en]	Adherence	02/08/2007	02/08/2007
Venezuela [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002803b6cc1&clang=_en]	Entry into force	 	02/10/2005
Viet Nam [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066be9&clang=_en]	Adherence	22/02/2005	02/10/2005
Yemen [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066be3&clang=_en]	Acceptance	21/06/2005	02/10/2005
Zambia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280066b83&clang=_en]	Adherence	11/04/2006	02/10/2005
Zimbabwe [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002803720dd&clang=_en]	Adherence	30/11/2012	30/11/2012
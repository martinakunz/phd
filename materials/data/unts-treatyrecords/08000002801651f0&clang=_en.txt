[https://treaties.un.org/Pages/showDetails.aspx?objid=08000002801651f0&clang=_en]
[2022-08-14T20:13:11Z]


Registration Number	52	
Title	Constitution of the United Nations Educational, Scientific and Cultural Organization	
Participant(s)	
Submitter	Netherlands	
Places/dates of conclusion	Place	Date	London	16/11/1945	
EIF information	4 November 1946 	
Authentic texts	French	English	
Attachments	
ICJ information	
Depositary	Government of the United Kingdom of Great Britain and Northern Ireland	
Registration Date	Netherlands 12 June 1947	
Subject terms	United Nations Educational, Scientific and Cultural Organization (UNESCO)	United Nations (UN)	UNESCO Constitution	Charters-Constitutions-Statutes	
Agreement type	Multilateral	
UNTS Volume Number	 4 (p.275)	
Publication format	Full	
Certificate Of Registration	
Text document(s)	volume-4-I-52-English.pdf [https://treaties.un.org/doc/Publication/UNTS/Volume%204/volume-4-I-52-English.pdf]	volume-4-I-52-French.pdf [https://treaties.un.org/doc/Publication/UNTS/Volume%204/volume-4-I-52-French.pdf]	
Volume In PDF	v4.pdf [https://treaties.un.org/doc/Publication/UNTS/Volume%204/v4.pdf]	
Map(s)	
Corrigendum/Addendum

Participant	Action	Date of Notification/Deposit	Date of Effect
Afghanistan [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028016566d&clang=_en]	Acceptance	04/05/1948	 
Albania [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028016554f&clang=_en]	Acceptance	17/10/1958	16/10/1958
Andorra [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165236&clang=_en]	Acceptance	20/10/1993	20/10/1993
Angola [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165363&clang=_en]	Acceptance	09/11/1976	11/03/1977
Antigua and Barbuda [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028016530b&clang=_en]	Acceptance	15/07/1982	15/07/1982
Argentina [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165659&clang=_en]	Acceptance	15/09/1948	 
Argentina [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165716&clang=_en]	Signature	16/11/1945	 
Armenia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165290&clang=_en]	Acceptance	09/06/1992	09/06/1992
Armenia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801652a0&clang=_en]	Acceptance	09/06/1992	09/06/1992
Australia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028016577c&clang=_en]	Acceptance	11/06/1946	04/11/1946
Austria [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165668&clang=_en]	Acceptance	14/07/1948	 
Azerbaijan [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028016528a&clang=_en]	Acceptance	03/06/1992	03/06/1992
Azerbaijan [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801652a6&clang=_en]	Acceptance	03/06/1992	03/06/1992
Bahamas [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165315&clang=_en]	Acceptance	23/04/1981	23/04/1981
Bahrain [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801653d7&clang=_en]	Acceptance	18/01/1972	18/01/1972
Bangladesh [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801653c3&clang=_en]	Acceptance	27/10/1972	 
Barbados [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801653e6&clang=_en]	Acceptance	24/10/1968	 
Belgium [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801656b7&clang=_en]	Acceptance	29/11/1946	29/11/1946
Belize [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165212&clang=_en]	Acceptance	19/05/1982	10/05/1982
Bhutan [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165310&clang=_en]	Acceptance	13/04/1982	13/04/1982
Bolivia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801656a5&clang=_en]	Acceptance	13/11/1946	13/11/1946
Bosnia and Herzegovina [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028016525a&clang=_en]	Acceptance	02/06/1993	02/06/1993
Botswana [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028016533d&clang=_en]	Acceptance	24/09/1979	24/09/1979
Brazil [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165727&clang=_en]	Acceptance	14/10/1946	04/11/1946
Bulgaria [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165583&clang=_en]	Acceptance	17/05/1956	 
Bulgaria [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165588&clang=_en]	Signature	16/03/1956	 
Burma [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165638&clang=_en]	Signature	27/06/1949	 
Burma [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165632&clang=_en]	Acceptance	31/05/1949	27/06/1949
Burundi [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028016547d&clang=_en]	Acceptance	16/11/1962	 
Burundi [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165478&clang=_en]	Acceptance	12/11/1962	16/11/1962
Byelorussian Soviet Socialist Republic [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801655a6&clang=_en]	Acceptance	12/05/1954	 
Cambodia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801655d3&clang=_en]	Acceptance	03/07/1951	 
Cameroon [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165511&clang=_en]	Acceptance	11/11/1960	11/11/1960
Canada [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165748&clang=_en]	Acceptance	06/09/1946	04/11/1946
Cape Verde [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165354&clang=_en]	Acceptance	14/11/1977	15/02/1978
Central African Republic [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028016550b&clang=_en]	Acceptance	11/11/1960	11/11/1960
Ceylon [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028016561e&clang=_en]	Acceptance	14/11/1949	 
Chad [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801654dd&clang=_en]	Acceptance	19/12/1960	19/12/1960
Chile [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165711&clang=_en]	Signature	16/11/1945	 
Chile [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801655b0&clang=_en]	Acceptance	07/07/1953	 
China [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028016573f&clang=_en]	Acceptance	13/09/1946	04/11/1946
Colombia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028016570c&clang=_en]	Signature	16/11/1945	 
Colombia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165686&clang=_en]	Acceptance	31/10/1947	 
Comoros [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028016535e&clang=_en]	Acceptance	22/03/1977	22/03/1977
Congo (Brazzaville) [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028016552f&clang=_en]	Acceptance	24/10/1960	24/10/1960
Congo (Leopoldville) [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801654e2&clang=_en]	Acceptance	25/11/1960	25/11/1960
Cook Islands [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028016529b&clang=_en]	Acceptance	25/10/1989	25/10/1989
Costa Rica [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165600&clang=_en]	Signature	19/05/1950	 
Costa Rica [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801655ec&clang=_en]	Acceptance	19/05/1950	 
Croatia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801652b2&clang=_en]	Acceptance	01/06/1992	01/06/1992
Croatia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165284&clang=_en]	Acceptance	01/06/1992	01/06/1992
Cuba [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165690&clang=_en]	Acceptance	29/08/1947	 
Cuba [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165707&clang=_en]	Signature	16/11/1945	 
Cyprus [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801654d2&clang=_en]	Notification	 	06/02/1961
Czech Republic [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165260&clang=_en]	Acceptance	22/02/1993	22/02/1993
Czechoslovakia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028016572d&clang=_en]	Acceptance	05/10/1946	04/11/1946
Dahomey [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165535&clang=_en]	Acceptance	18/10/1960	18/10/1960
Democratic People's Republic of Korea [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801653a4&clang=_en]	Acceptance	18/10/1974	18/10/1974
Denmark [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165739&clang=_en]	Acceptance	20/09/1946	04/11/1946
Djibouti [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801652e7&clang=_en]	Acceptance	31/08/1989	31/08/1989
Dominica [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165348&clang=_en]	Acceptance	09/01/1979	09/01/1979
Dominican Republic [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165760&clang=_en]	Acceptance	02/07/1946	04/11/1946
Ecuador [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165702&clang=_en]	Signature	16/11/1945	 
Ecuador [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028016569a&clang=_en]	Acceptance	22/01/1947	 
Egypt [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165754&clang=_en]	Acceptance	16/07/1946	04/11/1946
El Salvador [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165672&clang=_en]	Acceptance	28/04/1948	 
Equatorial Guinea [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165343&clang=_en]	Acceptance	29/11/1979	29/11/1979
Eritrea [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028016524e&clang=_en]	Acceptance	02/09/1993	02/09/1993
Estonia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801652db&clang=_en]	Acceptance	14/10/1991	14/10/1991
Ethiopia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165592&clang=_en]	Signature	01/06/1955	 
Ethiopia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028016558d&clang=_en]	Acceptance	01/07/1955	 
Federal Republic of Germany [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801655c9&clang=_en]	Acceptance	11/07/1951	 
Fiji [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801652fc&clang=_en]	Acceptance	14/07/1983	14/07/1983
Finland [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165579&clang=_en]	Acceptance	10/10/1956	 
France [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165766&clang=_en]	Acceptance	29/06/1946	04/11/1946
Gabon [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801654f4&clang=_en]	Acceptance	16/11/1960	16/11/1960
Gambia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801653b9&clang=_en]	Acceptance	01/08/1973	 
Gambia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801653ae&clang=_en]	Acceptance	01/08/1973	01/08/1973
Georgia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165296&clang=_en]	Acceptance	07/10/1992	07/10/1992
German Democratic Republic [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801653be&clang=_en]	Acceptance	24/11/1972	 
German Democratic Republic [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801653b3&clang=_en]	Acceptance	24/11/1972	 
Ghana [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165560&clang=_en]	Signature	11/04/1958	11/04/1958
Ghana [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028016555b&clang=_en]	Acceptance	29/10/1957	11/04/1958
Greece [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028016571b&clang=_en]	Acceptance	04/11/1946	04/11/1946
Grenada [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028016539a&clang=_en]	Acceptance	29/11/1974	17/02/1975
Guatemala [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801656fd&clang=_en]	Signature	16/11/1945	 
Guatemala [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165614&clang=_en]	Acceptance	02/01/1950	 
Guinea [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028016554a&clang=_en]	Signature	02/02/1960	 
Guinea [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165545&clang=_en]	Acceptance	26/11/1959	 
Guinea-Bissau [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801653a9&clang=_en]	Acceptance	01/11/1974	01/11/1974
Guyana [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801653fa&clang=_en]	Acceptance	21/03/1967	 
Haiti [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801656c9&clang=_en]	Acceptance	18/11/1946	18/11/1946
Honduras [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028016567c&clang=_en]	Acceptance	16/12/1947	 
Hungary [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028016565e&clang=_en]	Acceptance	14/09/1948	 
Iceland [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165464&clang=_en]	Acceptance	08/06/1964	 
India [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165776&clang=_en]	Acceptance	12/06/1946	04/11/1946
Indonesia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801655e7&clang=_en]	Acceptance	27/05/1950	 
Indonesia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801653f5&clang=_en]	Cancellation of denunciation	30/07/1966	 
Indonesia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801655fb&clang=_en]	Signature	27/05/1950	 
Indonesia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165440&clang=_en]	Withdrawal	12/02/1965	31/12/1966
Iran [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801656f8&clang=_en]	Signature	16/11/1945	 
Iran [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165663&clang=_en]	Acceptance	06/09/1948	 
Iraq [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801656f3&clang=_en]	Signature	16/11/1945	 
Iraq [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165654&clang=_en]	Acceptance	21/10/1948	 
Ireland [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801654c8&clang=_en]	Acceptance	03/10/1961	 
Israel [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028016562d&clang=_en]	Acceptance	14/09/1949	 
Israel [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165623&clang=_en]	Signature	16/09/1949	 
Italy [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165677&clang=_en]	Acceptance	27/01/1948	 
Ivory Coast [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165540&clang=_en]	Signature	13/10/1960	27/10/1960
Ivory Coast [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028016553b&clang=_en]	Acceptance	27/10/1960	27/10/1960
Jamaica [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028016549b&clang=_en]	Signature	07/11/1962	 
Jamaica [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165496&clang=_en]	Acceptance	07/11/1962	07/11/1962
Japan [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801655d8&clang=_en]	Acceptance	02/07/1951	 
Jordan [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801655f6&clang=_en]	Signature	14/06/1950	 
Jordan [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801655e2&clang=_en]	Acceptance	14/06/1950	 
Kazakhstan [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801652be&clang=_en]	Acceptance	22/05/1992	22/05/1992
Kazakhstan [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028016527e&clang=_en]	Acceptance	22/05/1992	22/05/1992
Kenya [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165469&clang=_en]	Acceptance	07/04/1964	 
Kiribati [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801652e1&clang=_en]	Acceptance	24/10/1989	24/10/1989
Korea [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801655f1&clang=_en]	Signature	14/06/1950	 
Korea [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801655dd&clang=_en]	Acceptance	14/06/1950	 
Kuwait [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801654e8&clang=_en]	Acceptance	18/11/1960	18/11/1960
Kyrgyzstan [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165278&clang=_en]	Acceptance	02/06/1992	02/06/1992
Kyrgyzstan [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801652ac&clang=_en]	Acceptance	02/06/1992	02/06/1992
Laos [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801655ce&clang=_en]	Acceptance	09/07/1951	 
Latvia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801652d5&clang=_en]	Acceptance	14/10/1991	14/10/1991
Lebanon [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165721&clang=_en]	Acceptance	28/10/1946	04/11/1946
Lesotho [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801653f0&clang=_en]	Acceptance	29/09/1967	 
Liberia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165695&clang=_en]	Acceptance	06/03/1947	 
Liberia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801656ee&clang=_en]	Signature	16/11/1945	 
Libya [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801655b5&clang=_en]	Acceptance	09/03/1953	 
Libya [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801655ba&clang=_en]	Signature	27/06/1953	 
Lithuania [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801652cf&clang=_en]	Acceptance	07/10/1991	07/10/1991
Luxembourg [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801656e9&clang=_en]	Signature	16/11/1945	 
Luxembourg [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028016568b&clang=_en]	Acceptance	27/10/1947	 
Madagascar [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165517&clang=_en]	Acceptance	10/11/1960	10/11/1960
Malawi [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028016545f&clang=_en]	Acceptance	27/10/1964	 
Malaysia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165555&clang=_en]	Acceptance	16/06/1958	16/06/1958
Maldives [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028016532e&clang=_en]	Acceptance	26/03/1980	26/03/1980
Mali [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165529&clang=_en]	Acceptance	07/11/1960	07/11/1960
Malta [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165454&clang=_en]	Acceptance	10/02/1965	10/02/1965
Marshall Islands [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165220&clang=_en]	Acceptance	30/06/1995	30/06/1995
Mauritania [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801654c3&clang=_en]	Acceptance	10/01/1962	 
Mauritius [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801653e1&clang=_en]	Acceptance	25/10/1968	 
Mexico [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028016576c&clang=_en]	Acceptance	12/06/1946	04/11/1946
Micronesia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165206&clang=_en]	Acceptance	19/10/1999	19/10/1999
Monaco [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028016563d&clang=_en]	Acceptance	06/07/1949	06/07/1949
Mongolia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165473&clang=_en]	Signature	01/11/1962	 
Mongolia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801654aa&clang=_en]	Acceptance	04/10/1962	01/11/1962
Montenegro [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801651fb&clang=_en]	Acceptance	01/03/2007	01/03/2007
Morocco [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028016556a&clang=_en]	Acceptance	07/11/1956	 
Mozambique [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165383&clang=_en]	Acceptance	16/08/1976	11/10/1976
Nauru [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028016521b&clang=_en]	Acceptance	25/07/1996	25/07/1996
Nepal [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028016546e&clang=_en]	Acceptance	01/05/1953	 
Netherlands [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801656b1&clang=_en]	Acceptance	01/01/1947	04/11/1946
New Zealand [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028016578e&clang=_en]	Acceptance	06/03/1946	04/11/1946
Nicaragua [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801655c4&clang=_en]	Acceptance	22/02/1952	 
Nicaragua [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801656e4&clang=_en]	Signature	16/11/1945	 
Niger [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028016551d&clang=_en]	Acceptance	10/11/1960	10/11/1960
Nigeria [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165523&clang=_en]	Acceptance	14/11/1960	14/11/1960
Niue [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028016522a&clang=_en]	Acceptance	26/10/1993	26/10/1993
None [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028016531f&clang=_en]	Amendment	04/10/1980	 
None [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028016543b&clang=_en]	Amendment	01/12/1947	01/12/1947
None [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165404&clang=_en]	Amendment	03/12/1958	03/12/1958
None [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801653dc&clang=_en]	Amendment	04/11/1968	04/11/1968
None [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165431&clang=_en]	Amendment	05/10/1949	05/10/1949
None [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028016541d&clang=_en]	Amendment	05/12/1952	05/12/1952
None [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165368&clang=_en]	Amendment	08/11/1976	08/11/1976
None [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028016540e&clang=_en]	Amendment	08/12/1954	08/12/1954
None [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165418&clang=_en]	Amendment	08/12/1954	08/12/1954
None [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165409&clang=_en]	Amendment	10/11/1956	10/11/1956
None [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165436&clang=_en]	Amendment	10/12/1948	10/12/1948
None [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165427&clang=_en]	Amendment	11/07/1951	11/07/1951
None [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165422&clang=_en]	Amendment	11/07/1951	11/07/1951
None [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028016542c&clang=_en]	Amendment	15/06/1950	15/06/1950
None [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801653ff&clang=_en]	Amendment	15/11/1962	15/11/1962
None [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165413&clang=_en]	Amendment	22/11/1954	22/11/1954
None [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165388&clang=_en]	Amendment	24/10/1972	24/10/1972
None [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028016538e&clang=_en]	Amendment	30/10/1972	30/10/1972
Norway [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028016574e&clang=_en]	Acceptance	08/08/1946	04/11/1946
Oman [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801653cd&clang=_en]	Acceptance	16/12/1971	10/02/1972
Pakistan [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165628&clang=_en]	Acceptance	14/09/1949	 
Palau [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028016520c&clang=_en]	Acceptance	20/09/1999	20/09/1999
Panama [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028016560f&clang=_en]	Acceptance	10/01/1950	 
Panama [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801656df&clang=_en]	Signature	16/11/1945	 
Papua New Guinea [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028016537e&clang=_en]	Acceptance	04/10/1976	04/10/1976
Paraguay [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165597&clang=_en]	Signature	20/06/1955	 
Paraguay [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028016559c&clang=_en]	Acceptance	20/06/1955	 
Peru [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801656c3&clang=_en]	Acceptance	21/11/1946	04/11/1946
Philippines [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028016569f&clang=_en]	Acceptance	21/11/1946	21/11/1946
Poland [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801656ab&clang=_en]	Acceptance	06/11/1946	06/11/1946
Portugal [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028016544f&clang=_en]	Acceptance	11/03/1965	 
Portugal [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028016536e&clang=_en]	Acceptance	11/09/1974	11/09/1974
Portugal [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165374&clang=_en]	Withdrawal	07/07/1971	31/12/1972
Qatar [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801653d2&clang=_en]	Acceptance	27/01/1972	27/01/1972
Republic of Moldova [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801652b8&clang=_en]	Acceptance	27/05/1992	27/05/1992
Republic of Moldova [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165272&clang=_en]	Acceptance	27/05/1992	27/05/1992
Romania [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028016557e&clang=_en]	Acceptance	27/07/1956	 
Russian Federation [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801652c4&clang=_en]	Notification	30/12/1991	 
Rwanda [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165491&clang=_en]	Signature	07/11/1962	 
Rwanda [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028016548c&clang=_en]	Acceptance	07/11/1962	07/11/1962
Samoa [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028016531a&clang=_en]	Acceptance	03/04/1981	03/04/1981
San Marino [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028016539f&clang=_en]	Acceptance	12/11/1974	12/11/1974
Sao Tome and Principe [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165338&clang=_en]	Acceptance	22/01/1980	22/01/1980
Saudi Arabia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165788&clang=_en]	Acceptance	30/04/1946	04/11/1946
Senegal [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801654d8&clang=_en]	Acceptance	10/11/1960	10/11/1960
Seychelles [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165379&clang=_en]	Acceptance	18/01/1976	18/10/1976
Sierra Leone [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801654b9&clang=_en]	Acceptance	28/03/1962	 
Singapore [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801df079&clang=_en]	Acceptance	08/10/2007	08/10/2007
Singapore [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028016544a&clang=_en]	Acceptance	10/10/1965	28/10/1965
Singapore [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165445&clang=_en]	Signature	28/10/1965	28/10/1965
Singapore [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801652ed&clang=_en]	Exclusion	10/12/1984	31/12/1985
Slovakia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165266&clang=_en]	Acceptance	09/02/1993	09/02/1993
Slovenia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028016526c&clang=_en]	Acceptance	27/05/1992	27/05/1992
Solomon Islands [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165248&clang=_en]	Acceptance	07/09/1993	07/09/1993
Somalia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801654fa&clang=_en]	Acceptance	15/11/1960	15/11/1960
Southern Yemen [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801653eb&clang=_en]	Acceptance	15/10/1968	 
Spain [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801655bf&clang=_en]	Acceptance	30/01/1953	 
St. Christopher and Nevis [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165301&clang=_en]	Acceptance	26/10/1983	26/10/1983
St. Lucia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165333&clang=_en]	Acceptance	06/03/1980	06/03/1980
St. Vincent and the Grenadines [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165306&clang=_en]	Acceptance	15/02/1983	15/02/1983
Sudan [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165565&clang=_en]	Acceptance	26/11/1956	 
Suriname [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165393&clang=_en]	Acceptance	08/04/1976	16/07/1976
Swaziland [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165359&clang=_en]	Acceptance	25/01/1978	25/01/1978
Sweden [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165619&clang=_en]	Signature	23/01/1950	 
Sweden [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028016560a&clang=_en]	Acceptance	23/01/1950	 
Switzerland [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165643&clang=_en]	Acceptance	28/01/1949	28/01/1949
Syria [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801656cf&clang=_en]	Acceptance	16/11/1946	16/11/1946
Tajikistan [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165254&clang=_en]	Acceptance	06/04/1993	06/04/1993
Tanganyika [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801654be&clang=_en]	Acceptance	06/03/1962	 
Thailand [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028016564f&clang=_en]	Signature	29/12/1948	 
Thailand [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165649&clang=_en]	Acceptance	29/12/1948	01/01/1949
The former Yugoslav Republic of Macedonia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028016523c&clang=_en]	Acceptance	28/06/1993	28/06/1993
Togo [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801654ee&clang=_en]	Acceptance	17/11/1960	17/11/1960
Tonga [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165324&clang=_en]	Acceptance	29/09/1980	29/09/1980
Trinidad and Tobago [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801654a5&clang=_en]	Signature	02/11/1962	 
Trinidad and Tobago [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801654a0&clang=_en]	Acceptance	02/11/1962	02/11/1962
Tunisia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165574&clang=_en]	Signature	09/10/1956	 
Tunisia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028016556f&clang=_en]	Acceptance	08/11/1956	 
Turkey [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028016575a&clang=_en]	Acceptance	06/07/1946	04/11/1946
Turkmenistan [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165242&clang=_en]	Acceptance	17/08/1993	17/08/1993
Tuvalu [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801652c9&clang=_en]	Acceptance	21/10/1991	21/10/1991
Uganda [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165487&clang=_en]	Signature	09/11/1962	 
Uganda [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165482&clang=_en]	Acceptance	04/11/1962	09/11/1962
Ukrainian Soviet Socialist Republic [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801655a1&clang=_en]	Acceptance	12/05/1954	 
Union of South Africa [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165782&clang=_en]	Acceptance	03/06/1946	04/11/1946
Union of Soviet Socialist Republics [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801655ab&clang=_en]	Acceptance	21/04/1954	 
United Arab Emirates [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801653c8&clang=_en]	Acceptance	20/04/1972	 
United Kingdom of Great Britain and Northern Ireland [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165216&clang=_en]	Acceptance	01/07/1997	01/07/1997
United Kingdom of Great Britain and Northern Ireland [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165794&clang=_en]	Acceptance	20/02/1946	04/11/1946
United Kingdom of Great Britain and Northern Ireland [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801652f2&clang=_en]	Exclusion	05/12/1984	31/12/1985
United Nations (United Nations Council for Namibia) [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028016534f&clang=_en]	Acceptance	02/11/1978	02/11/1978
United States of America [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165733&clang=_en]	Acceptance	30/09/1946	04/11/1946
United States of America [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801652f7&clang=_en]	Exclusion	28/12/1983	31/12/1984
Upper Volta [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165506&clang=_en]	Signature	10/11/1960	 
Upper Volta [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165500&clang=_en]	Acceptance	14/11/1960	14/11/1960
Uruguay [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801656da&clang=_en]	Signature	16/11/1945	 
Uruguay [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165681&clang=_en]	Acceptance	08/11/1947	 
Uzbekistan [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165230&clang=_en]	Acceptance	26/10/1993	26/10/1993
Vanuatu [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165225&clang=_en]	Acceptance	10/02/1994	10/02/1994
Venezuela [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801656bd&clang=_en]	Acceptance	25/11/1946	25/11/1946
Viet Nam [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801654cd&clang=_en]	Acceptance	06/07/1951	 
Yemen Arab Republic [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801654b4&clang=_en]	Signature	08/02/1962	 
Yemen Arab Republic [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801654af&clang=_en]	Acceptance	02/04/1962	02/04/1962
Yugoslavia (Federal Republic of) [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165200&clang=_en]	Acceptance	20/12/2000	20/12/2000
Yugoslavia (Socialist Federal Republic of) [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801656d5&clang=_en]	Signature	16/11/1945	 
Yugoslavia (Socialist Federal Republic of) [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165605&clang=_en]	Acceptance	31/03/1950	 
Zambia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028016545a&clang=_en]	Acceptance	09/11/1964	 
Zimbabwe [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280165329&clang=_en]	Acceptance	22/09/1980	22/09/1980
[https://treaties.un.org/Pages/showDetails.aspx?objid=08000002800f3586&clang=_en]
[2022-08-14T20:17:02Z]


Registration Number	17512	
Title	Protocol additional to the Geneva Conventions of 12 August 1949, and relating to the protection of victims of international armed conflicts (Protocol I)	
Participant(s)	
Submitter	Switzerland	
Places/dates of conclusion	Place	Date	Geneva	08/06/1977	
EIF information	7 December 1978 	
Authentic texts	Spanish	Russian	French	English	Chinese	Arabic	
Attachments	with annexes	 and Final Act	 and resolutions	
ICJ information	
Depositary	Government of Switzerland	
Registration Date	Switzerland 23 January 1979	
Subject terms	War	Protocols to the Geneva Conventions	Legal matters	Humanitarian matters	Human rights	Geneva Conventions (with Protocols)	
Agreement type	Multilateral	
UNTS Volume Number	 1125 (p.3)	
Publication format	Full	
Certificate Of Registration	
Text document(s)	volume-1125-I-17512-English.pdf [https://treaties.un.org/doc/Publication/UNTS/Volume%201125/volume-1125-I-17512-English.pdf]	volume-1125-I-17512-French.pdf [https://treaties.un.org/doc/Publication/UNTS/Volume%201125/volume-1125-I-17512-French.pdf]	volume-1125-I-17512-Other.pdf [https://treaties.un.org/doc/Publication/UNTS/Volume%201125/volume-1125-I-17512-Other.pdf]	
Volume In PDF	v1125.pdf [https://treaties.un.org/doc/Publication/UNTS/Volume%201125/v1125.pdf]	
Map(s)	
Corrigendum/Addendum

Participant	Action	Date of Notification/Deposit	Date of Effect
Albania [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f37b8&clang=_en]	Accession	16/07/1993	16/01/1994
Algeria [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3905&clang=_en]	Accession	16/08/1989	16/02/1990
Algeria [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3684&clang=_en]	Declaration	16/08/1989	20/11/1990
Angola [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3a42&clang=_en]	Accession	20/09/1984	20/03/1985
Antigua and Barbuda [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f39bc&clang=_en]	Accession	06/10/1986	06/04/1987
Argentina [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3732&clang=_en]	Declaration	11/10/1996	 
Argentina [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f39a2&clang=_en]	Accession	26/11/1986	26/05/1987
Armenia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f37c4&clang=_en]	Accession	07/06/1993	07/12/1993
Australia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3c8a&clang=_en]	Signature	 	 
Australia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3887&clang=_en]	Ratification	21/06/1991	21/12/1991
Australia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f380b&clang=_en]	Declaration	23/09/1992	23/09/1992
Austria [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f36d7&clang=_en]	Notification	28/03/1980	 
Austria [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3c85&clang=_en]	Signature	08/06/1977	 
Austria [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3abd&clang=_en]	Ratification	13/08/1982	13/02/1983
Austria [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f36bf&clang=_en]	Declaration	13/08/1982	20/11/1990
Bahamas [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3b0f&clang=_en]	Accession	10/04/1980	10/10/1980
Bahrain [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f39aa&clang=_en]	Accession	30/10/1986	30/04/1987
Bangladesh [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3b02&clang=_en]	Accession	08/09/1980	08/03/1981
Barbados [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f38ca&clang=_en]	Accession	19/02/1990	19/08/1990
Belgium [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3998&clang=_en]	Declaration	27/03/1987	 
Belgium [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3c80&clang=_en]	Signature	 	 
Belgium [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f39d4&clang=_en]	Ratification	20/05/1986	20/11/1986
Belgium [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f36b7&clang=_en]	Declaration	27/03/1987	20/11/1990
Belize [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3a36&clang=_en]	Accession	29/06/1984	29/12/1984
Benin [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3c7b&clang=_en]	Signature	 	 
Benin [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f39ce&clang=_en]	Accession	28/05/1986	28/11/1986
Bolivia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3a66&clang=_en]	Accession	08/12/1983	08/06/1984
Bolivia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3811&clang=_en]	Declaration	10/08/1992	10/08/1992
Bosnia and Herzegovina [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f37fe&clang=_en]	Succession	31/12/1992	06/03/1992
Botswana [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3b2e&clang=_en]	Accession	23/05/1979	23/11/1979
Brazil [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f379b&clang=_en]	Declaration	23/11/1993	 
Brazil [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3830&clang=_en]	Accession	05/05/1992	05/11/1992
Brunei Darussalam [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3870&clang=_en]	Accession	14/10/1991	14/04/1992
Bulgaria [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3c71&clang=_en]	Signature	 	 
Bulgaria [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f38f3&clang=_en]	Ratification	26/09/1989	26/03/1990
Burkina Faso [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f35dd&clang=_en]	Declaration	24/05/2004	 
Burkina Faso [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3974&clang=_en]	Ratification	20/10/1987	20/04/1988
Burundi [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f37be&clang=_en]	Accession	10/06/1993	10/12/1993
Byelorussian Soviet Socialist Republic [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3c76&clang=_en]	Signature	 	 
Byelorussian Soviet Socialist Republic [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3694&clang=_en]	Declaration	23/10/1989	20/11/1990
Byelorussian Soviet Socialist Republic [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f38e5&clang=_en]	Ratification	23/10/1989	23/04/1990
Cambodia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3720&clang=_en]	Accession	14/01/1998	14/07/1998
Cameroon [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3a4e&clang=_en]	Accession	16/03/1984	16/09/1984
Canada [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3c6c&clang=_en]	Signature	 	 
Canada [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f38b4&clang=_en]	Ratification	20/11/1990	20/05/1991
Canada [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3688&clang=_en]	Declaration	20/11/1990	20/11/1990
Cape Verde [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3779&clang=_en]	Accession	16/03/1995	16/09/1995
Central African Republic [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3a2a&clang=_en]	Accession	17/07/1984	17/01/1985
Chad [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f372c&clang=_en]	Accession	17/01/1997	17/07/1997
Chile [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3c67&clang=_en]	Signature	 	 
Chile [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3896&clang=_en]	Ratification	24/04/1991	24/10/1991
China [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3a8c&clang=_en]	Territorial application	14/04/1999	01/07/1997
China [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3a85&clang=_en]	Accession	14/09/1983	14/03/1984
China [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f361c&clang=_en]	Declaration	31/05/2000	20/12/1999
Colombia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3748&clang=_en]	Declaration	17/04/1996	 
Colombia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f37a0&clang=_en]	Accession	01/09/1993	01/03/1994
Comoros [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f39e8&clang=_en]	Accession	21/11/1985	21/05/1986
Congo [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3a6c&clang=_en]	Accession	10/11/1983	10/05/1984
Cook Islands [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3962&clang=_en]	Exclusion	 	 
Costa Rica [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3609&clang=_en]	Declaration	09/12/1999	 
Costa Rica [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3a60&clang=_en]	Accession	15/12/1983	15/06/1984
Côte d'Ivoire [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f38f9&clang=_en]	Ratification	20/09/1989	20/03/1990
Croatia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3842&clang=_en]	Succession	11/05/1992	08/10/1991
Cuba [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3ab7&clang=_en]	Accession	25/11/1982	25/05/1983
Cyprus [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3c62&clang=_en]	Signature	 	 
Cyprus [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3b41&clang=_en]	Ratification	01/06/1979	01/12/1979
Czech Republic [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f37f2&clang=_en]	Succession	05/02/1993	01/01/1993
Czechoslovakia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3c5d&clang=_en]	Signature	 	 
Czechoslovakia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f38d0&clang=_en]	Ratification	14/02/1990	14/08/1990
Democratic People's Republic of Korea [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f394c&clang=_en]	Accession	09/03/1988	09/09/1988
Denmark [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3c58&clang=_en]	Signature	 	 
Denmark [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3ac4&clang=_en]	Ratification	17/06/1982	17/12/1982
Denmark [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f36c3&clang=_en]	Declaration	17/06/1982	20/11/1990
Djibouti [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f389d&clang=_en]	Accession	08/04/1991	08/10/1991
Dominica [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f374d&clang=_en]	Accession	25/04/1996	25/10/1996
Dominican Republic [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3785&clang=_en]	Accession	26/05/1994	26/11/1994
Ecuador [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3c4e&clang=_en]	Signature	 	 
Ecuador [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3b4d&clang=_en]	Ratification	10/04/1979	10/10/1979
Egypt [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3c49&clang=_en]	Signature	 	 
Egypt [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f37ec&clang=_en]	Ratification	09/10/1992	09/04/1993
El Salvador [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3c44&clang=_en]	Signature	 	 
El Salvador [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3b53&clang=_en]	Ratification	23/11/1978	23/05/1979
Equatorial Guinea [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f39c8&clang=_en]	Accession	24/07/1986	24/01/1987
Estonia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f37e0&clang=_en]	Accession	18/01/1993	18/07/1993
Federal Republic of Germany [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3c3a&clang=_en]	Signature	 	 
Federal Republic of Germany [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f367e&clang=_en]	Ratification	14/02/1991	14/08/1991
Finland [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3c3f&clang=_en]	Signature	 	 
Finland [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f399d&clang=_en]	Withdrawal of reservation	16/02/1987	 
Finland [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3b08&clang=_en]	Ratification	07/08/1980	07/02/1981
Finland [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f36cf&clang=_en]	Declaration	07/08/1980	20/11/1990
Gabon [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3b15&clang=_en]	Accession	08/04/1980	08/10/1980
Gambia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3933&clang=_en]	Accession	12/01/1989	12/07/1989
Georgia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f37a6&clang=_en]	Accession	14/09/1993	14/03/1994
German Democratic Republic [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3c53&clang=_en]	Signature	 	 
Germany [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f388e&clang=_en]	Ratification	14/02/1991	14/08/1991
Ghana [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3c35&clang=_en]	Signature	 	 
Ghana [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3c94&clang=_en]	Ratification	28/02/1978	07/12/1978
Greece [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f36ed&clang=_en]	Declaration	04/02/1998	 
Greece [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3927&clang=_en]	Ratification	31/03/1989	30/09/1989
Grenada [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3674&clang=_en]	Accession	23/09/1998	23/03/1999
Guatemala [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3c30&clang=_en]	Signature	 	 
Guatemala [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f397a&clang=_en]	Ratification	19/10/1987	19/04/1988
Guinea [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3796&clang=_en]	Declaration	20/12/1993	 
Guinea [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3a30&clang=_en]	Accession	11/07/1984	11/01/1985
Guinea-Bissau [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f39b6&clang=_en]	Accession	21/10/1986	21/04/1987
Guyana [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f396e&clang=_en]	Accession	18/01/1988	18/07/1988
Haiti [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801dbd38&clang=_en]	Accession	20/12/2006	20/06/2007
Holy See [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3c26&clang=_en]	Signature	 	 
Holy See [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f39ee&clang=_en]	Ratification	21/11/1985	21/05/1986
Honduras [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3c2b&clang=_en]	Signature	 	 
Honduras [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f377f&clang=_en]	Ratification	16/02/1995	16/08/1995
Hungary [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3c21&clang=_en]	Signature	 	 
Hungary [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3882&clang=_en]	Declaration	23/09/1991	 
Hungary [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3921&clang=_en]	Ratification	12/04/1989	12/10/1989
Iceland [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3c17&clang=_en]	Signature	 	 
Iceland [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3990&clang=_en]	Ratification	10/04/1987	10/10/1987
Iceland [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f36b3&clang=_en]	Declaration	10/04/1987	20/11/1990
Iran [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3c12&clang=_en]	Signature	 	 
Ireland [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3c0d&clang=_en]	Signature	 	 
Ireland [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f364f&clang=_en]	Ratification	19/05/1999	19/11/1999
Israel [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3a93&clang=_en]	Declaration	14/06/1983	 
Israel [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3a5b&clang=_en]	Declaration	04/01/1984	 
Israel [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3a1f&clang=_en]	Declaration	02/08/1984	 
Italy [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3c08&clang=_en]	Signature	 	 
Italy [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f36bb&clang=_en]	Declaration	27/02/1986	20/11/1990
Italy [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f39db&clang=_en]	Ratification	27/02/1986	27/08/1986
Ivory Coast [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3c1c&clang=_en]	Signature	 	 
Jamaica [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f39c2&clang=_en]	Accession	29/07/1986	29/01/1987
Japan [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f35c0&clang=_en]	Accession	31/08/2004	28/02/2005
Jordan [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3c03&clang=_en]	Signature	 	 
Jordan [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3b47&clang=_en]	Ratification	01/05/1979	01/11/1979
Kazakhstan [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f381d&clang=_en]	Succession	05/05/1992	21/12/1991
Kenya [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f366e&clang=_en]	Accession	23/02/1999	23/08/1999
Kuwait [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3a0d&clang=_en]	Accession	17/01/1985	17/07/1985
Kyrgyzstan [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3817&clang=_en]	Succession	18/09/1992	21/12/1991
Lao People's Democratic Republic [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3bf9&clang=_en]	Signature	 	 
Lao People's Democratic Republic [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f370c&clang=_en]	Declaration	30/01/1998	 
Lao People's Democratic Republic [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3afc&clang=_en]	Ratification	18/11/1980	18/05/1981
Latvia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f385f&clang=_en]	Accession	24/12/1991	24/06/1992
Lebanon [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3726&clang=_en]	Accession	23/07/1997	23/01/1998
Lesotho [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f378b&clang=_en]	Accession	20/05/1994	20/11/1994
Liberia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3952&clang=_en]	Accession	30/06/1988	30/12/1988
Libyan Arab Jamahiriya [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3c8f&clang=_en]	Accession	07/06/1978	07/12/1978
Liechtenstein [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3bf4&clang=_en]	Signature	 	 
Liechtenstein [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f38dd&clang=_en]	Ratification	10/08/1989	10/02/1990
Liechtenstein [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f369c&clang=_en]	Declaration	10/08/1989	20/11/1990
Lithuania [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f35f9&clang=_en]	Accession	13/07/2000	13/01/2001
Luxembourg [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f37d5&clang=_en]	Declaration	12/05/1993	 
Luxembourg [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3bef&clang=_en]	Signature	 	 
Luxembourg [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f38ff&clang=_en]	Ratification	29/08/1989	28/02/1990
Madagascar [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f37d0&clang=_en]	Declaration	27/07/1993	 
Madagascar [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3bea&clang=_en]	Signature	 	 
Madagascar [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f383c&clang=_en]	Ratification	08/05/1992	08/11/1992
Malawi [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3876&clang=_en]	Accession	07/10/1991	07/04/1992
Maldives [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f387c&clang=_en]	Accession	03/09/1991	03/03/1992
Mali [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f392d&clang=_en]	Accession	08/02/1989	08/08/1989
Malta [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3913&clang=_en]	Accession	17/04/1989	17/10/1989
Malta [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f36a4&clang=_en]	Declaration	17/04/1989	20/11/1990
Mauritania [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3b1b&clang=_en]	Accession	14/03/1980	14/09/1980
Mauritius [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3ad1&clang=_en]	Accession	22/03/1982	22/09/1982
Mexico [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3a9e&clang=_en]	Accession	10/03/1983	10/09/1983
Micronesia (Federated States of) [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3767&clang=_en]	Accession	19/09/1995	19/03/1996
Monaco [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3616&clang=_en]	Accession	07/01/2000	07/07/2000
Mongolia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3be5&clang=_en]	Signature	 	 
Mongolia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3753&clang=_en]	Ratification	06/12/1995	06/06/1996
Montenegro [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3594&clang=_en]	Declaration	02/08/2006	 
Montenegro [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f35a8&clang=_en]	Accession	02/08/2006	02/02/2007
Morocco [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3bfe&clang=_en]	Signature	 	 
Mozambique [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3a98&clang=_en]	Accession	14/03/1983	14/09/1983
Namibia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3791&clang=_en]	Declaration	17/06/1994	 
Namibia (United Nations Council for Namibia) [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3a72&clang=_en]	Accession	18/10/1983	18/04/1984
Nauru [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f35ae&clang=_en]	Accession	27/06/2006	27/12/2006
Netherlands [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3be0&clang=_en]	Signature	 	 
Netherlands [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f36ac&clang=_en]	Ratification	26/06/1987	20/11/1990
Netherlands [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3987&clang=_en]	Ratification	26/06/1987	26/12/1987
New Zealand [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3bcc&clang=_en]	Signature	 	 
New Zealand [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3967&clang=_en]	Ratification	08/02/1988	08/08/1988
New Zealand [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f36a8&clang=_en]	Declaration	08/02/1988	20/11/1990
Nicaragua [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3bdb&clang=_en]	Signature	 	 
Nicaragua [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f363a&clang=_en]	Ratification	19/07/1999	19/01/2000
Niger [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3bd6&clang=_en]	Signature	 	 
Niger [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3b3b&clang=_en]	Ratification	08/06/1979	08/12/1979
Nigeria [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3939&clang=_en]	Accession	10/10/1988	10/04/1989
Niue Island [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f395d&clang=_en]	Exclusion	 	 
None [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3af7&clang=_en]	Rectification	20/04/1981	08/05/1981
Norway [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3bd1&clang=_en]	Signature	 	 
Norway [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3aea&clang=_en]	Ratification	14/12/1981	14/06/1982
Norway [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f36cb&clang=_en]	Declaration	14/12/1981	20/11/1990
Oman [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3a54&clang=_en]	Accession	29/03/1984	29/09/1984
Pakistan [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3bc7&clang=_en]	Signature	 	 
Palau [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f373c&clang=_en]	Accession	25/06/1996	25/12/1996
Panama [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3bc2&clang=_en]	Signature	 	 
Panama [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3628&clang=_en]	Declaration	26/10/1999	 
Panama [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f376d&clang=_en]	Ratification	18/09/1995	18/03/1996
Paraguay [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f36fc&clang=_en]	Declaration	30/01/1998	 
Paraguay [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f38ae&clang=_en]	Accession	30/11/1990	30/05/1991
Peru [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3bbd&clang=_en]	Signature	 	 
Peru [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f390d&clang=_en]	Ratification	14/07/1989	14/01/1990
Philippines [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3bb8&clang=_en]	Signature	 	 
Poland [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3bb3&clang=_en]	Signature	 	 
Poland [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3805&clang=_en]	Declaration	02/10/1992	02/10/1992
Poland [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3865&clang=_en]	Ratification	23/10/1991	23/04/1992
Portugal [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3bae&clang=_en]	Signature	 	 
Portugal [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3622&clang=_en]	Declaration	22/11/1999	22/11/1999
Portugal [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3836&clang=_en]	Ratification	27/05/1992	27/11/1992
Qatar [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f386b&clang=_en]	Declaration	24/09/1991	 
Qatar [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3945&clang=_en]	Accession	05/04/1988	05/10/1988
Republic of Korea [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3ba9&clang=_en]	Signature	 	 
Republic of Korea [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f35e9&clang=_en]	Declaration	16/04/2004	 
Republic of Korea [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3ae3&clang=_en]	Ratification	15/01/1982	15/07/1982
Republic of Moldova [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f37ca&clang=_en]	Accession	24/05/1993	24/11/1993
Romania [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3ba4&clang=_en]	Signature	 	 
Romania [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f38be&clang=_en]	Ratification	21/06/1990	21/12/1990
Russian Federation [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280563aca&clang=_en]	Withdrawal of declaration	23/10/2019	 
Russian Federation [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3829&clang=_en]	Notification	13/01/1992	 
Rwanda [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3a13&clang=_en]	Accession	19/11/1984	19/05/1985
Samoa [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3a24&clang=_en]	Accession	23/08/1984	23/02/1985
San Marino [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3b9a&clang=_en]	Signature	 	 
Sao Tome and Principe [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3742&clang=_en]	Accession	05/07/1996	05/01/1997
Saudi Arabia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3980&clang=_en]	Accession	21/08/1987	21/02/1988
Senegal [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3b9f&clang=_en]	Signature	 	 
Senegal [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3a01&clang=_en]	Ratification	07/05/1985	07/11/1985
Seychelles [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3855&clang=_en]	Declaration	22/05/1992	 
Seychelles [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3a19&clang=_en]	Accession	08/11/1984	08/05/1985
Sierra Leone [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f39b0&clang=_en]	Accession	21/10/1986	21/04/1987
Slovakia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f37da&clang=_en]	Succession	02/04/1993	01/01/1993
Slovenia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3849&clang=_en]	Succession	26/03/1992	25/06/1991
Solomon Islands [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f393f&clang=_en]	Accession	19/09/1988	19/03/1989
South Africa [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3a49&clang=_en]	Declaration	12/03/1984	 
South Africa [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f375b&clang=_en]	Accession	21/11/1995	21/05/1996
Spain [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f35d3&clang=_en]	Communication	04/05/2004	 
Spain [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3b95&clang=_en]	Signature	 	 
Spain [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f36a0&clang=_en]	Declaration	21/04/1989	20/11/1990
Spain [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f391a&clang=_en]	Ratification	21/04/1989	21/10/1989
St. Christopher and Nevis [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f39e2&clang=_en]	Accession	14/02/1986	14/08/1986
St. Lucia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3ab1&clang=_en]	Accession	07/10/1982	07/04/1983
St. Vincent and the Grenadines [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3a7f&clang=_en]	Accession	08/04/1983	08/10/1983
State of Palestine [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002804fcda2&clang=_en]	Declaration	26/03/2018	 
State of Palestine [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280415c55&clang=_en]	Accession	02/04/2014	02/04/2014
Sudan [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f35b4&clang=_en]	Accession	07/03/2006	07/09/2006
Suriname [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f39f5&clang=_en]	Accession	16/12/1985	16/06/1986
Swaziland [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3761&clang=_en]	Accession	02/11/1995	02/05/1996
Sweden [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3b90&clang=_en]	Signature	 	 
Sweden [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f36d3&clang=_en]	Declaration	31/08/1979	20/11/1990
Sweden [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3b27&clang=_en]	Ratification	31/08/1979	29/02/1980
Switzerland [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3ade&clang=_en]	Withdrawal of reservation	17/06/2005	 
Switzerland [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3b8b&clang=_en]	Signature	 	 
Switzerland [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3ad7&clang=_en]	Ratification	17/02/1982	17/08/1982
Switzerland [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f36c7&clang=_en]	Declaration	17/02/1982	20/11/1990
Syrian Arab Republic [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3a78&clang=_en]	Accession	14/11/1983	14/05/1984
Tajikistan [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f371b&clang=_en]	Declaration	10/09/1997	 
Tajikistan [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f37f8&clang=_en]	Succession	13/01/1993	21/12/1991
The former Yugoslav Republic of Macedonia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3737&clang=_en]	Declaration	18/10/1996	 
The former Yugoslav Republic of Macedonia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f37b2&clang=_en]	Succession	01/09/1993	08/09/1991
Timor-Leste [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f35ba&clang=_en]	Accession	12/04/2005	12/10/2005
Togo [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3b86&clang=_en]	Signature	 	 
Togo [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f385a&clang=_en]	Declaration	21/11/1991	 
Togo [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3a3c&clang=_en]	Ratification	21/06/1984	21/12/1984
Tokelau Islands [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3958&clang=_en]	Exclusion	 	 
Tunisia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3b81&clang=_en]	Signature	 	 
Tunisia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3b21&clang=_en]	Ratification	09/08/1979	09/02/1980
Turkmenistan [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3823&clang=_en]	Succession	10/04/1992	26/12/1991
Uganda [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f38a3&clang=_en]	Accession	13/03/1991	13/09/1991
Ukrainian Soviet Socialist Republic [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3b77&clang=_en]	Signature	 	 
Ukrainian Soviet Socialist Republic [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3690&clang=_en]	Declaration	25/01/1990	20/11/1990
Ukrainian Soviet Socialist Republic [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f38d6&clang=_en]	Ratification	25/01/1990	25/07/1990
Union of Soviet Socialist Republics [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3b6d&clang=_en]	Signature	 	 
Union of Soviet Socialist Republics [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3698&clang=_en]	Declaration	29/09/1989	20/11/1990
Union of Soviet Socialist Republics [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f38ec&clang=_en]	Ratification	29/09/1989	29/03/1990
United Arab Emirates [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3850&clang=_en]	Declaration	06/03/1992	 
United Arab Emirates [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3aa4&clang=_en]	Accession	09/03/1983	09/09/1983
United Kingdom of Great Britain and Northern Ireland [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3640&clang=_en]	Declaration	17/05/1999	 
United Kingdom of Great Britain and Northern Ireland [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3b7c&clang=_en]	Signature	 	 
United Kingdom of Great Britain and Northern Ireland [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f36db&clang=_en]	Ratification	28/01/1998	28/07/1998
United Republic of Tanzania [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3aab&clang=_en]	Accession	15/02/1983	15/08/1983
United States of America [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3b72&clang=_en]	Signature	 	 
Upper Volta [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3b68&clang=_en]	Signature	 	 
Uruguay [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f38a9&clang=_en]	Declaration	17/07/1990	 
Uruguay [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f39fb&clang=_en]	Accession	13/12/1985	13/06/1986
Uruguay [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f368c&clang=_en]	Declaration	17/07/1990	20/11/1990
Uzbekistan [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f37ac&clang=_en]	Accession	08/10/1993	08/04/1994
Vanuatu [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3a07&clang=_en]	Accession	28/02/1985	28/08/1985
Venezuela [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3679&clang=_en]	Accession	23/07/1998	23/01/1999
Viet Nam [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3b63&clang=_en]	Signature	 	 
Viet Nam [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3af1&clang=_en]	Ratification	19/10/1981	19/04/1982
Yemen [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f38c4&clang=_en]	Ratification	17/04/1990	17/10/1990
Yemen Arab Republic [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3b5e&clang=_en]	Signature	 	 
Yugoslavia (Socialist Federal Republic of) [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3b59&clang=_en]	Signature	 	 
Yugoslavia (Socialist Federal Republic of) [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3b34&clang=_en]	Ratification	11/06/1979	11/12/1979
Zaire [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3acb&clang=_en]	Accession	03/06/1982	03/12/1982
Zambia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f3773&clang=_en]	Accession	04/05/1995	04/11/1995
Zimbabwe [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800f37e6&clang=_en]	Accession	19/10/1992	19/04/1993
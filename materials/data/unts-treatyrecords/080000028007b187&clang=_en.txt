[https://treaties.un.org/Pages/showDetails.aspx?objid=080000028007b187&clang=_en]
[2022-08-14T20:10:18Z]


Registration Number	39591	
Title	Convention (No. 184) concerning safety and health in agriculture	
Participant(s)	
Submitter	International Labour Organisation	
Places/dates of conclusion	Place	Date	Geneva	21/06/2001	
EIF information	20 September 2003 , in accordance with article 23	
Authentic texts	French	English	
Attachments	
ICJ information	
Depositary	Director-General of the International Labour Office	
Registration Date	International Labour Organisation 6 October 2003	
Subject terms	Labour	Health and health services	Agriculture	
Agreement type	Multilateral	
UNTS Volume Number	 2227 (p.241)	
Publication format	Full	
Certificate Of Registration	COR-Reg-39591-Sr-50217.pdf [https://treaties.un.org/doc/Treaties/2003/10/20031006%2011-36%20AM/Other%20Documents/COR-Reg-39591-Sr-50217.pdf]	
Text document(s)	
Volume In PDF	v2227.pdf [https://treaties.un.org/doc/Publication/UNTS/Volume%202227/v2227.pdf]	
Map(s)	
Corrigendum/Addendum

Participant	Action	Date of Notification/Deposit	Date of Effect
Antigua and Barbuda [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002805c6ae3&clang=_en]	Ratification	28/07/2021	28/07/2022
Argentina [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028007b1a1&clang=_en]	Ratification	26/06/2006	26/06/2007
Belgium [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002804797db&clang=_en]	Ratification	10/11/2015	10/11/2016
Bosnia and Herzegovina [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002802b5ac9&clang=_en]	Ratification	18/01/2010	18/01/2011
Burkina Faso [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002802b5ab4&clang=_en]	Ratification	28/10/2009	28/10/2010
Fiji [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028022c4c3&clang=_en]	Ratification	28/05/2008	28/05/2009
Finland [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028007b1bf&clang=_en]	Ratification	21/02/2003	21/02/2004
France [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002805c6bcd&clang=_en]	Ratification	26/01/2021	26/01/2022
Ghana [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028032b655&clang=_en]	Ratification	06/06/2011	06/06/2012
Iraq [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002805c6c87&clang=_en]	Ratification	21/05/2021	21/05/2022
Kyrgyzstan [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028007b1b9&clang=_en]	Ratification	10/05/2004	10/05/2005
Luxembourg [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801ebfcf&clang=_en]	Ratification	08/04/2008	08/04/2009
Malawi [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002805752e9&clang=_en]	Ratification	07/11/2019	07/11/2020
Portugal [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002803a62e4&clang=_en]	Ratification	08/11/2012	08/11/2013
Republic of Moldova [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028007b1c5&clang=_en]	Ratification	20/09/2002	20/09/2003
Sao Tome and Principe [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028007b1ad&clang=_en]	Ratification	04/05/2005	04/05/2006
Serbia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028055a2ce&clang=_en]	Ratification	12/03/2019	12/03/2020
Slovakia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028007b1cb&clang=_en]	Ratification	14/06/2002	20/09/2003
Sweden [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028007b1b3&clang=_en]	Ratification	09/06/2004	09/06/2005
Ukraine [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002802b5ac5&clang=_en]	Ratification	01/12/2009	01/12/2010
Uruguay [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028007b1a7&clang=_en]	Ratification	25/05/2005	25/05/2006
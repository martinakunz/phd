[https://treaties.un.org/Pages/showDetails.aspx?objid=08000002800cf3c9&clang=_en]
[2022-08-14T20:16:24Z]


Registration Number	24404	
Title	Convention on Early Notification of a Nuclear Accident	
Participant(s)	
Submitter	International Atomic Energy Agency	
Places/dates of conclusion	Place	Date	Vienna	26/09/1986	
EIF information	27 October 1986 	
Authentic texts	
Attachments	
ICJ information	
Depositary	Director-General of the International Atomic Energy Agency	
Registration Date	International Atomic Energy Agency 7 November 1986	
Subject terms	Nuclear matters	Legal matters	Environment	Energy	Disaster relief	
Agreement type	Multilateral	
UNTS Volume Number	 1439 (p.275)	
Publication format	Full	
Certificate Of Registration	
Text document(s)	volume-1439-I-24404-English.pdf [https://treaties.un.org/doc/Publication/UNTS/Volume%201439/volume-1439-I-24404-English.pdf]	volume-1439-I-24404-French.pdf [https://treaties.un.org/doc/Publication/UNTS/Volume%201439/volume-1439-I-24404-French.pdf]	volume-1439-I-24404-Other.pdf [https://treaties.un.org/doc/Publication/UNTS/Volume%201439/volume-1439-I-24404-Other.pdf]	
Volume In PDF	v1439.pdf [https://treaties.un.org/doc/Publication/UNTS/Volume%201439/v1439.pdf]	
Map(s)	
Corrigendum/Addendum

Participant	Action	Date of Notification/Deposit	Date of Effect
Afghanistan [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf801&clang=_en]	Signature	26/09/1986	 
Albania [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf448&clang=_en]	Accession	30/09/2003	30/10/2003
Algeria [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf422&clang=_en]	Ratification	15/01/2004	15/02/2004
Algeria [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf5d9&clang=_en]	Provisional application	24/09/1987	24/09/1987
Angola [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf41c&clang=_en]	Accession	22/12/2004	22/01/2005
Argentina [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf59a&clang=_en]	Accession	17/01/1990	17/02/1990
Armenia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf4ef&clang=_en]	Accession	24/08/1993	24/09/1993
Australia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf7fc&clang=_en]	Signature	26/09/1986	 
Australia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf62a&clang=_en]	Ratification	22/09/1987	23/10/1987
Austria [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf7f7&clang=_en]	Signature	26/09/1986	 
Austria [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf624&clang=_en]	Ratification	18/02/1988	20/03/1988
Bahrain [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002802e159c&clang=_en]	Accession	05/05/2011	04/06/2011
Bangladesh [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf61e&clang=_en]	Accession	07/01/1988	07/02/1988
Belgium [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf7f2&clang=_en]	Signature	26/09/1986	 
Belgium [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf48b&clang=_en]	Ratification	04/01/1999	04/02/1999
Benin [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028056291d&clang=_en]	Accession	18/09/2019	18/10/2019
Bolivia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf435&clang=_en]	Accession	22/08/2003	21/09/2003
Bosnia and Herzegovina [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf491&clang=_en]	Succession	30/06/1998	01/03/1992
Botswana [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002803058b3&clang=_en]	Accession	11/11/2011	11/12/2011
Brazil [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf7ed&clang=_en]	Signature	26/09/1986	 
Brazil [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf55b&clang=_en]	Ratification	04/12/1990	04/01/1991
Bulgaria [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf7e8&clang=_en]	Signature	26/09/1986	 
Bulgaria [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf4d1&clang=_en]	Withdrawal of reservation	11/05/1994	11/05/1994
Bulgaria [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf617&clang=_en]	Ratification	24/02/1988	26/03/1988
Burkina Faso [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002803f0107&clang=_en]	Accession	07/08/2014	06/09/2014
Byelorussian Soviet Socialist Republic [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf7e3&clang=_en]	Signature	26/09/1986	 
Byelorussian Soviet Socialist Republic [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf6c0&clang=_en]	Ratification	26/01/1987	26/02/1987
Cambodia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028032163a&clang=_en]	Accession	05/04/2012	05/05/2012
Cameroon [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf3ef&clang=_en]	Ratification	17/01/2006	16/02/2006
Canada [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf7de&clang=_en]	Signature	26/09/1986	 
Canada [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf594&clang=_en]	Ratification	18/01/1990	18/02/1990
Chile [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf7d9&clang=_en]	Signature	26/09/1986	 
Chile [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf3f5&clang=_en]	Ratification	15/11/2005	15/12/2005
China [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf7d4&clang=_en]	Signature	26/09/1986	 
China [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf6d2&clang=_en]	Declaration	26/11/1986	 
China [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf610&clang=_en]	Ratification	10/09/1987	11/10/1987
Colombia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf454&clang=_en]	Accession	28/03/2003	28/04/2003
Congo [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002805bca40&clang=_en]	Accession	03/09/2021	03/10/2021
Costa Rica [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf7cf&clang=_en]	Signature	26/09/1986	 
Costa Rica [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf534&clang=_en]	Ratification	16/09/1991	17/10/1991
Côte d'Ivoire [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028058cb04&clang=_en]	Ratification	21/09/2020	21/10/2020
Croatia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf51d&clang=_en]	Succession	29/09/1992	08/10/1991
Cuba [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf7ca&clang=_en]	Signature	26/09/1986	 
Cuba [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf54d&clang=_en]	Ratification	08/01/1991	08/02/1991
Cyprus [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf645&clang=_en]	Accession	04/01/1989	04/02/1989
Czech Republic [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf4f5&clang=_en]	Succession	24/03/1993	01/01/1993
Czechoslovakia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf496&clang=_en]	Withdrawal of reservation	06/06/1991	 
Czechoslovakia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf7c5&clang=_en]	Definitive signature	26/09/1986	27/10/1986
Democratic People's Republic of Korea [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf7c0&clang=_en]	Signature	29/09/1986	 
Denmark [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf7b6&clang=_en]	Definitive signature	26/09/1986	27/10/1986
Dominican Republic [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028029811a&clang=_en]	Accession	29/04/2010	29/05/2010
Ecuador [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280562918&clang=_en]	Accession	16/09/2019	16/10/2019
Egypt [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf7b1&clang=_en]	Signature	26/09/1986	 
Egypt [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf673&clang=_en]	Ratification	06/07/1988	06/08/1988
El Salvador [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf407&clang=_en]	Accession	26/01/2005	26/02/2005
Eritrea [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028058112b&clang=_en]	Accession	13/03/2020	12/04/2020
Estonia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf4c7&clang=_en]	Accession	09/05/1994	09/06/1994
European Atomic Energy Community [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf3e8&clang=_en]	Accession	14/11/2006	14/12/2006
Federal Republic of Germany [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf7ac&clang=_en]	Signature	26/09/1986	 
Federal Republic of Germany [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf5d1&clang=_en]	Ratification	14/09/1989	15/10/1989
Finland [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf7a7&clang=_en]	Signature	26/09/1986	 
Finland [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf6cd&clang=_en]	Approval	11/12/1986	11/01/1987
Food and Agriculture Organization of the United Nations [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf554&clang=_en]	Accession	19/10/1990	19/11/1990
France [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf7a2&clang=_en]	Signature	26/09/1986	 
France [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf63e&clang=_en]	Approval	06/03/1989	06/04/1989
Gabon [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002801df187&clang=_en]	Accession	19/02/2008	20/03/2008
Georgia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002802cedb0&clang=_en]	Accession	06/10/2010	05/11/2010
German Democratic Republic [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf7bb&clang=_en]	Signature	26/09/1986	 
German Democratic Republic [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf6ac&clang=_en]	Ratification	29/04/1987	30/05/1987
Ghana [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028048a02c&clang=_en]	Accession	05/09/2016	05/10/2016
Greece [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf79d&clang=_en]	Signature	26/09/1986	 
Greece [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf53a&clang=_en]	Ratification	06/06/1991	07/07/1991
Guatemala [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf798&clang=_en]	Signature	26/09/1986	 
Guatemala [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf66d&clang=_en]	Ratification	08/08/1988	08/09/1988
Holy See [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf793&clang=_en]	Signature	26/09/1986	 
Hungary [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf78e&clang=_en]	Signature	26/09/1986	 
Hungary [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf5a6&clang=_en]	Withdrawal of reservation	30/11/1989	 
Hungary [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf6a5&clang=_en]	Ratification	10/03/1987	10/04/1987
Iceland [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf789&clang=_en]	Signature	26/09/1986	 
Iceland [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf5cb&clang=_en]	Ratification	27/09/1989	28/10/1989
India [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf784&clang=_en]	Signature	29/09/1986	 
India [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf609&clang=_en]	Ratification	28/01/1988	28/02/1988
India [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf49a&clang=_en]	Ratification	28/01/1988	28/02/1988
Indonesia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf77f&clang=_en]	Signature	26/09/1986	 
Indonesia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf4e8&clang=_en]	Ratification	12/11/1993	13/12/1993
Iran (Islamic Republic of) [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf77a&clang=_en]	Signature	26/09/1986	 
Iraq [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf665&clang=_en]	Ratification	21/07/1988	21/08/1988
Ireland [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf775&clang=_en]	Signature	26/09/1986	 
Ireland [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf52e&clang=_en]	Ratification	13/09/1991	14/10/1991
Islamic Republic of Iran [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf466&clang=_en]	Ratification	09/10/2000	09/11/2000
Israel [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf770&clang=_en]	Signature	26/09/1986	 
Israel [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf58f&clang=_en]	Objection	19/01/1990	 
Israel [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf5a1&clang=_en]	Objection	04/01/1989	 
Israel [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf637&clang=_en]	Ratification	25/05/1989	25/06/1989
Italy [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf76b&clang=_en]	Signature	26/09/1986	 
Italy [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf588&clang=_en]	Ratification	08/02/1990	11/03/1990
Ivory Coast [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf766&clang=_en]	Signature	26/09/1986	 
Japan [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf694&clang=_en]	Acceptance	09/06/1987	10/07/1987
Jordan [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf761&clang=_en]	Signature	02/10/1986	 
Jordan [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf603&clang=_en]	Ratification	11/12/1987	11/01/1988
Kazakhstan [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280298116&clang=_en]	Accession	10/03/2010	09/04/2010
Kuwait [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf44e&clang=_en]	Accession	13/05/2003	13/06/2003
Lao People's Democratic Republic [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280385cc5&clang=_en]	Accession	10/05/2013	09/06/2013
Latvia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf523&clang=_en]	Accession	28/12/1992	28/01/1993
Lebanon [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf75c&clang=_en]	Signature	26/09/1986	 
Lebanon [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf4b2&clang=_en]	Ratification	17/04/1997	18/05/1997
Lesotho [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002803af9a3&clang=_en]	Accession	17/09/2013	17/10/2013
Libyan Arab Jamahiriya [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280260836&clang=_en]	Accession	13/08/2009	12/09/2009
Liechtenstein [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf757&clang=_en]	Signature	26/09/1986	 
Liechtenstein [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf4d6&clang=_en]	Ratification	19/04/1994	20/05/1994
Lithuania [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf4cc&clang=_en]	Accession	16/11/1994	17/12/1994
Luxembourg [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf752&clang=_en]	Signature	26/09/1986	 
Luxembourg [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf47c&clang=_en]	Ratification	26/09/2000	27/10/2000
Madagascar [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002804aa8de&clang=_en]	Accession	03/03/2017	02/04/2017
Malawi [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002805da3c8&clang=_en]	Accession	11/02/2022	13/03/2022
Malaysia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf5fb&clang=_en]	Definitive signature	01/09/1987	02/10/1987
Mali [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf74d&clang=_en]	Signature	02/10/1986	 
Mali [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf3de&clang=_en]	Ratification	01/10/2007	31/10/2007
Mauritania [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002802fb1f6&clang=_en]	Accession	19/09/2011	19/10/2011
Mauritius [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf50e&clang=_en]	Accession	17/08/1992	17/09/1992
Mexico [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf748&clang=_en]	Signature	26/09/1986	 
Mexico [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf681&clang=_en]	Ratification	10/05/1988	10/06/1988
Monaco [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf743&clang=_en]	Signature	26/09/1986	 
Monaco [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf5df&clang=_en]	Approval	19/07/1989	19/08/1989
Mongolia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf699&clang=_en]	Ratification	11/06/1987	12/07/1987
Mongolia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf56e&clang=_en]	Withdrawal of reservation	18/06/1990	18/06/1990
Montenegro [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf3e3&clang=_en]	Succession	21/03/2007	03/06/2006
Morocco [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf73e&clang=_en]	Signature	26/09/1986	 
Morocco [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf4e2&clang=_en]	Ratification	07/10/1993	07/11/1993
Mozambique [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280284350&clang=_en]	Accession	30/10/2009	29/11/2009
Myanmar [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf4a1&clang=_en]	Accession	18/12/1997	18/01/1998
Netherlands [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf736&clang=_en]	Signature	26/09/1986	 
Netherlands [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf528&clang=_en]	Acceptance	23/09/1991	24/10/1991
New Zealand [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf69f&clang=_en]	Accession	11/03/1987	11/04/1987
Nicaragua [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf4db&clang=_en]	Accession	11/11/1993	12/12/1993
Niger [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf731&clang=_en]	Signature	26/09/1986	 
Niger [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002805c563b&clang=_en]	Ratification	19/11/2021	19/12/2021
Nigeria [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf561&clang=_en]	Ratification	10/08/1990	10/09/1990
Norway [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf72c&clang=_en]	Definitive signature	26/09/1986	27/10/1986
Oman [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028026c448&clang=_en]	Accession	09/07/2009	08/08/2009
Pakistan [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf5c4&clang=_en]	Accession	11/09/1989	12/10/1989
Panama [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf727&clang=_en]	Signature	26/09/1986	 
Panama [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf482&clang=_en]	Ratification	01/04/1999	02/05/1999
Paraguay [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf722&clang=_en]	Signature	02/10/1986	 
Paraguay [https://treaties.un.org/Pages/showActionDetails.aspx?objid=080000028035adf5&clang=_en]	Ratification	06/02/2013	08/03/2013
Peru [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf4c1&clang=_en]	Accession	17/07/1995	17/08/1995
Philippines [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf4ad&clang=_en]	Accession	05/05/1997	05/06/1997
Poland [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf71d&clang=_en]	Signature	26/09/1986	 
Poland [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf68e&clang=_en]	Withdrawal of reservation	18/06/1997	18/06/1997
Poland [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf687&clang=_en]	Ratification	24/03/1988	24/04/1988
Portugal [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf718&clang=_en]	Signature	26/09/1986	 
Portugal [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf4ff&clang=_en]	Ratification	30/04/1993	31/05/1993
Qatar [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf3fb&clang=_en]	Accession	04/11/2005	04/12/2005
Republic of Korea [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf57b&clang=_en]	Accession	08/06/1990	09/07/1990
Republic of Moldova [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf4b7&clang=_en]	Accession	07/05/1998	07/06/1998
Romania [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf567&clang=_en]	Accession	12/06/1990	13/07/1990
Russian Federation [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf509&clang=_en]	Notification of continuity	26/12/1991	 
Rwanda [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002805bd9d6&clang=_en]	Accession	23/09/2021	23/10/2021
Saudi Arabia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf5b6&clang=_en]	Accession	03/11/1989	04/12/1989
Senegal [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280219e41&clang=_en]	Ratification	24/12/2008	23/01/2009
Singapore [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf4a8&clang=_en]	Accession	15/12/1997	15/01/1998
Slovakia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf4fa&clang=_en]	Succession	10/02/1993	01/01/1993
Slovenia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf517&clang=_en]	Succession	07/07/1992	25/06/1991
South Africa [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf5f3&clang=_en]	Ratification	10/08/1987	10/09/1987
Spain [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf713&clang=_en]	Signature	26/09/1986	 
Spain [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf5bd&clang=_en]	Ratification	13/09/1989	14/10/1989
Sri Lanka [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf546&clang=_en]	Accession	11/01/1991	11/02/1991
St. Vincent and the Grenadines [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf460&clang=_en]	Accession	18/09/2001	19/10/2001
Sudan [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf70e&clang=_en]	Signature	26/09/1986	 
Sweden [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf709&clang=_en]	Signature	26/09/1986	 
Sweden [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf6b3&clang=_en]	Ratification	27/02/1987	30/03/1987
Switzerland [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf704&clang=_en]	Signature	26/09/1986	 
Switzerland [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf67b&clang=_en]	Ratification	31/05/1988	01/07/1988
Syrian Arab Republic [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280528625&clang=_en]	Ratification	17/09/2018	17/10/2018
Tajikistan [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002802f792a&clang=_en]	Accession	01/09/2011	01/10/2011
Thailand [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf630&clang=_en]	Ratification	21/03/1989	21/04/1989
The former Yugoslav Republic of Macedonia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf4bc&clang=_en]	Succession	20/09/1996	17/11/1991
Tunisia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf6ff&clang=_en]	Signature	24/02/1987	 
Tunisia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf651&clang=_en]	Ratification	24/02/1989	27/03/1989
Turkey [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf6fa&clang=_en]	Signature	26/09/1986	 
Turkey [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf53f&clang=_en]	Ratification	03/01/1991	03/02/1991
Ukrainian Soviet Socialist Republic [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf6f5&clang=_en]	Signature	26/09/1986	 
Ukrainian Soviet Socialist Republic [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf6b9&clang=_en]	Ratification	26/01/1987	26/02/1987
Union of Soviet Socialist Republics [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf5ab&clang=_en]	Objection	09/11/1989	 
Union of Soviet Socialist Republics [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf6f0&clang=_en]	Signature	26/09/1986	 
Union of Soviet Socialist Republics [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf6c7&clang=_en]	Ratification	23/12/1986	24/01/1987
United Arab Emirates [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf5ec&clang=_en]	Accession	02/10/1987	02/11/1987
United Kingdom of Great Britain and Northern Ireland [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf6eb&clang=_en]	Signature	26/09/1986	 
United Kingdom of Great Britain and Northern Ireland [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf581&clang=_en]	Ratification	09/02/1990	12/03/1990
United Republic of Tanzania [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf401&clang=_en]	Accession	27/01/2005	26/02/2005
United States of America [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf6e6&clang=_en]	Signature	26/09/1986	 
United States of America [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf657&clang=_en]	Ratification	19/09/1988	20/10/1988
Uruguay [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf5b0&clang=_en]	Accession	21/12/1989	21/01/1990
Venezuela (Bolivarian Republic of) [https://treaties.un.org/Pages/showActionDetails.aspx?objid=0800000280401e3d&clang=_en]	Accession	22/09/2014	22/10/2014
Viet Nam [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf5e5&clang=_en]	Accession	29/09/1987	30/10/1987
World Health Organization [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf65e&clang=_en]	Accession	10/08/1988	10/09/1988
World Meteorological Organization [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf574&clang=_en]	Accession	17/04/1990	18/05/1990
Yugoslavia (Federal Republic of) [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf504&clang=_en]	Notification of continuity	28/04/1992	 
Yugoslavia (Federal Republic of) [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf45a&clang=_en]	Succession	05/02/2002	27/04/1992
Yugoslavia (Socialist Federal Republic of) [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf6e1&clang=_en]	Signature	27/05/1987	 
Yugoslavia (Socialist Federal Republic of) [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf64b&clang=_en]	Ratification	08/02/1989	11/03/1989
Zaire [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf6dc&clang=_en]	Signature	30/09/1986	 
Zimbabwe [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800cf6d7&clang=_en]	Signature	26/09/1986	 
Zimbabwe [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002805bd356&clang=_en]	Ratification	20/09/2021	20/10/2021
[https://treaties.un.org/Pages/showDetails.aspx?objid=08000002800bc2fb&clang=_en]
[2022-08-14T20:17:16Z]


Registration Number	28395	
Title	Convention on the conservation of migratory species of wild animals	
Participant(s)	
Submitter	Germany	
Places/dates of conclusion	Place	Date	Bonn	23/06/1979	
EIF information	1 November 1983 , in accordance with article XVIII	
Authentic texts	Spanish	Russian	German	French	English	
Attachments	with appendices	
ICJ information	
Depositary	Government of the Federal Republic of Germany	
Registration Date	Germany 8 October 1991	
Subject terms	Wildlife (protection)	Migration	Environment	CMS (migratory species)	Bonn Convention (conservation of migratory species)	Animals	
Agreement type	Multilateral	
UNTS Volume Number	 1651	
Publication format	Full	
Certificate Of Registration	
Text document(s)	Chinese_text-depositary's_official_version.pdf [https://treaties.un.org/doc/Treaties/1991/10/19911007%2001-20%20PM/Chinese_text-depositary's_official_version.pdf]	
Volume In PDF	v1651.pdf [https://treaties.un.org/doc/Publication/UNTS/Volume%201651/v1651.pdf]	
Map(s)	
Corrigendum/Addendum

Participant	Action	Date of Notification/Deposit	Date of Effect
Australia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800bc3bf&clang=_en]	Accession	26/06/1991	01/09/1991
Belgium [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800bc3b9&clang=_en]	Accession	11/07/1990	01/10/1991
Benin [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800bc3b3&clang=_en]	Accession	14/01/1986	01/04/1986
Burkina Faso [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800bc3ad&clang=_en]	Accession	09/10/1989	01/01/1990
Cameroon [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800bc41c&clang=_en]	Ratification	07/09/1981	01/11/1983
Chile [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800bc417&clang=_en]	Accession	15/09/1981	01/11/1983
Denmark [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800bc335&clang=_en]	Withdrawal of declaration	07/04/1989	 
Denmark [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800bc410&clang=_en]	Ratification	05/08/1982	01/11/1983
Egypt [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800bc40b&clang=_en]	Ratification	11/02/1982	01/11/1983
European Economic Community [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800bc405&clang=_en]	Accession	01/08/1983	01/11/1983
Federal Republic of Germany [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800bc39a&clang=_en]	Ratification	31/07/1984	01/10/1984
Finland [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800bc3a7&clang=_en]	Accession	03/10/1988	01/01/1989
France [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800bc3a0&clang=_en]	Approval	23/04/1990	01/07/1990
Ghana [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800bc394&clang=_en]	Accession	19/01/1988	01/04/1988
Hungary [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800bc400&clang=_en]	Accession	12/07/1983	01/11/1983
India [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800bc3fb&clang=_en]	Ratification	04/05/1982	01/11/1983
Ireland [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800bc3f6&clang=_en]	Ratification	05/08/1983	01/11/1983
Israel [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800bc3f1&clang=_en]	Accession	17/05/1983	01/11/1983
Italy [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800bc3ec&clang=_en]	Ratification	26/08/1983	01/11/1983
Luxembourg [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800bc3d6&clang=_en]	Ratification	30/11/1982	01/11/1983
Mali [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800bc38e&clang=_en]	Accession	28/07/1987	01/10/1987
Netherlands [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800bc3e5&clang=_en]	Acceptance	05/06/1981	01/11/1983
Niger [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800bc3e0&clang=_en]	Ratification	03/07/1980	01/11/1983
Nigeria [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800bc388&clang=_en]	Accession	15/10/1986	01/01/1987
None [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800bc31c&clang=_en]	Correction	10/04/1996	 
None [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800bc321&clang=_en]	Rectification	10/03/1994	01/05/1994
None [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800bc32b&clang=_en]	Amendment	11/06/1994	09/09/1994
None [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800bc3c5&clang=_en]	Amendment	12/01/1989	12/01/1989
None [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800bc330&clang=_en]	Amendment	13/09/1991	12/12/1991
None [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800bc326&clang=_en]	Rectification	15/12/1993	15/02/1994
None [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800bc3cb&clang=_en]	Amendment	24/01/1986	24/01/1986
Norway [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800bc33a&clang=_en]	Reservation	11/01/1989	 
Norway [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800bc382&clang=_en]	Approval	30/05/1985	01/08/1985
Pakistan [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800bc37c&clang=_en]	Accession	22/09/1987	01/12/1987
Panama [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800bc376&clang=_en]	Accession	20/02/1989	01/05/1989
Portugal [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800bc3d1&clang=_en]	Ratification	21/01/1981	01/11/1987
Saudi Arabia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800bc36f&clang=_en]	Accession	17/12/1990	01/03/1991
Senegal [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800bc369&clang=_en]	Accession	18/03/1988	01/06/1988
Somalia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800bc363&clang=_en]	Ratification	11/11/1985	01/02/1986
Spain [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800bc35d&clang=_en]	Ratification	12/02/1985	01/05/1985
Sri Lanka [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800bc357&clang=_en]	Ratification	06/06/1990	01/09/1990
Sweden [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800bc3db&clang=_en]	Ratification	09/06/1983	01/11/1983
Tunisia [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800bc351&clang=_en]	Accession	27/05/1987	01/08/1987
United Kingdom of Great Britain and Northern Ireland [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800bc34b&clang=_en]	Ratification	23/07/1985	01/10/1985
Uruguay [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800bc345&clang=_en]	Accession	01/02/1990	01/05/1990
Zaire [https://treaties.un.org/Pages/showActionDetails.aspx?objid=08000002800bc33f&clang=_en]	Accession	22/06/1990	01/09/1990
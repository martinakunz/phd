INTERNATIONAL CONVENTION ON CIVIL LIABILITY FOR BUNKER OIL POLLUTION DAMAGE, 2001

The States Parties to this Convention,

RECALLING article 194 of the United Nations Convention on the Law of the Sea, 1982, which provides that States shall take all measures necessary to prevent, reduce and control pollution of the marine environment,

RECALLING ALSO article 235 of that Convention, which provides that, with the objective of assuring prompt and adequate compensation in respect of all damage caused by pollution of the marine environment, States shall co-operate in the further development of relevant rules of international law,

NOTING the success of the International Convention on Civil Liability for Oil Pollution Damage, 1992 and the International Convention on the Establishment of an International Fund for Compensation for Oil Pollution Damage, 1992 in ensuring that compensation is available to persons who suffer damage caused by pollution resulting from the escape or discharge of oil carried in bulk at sea by ships,

NOTING ALSO the adoption of the International Convention on Liability and Compensation for Damage in Connection with the Carriage of Hazardous and Noxious Substances by Sea, 1996 in order to provide adequate, prompt and effective compensation for damage caused by incidents in connection with the carriage by sea of hazardous and noxious substances,

RECOGNIZING the importance of establishing strict liability for all forms of oil pollution which is linked to an appropriate limitation of the level of that liability,

CONSIDERING that complementary measures are necessary to ensure the payment of adequate, prompt and effective compensation for damage caused by pollution resulting from the escape or discharge of bunker oil from ships,

DESIRING to adopt uniform international rules and procedures for determining questions of liability and providing adequate compensation in such cases,

HAVE AGREED as follows:

Article 1  Definitions

For the purposes of this Convention:

1	"Ship" means any seagoing vessel and seaborne craft, of any type whatsoever.

2	"Person" means any individual or partnership or any public or private body, whether corporate or not, including a State or any of its constituent subdivisions.

3	"Shipowner" means the owner, including the registered owner, bareboat charterer, manager and operator of the ship.

4	''Registered owner'' means the person or persons registered as the owner of the ship or, in the absence of registration, the person or persons owning the ship. However, in the case of a ship owned by a State and operated by a company which in that State is registered as the ship’s operator, ''registered owner'' shall mean such company.

5	"Bunker oil" means any hydrocarbon mineral oil, including lubricating oil, used or intended to be used for the operation or propulsion of the ship, and any residues of such oil.

6	"Civil Liability Convention" means the International Convention on Civil Liability for Oil Pollution Damage, 1992, as amended.

7	"Preventive measures" means any reasonable measures taken by any person after an incident has occurred to prevent or minimize pollution damage.

8	"Incident" means any occurrence or series of occurrences having the same origin, which causes pollution damage or creates a grave and imminent threat of causing such damage.

9	"Pollution damage" means:

(a)	loss or damage caused outside the ship by contamination resulting from the escape or discharge of bunker oil from the ship, wherever such escape or discharge may occur, provided that compensation for impairment of the environment other than loss of profit from such impairment shall be limited to costs of reasonable measures of reinstatement actually undertaken or to be undertaken; and

(b)	the costs of preventive measures and further loss or damage caused by preventive measures.

10	''State of the ship's registry" means, in relation to a registered ship, the State of registration of the ship and, in relation to an unregistered ship, the State whose flag the ship is entitled to fly.

11	''Gross tonnage'' means gross tonnage calculated in accordance with the tonnage measurement regulations contained in Annex 1 of the International Convention on Tonnage Measurement of Ships, 1969.

12	"Organization" means the International Maritime Organization.

13	"Secretary-General" means the Secretary-General of the Organization.

Article 2  Scope of application

This Convention shall apply exclusively:

(a)	to pollution damage caused:

(i)	in the territory, including the territorial sea, of a State Party, and

(ii)	in the exclusive economic zone of a State Party, established in accordance with international law, or, if a State Party has not established such a zone, in an area beyond and adjacent to the territorial sea of that State determined by that State in accordance with international law and extending not more than 200 nautical miles from the baselines from which the breadth of its territorial sea is measured;

(b)	to preventive measures, wherever taken, to prevent or minimize such damage.

Article 3  Liability of the shipowner

1	Except as provided in paragraphs 3 and 4, the shipowner at the time of an incident shall be liable for pollution damage caused by any bunker oil on board or originating from the ship, provided that, if an incident consists of a series of occurrences having the same origin, the liability shall attach to the shipowner at the time of the first of such occurrences.

2	Where more than one person is liable in accordance with paragraph 1, their liability shall be joint and several.

3	No liability for pollution damage shall attach to the shipowner if the shipowner proves that:

(a)	the damage resulted from an act of war, hostilities, civil war, insurrection or a natural phenomenon of an exceptional, inevitable and irresistible character; or

(b)	the damage was wholly caused by an act or omission done with the intent to cause damage by a third party; or

(c)	the damage was wholly caused by the negligence or other wrongful act of any Government or other authority responsible for the maintenance of lights or other navigational aids in the exercise of that function.

4	If the shipowner proves that the pollution damage resulted wholly or partially either from an act or omission done with intent to cause damage by the person who suffered the damage or from the negligence of that person, the shipowner may be exonerated wholly or partially from liability to such person.

5	No claim for compensation for pollution damage shall be made against the shipowner otherwise than in accordance with this Convention.

6	Nothing in this Convention shall prejudice any right of recourse of the shipowner which exists independently of this Convention.

Article 4  Exclusions

1	This Convention shall not apply to pollution damage as defined in the Civil Liability Convention, whether or not compensation is payable in respect of it under that Convention.

2	Except as provided in paragraph 3, the provisions of this Convention shall not apply to warships, naval auxiliary or other ships owned or operated by a State and used, for the time being, only on Government non-commercial service.

3	A State Party may decide to apply this Convention to its warships or other ships described in paragraph 2, in which case it shall notify the Secretary-General thereof specifying the terms and conditions of such application.

4	With respect to ships owned by a State Party and used for commercial purposes, each State shall be subject to suit in the jurisdictions set forth in article 9 and shall waive all defences based on its status as a sovereign State.

Article 5  Incidents involving two or more ships

When an incident involving two or more ships occurs and pollution damage results therefrom, the shipowners of all the ships concerned, unless exonerated under article 3, shall be jointly and severally liable for all such damage which is not reasonably separable.

Article 6  Limitation of liability

Nothing in this Convention shall affect the right of the shipowner and the person or persons providing insurance or other financial security to limit liability under any applicable national or international regime, such as the Convention on Limitation of Liability for Maritime Claims, 1976, as amended.

Article 7  Compulsory insurance or financial security

1	The registered owner of a ship having a gross tonnage greater than 1000 registered in a State Party shall be required to maintain insurance or other financial security, such as the guarantee of a bank or similar financial institution, to cover the liability of the registered owner for pollution damage in an amount equal to the limits of liability under the applicable national or international limitation regime, but in all cases, not exceeding an amount calculated in accordance with the Convention on Limitation of Liability for Maritime Claims, 1976, as amended.

2	A certificate attesting that insurance or other financial security is in force in accordance with the provisions of this Convention shall be issued to each ship after the appropriate authority of a State Party has determined that the requirements of paragraph 1 have been complied with. With respect to a ship registered in a State Party such certificate shall be issued or certified by the appropriate authority of the State of the ship’s registry; with respect to a ship not registered in a State Party it may be issued or certified by the appropriate authority of any State Party. This certificate shall be in the form of the model set out in the annex to this Convention and shall contain the following particulars:

(a)	name of ship, distinctive number or letters and port of registry;

(b)	name and principal place of business of the registered owner;

(c)	IMO ship identification number;

(d)	type and duration of security;

(e)	name and principal place of business of insurer or other person giving security and, where appropriate, place of business where the insurance or security is established;

(f)	period of validity of the certificate which shall not be longer than the period of validity of the insurance or other security.

3	(a)	A State Party may authorize either an institution or an organization recognized by it to issue the certificate referred to in paragraph 2. Such institution or organization shall inform that State of the issue of each certificate. In all cases, the State Party shall fully guarantee the completeness and accuracy of the certificate so issued and shall undertake to ensure the necessary arrangements to satisfy this obligation.

(b)	A State Party shall notify the Secretary-General of :

(i)	the specific responsibilities and conditions of the authority delegated to an institution or organization recognised by it;

(ii)	the withdrawal of such authority; and

(iii)	the date from which such authority or withdrawal of such authority takes effect.

An authority delegated shall not take effect prior to three months from the date on which notification to that effect was given to the Secretary-General.

(c)	The institution or organization authorized to issue certificates in accordance with this paragraph shall, as a minimum, be authorized to withdraw these certificates if the conditions under which they have been issued are not maintained. In all cases the institution or organization shall report such withdrawal to the State on whose behalf the certificate was issued.

4	The certificate shall be in the official language or languages of the issuing State. If the language used is not English, French or Spanish, the text shall include a translation into one of these languages and, where the State so decides, the official language of the State may be omitted.

5	The certificate shall be carried on board the ship and a copy shall be deposited with the authorities who keep the record of the ship's registry or, if the ship is not registered in a State Party, with the authorities issuing or certifying the certificate.

6	An insurance or other financial security shall not satisfy the requirements of this article if it can cease, for reasons other than the expiry of the period of validity of the insurance or security specified in the certificate under paragraph 2 of this article, before three months have elapsed from the date on which notice of its termination is given to the authorities referred to in paragraph 5 of this article, unless the certificate has been surrendered to these authorities or a new certificate has been issued within the said period. The foregoing provisions shall similarly apply to any modification which results in the insurance or security no longer satisfying the requirements of this article.

7	The State of the ship’s registry shall, subject to the provisions of this article, determine the conditions of issue and validity of the certificate.

8	Nothing in this Convention shall be construed as preventing a State Party from relying on information obtained from other States or the Organization or other international organisations relating to the financial standing of providers of insurance or financial security for the purposes of this Convention. In such cases, the State Party relying on such information is not relieved of its responsibility as a State issuing the certificate required by paragraph 2.

9	Certificates issued or certified under the authority of a State Party shall be accepted by other States Parties for the purposes of this Convention and shall be regarded by other States Parties as having the same force as certificates issued or certified by them even if issued or certified in respect of a ship not registered in a State Party. A State Party may at any time request consultation with the issuing or certifying State should it believe that the insurer or guarantor named in the insurance certificate is not financially capable of meeting the obligations imposed by this Convention.

10	Any claim for compensation for pollution damage may be brought directly against the insurer or other person providing financial security for the registered owner’s liability for pollution damage. In such a case the defendant may invoke the defences (other than bankruptcy or winding up of the shipowner) which the shipowner would have been entitled to invoke, including limitation pursuant to article 6. Furthermore, even if the shipowner is not entitled to limitation of liability according to article 6, the defendant may limit liability to an amount equal to the amount of the insurance or other financial security required to be maintained in accordance with paragraph 1. Moreover, the defendant may invoke the defence that the pollution damage resulted from the wilful misconduct of the shipowner, but the defendant shall not invoke any other defence which the defendant might have been entitled to invoke in proceedings brought by the shipowner against the defendant. The defendant shall in any event have the right to require the shipowner to be joined in the proceedings.

11	A State Party shall not permit a ship under its flag to which this article applies to operate at any time, unless a certificate has been issued under paragraphs 2 or 14.

12	Subject to the provisions of this article, each State Party shall ensure, under its national law, that insurance or other security, to the extent specified in paragraph 1, is in force in respect of any ship having a gross tonnage greater than 1000, wherever registered, entering or leaving a port in its territory, or arriving at or leaving an offshore facility in its territorial sea.

13	Notwithstanding the provisions of paragraph 5, a State Party may notify the Secretary-General that, for the purposes of paragraph 12, ships are not required to carry on board or to produce the certificate required by paragraph 2, when entering or leaving ports or arriving at or leaving from offshore facilities in its territory, provided that the State Party which issues the certificate required by paragraph 2 has notified the Secretary-General that it maintains records in an electronic format, accessible to all States Parties, attesting the existence of the certificate and enabling States Parties to discharge their obligations under paragraph 12.

14	If insurance or other financial security is not maintained in respect of a ship owned by a State Party, the provisions of this article relating thereto shall not be applicable to such ship, but the ship shall carry a certificate issued by the appropriate authority of the State of the ship's registry stating that the ship is owned by that State and that the ship's liability is covered within the limit prescribed in accordance with paragraph 1. Such a certificate shall follow as closely as possible the model prescribed by paragraph 2.

15	A State may, at the time of ratification, acceptance, approval of, or accession to this Convention, or at any time thereafter, declare that this article does not apply to ships operating exclusively within the area of that State referred to in article 2(a)(i).

Article 8  Time limits

Rights to compensation under this Convention shall be extinguished unless an action is brought thereunder within three years from the date when the damage occurred. However, in no case shall an action be brought more than six years from the date of the incident which caused the damage. Where the incident consists of a series of occurrences, the six-years’ period shall run from the date of the first such occurrence.

Article 9  Jurisdiction

1	Where an incident has caused pollution damage in the territory, including the territorial sea, or in an area referred to in article 2(a)(ii) of one or more States Parties, or preventive measures have been taken to prevent or minimise pollution damage in such territory, including the territorial sea, or in such area, actions for compensation against the shipowner, insurer or other person providing security for the shipowner's liability may be brought only in the courts of any such States Parties.

2	Reasonable notice of any action taken under paragraph 1 shall be given to each defendant.

3	Each State Party shall ensure that its courts have jurisdiction to entertain actions for compensation under this Convention.

Article 10  Recognition and enforcement

1	Any judgement given by a Court with jurisdiction in accordance with article 9 which is enforceable in the State of origin where it is no longer subject to ordinary forms of review, shall be recognised in any State Party, except:

(a)	where the judgement was obtained by fraud; or

(b)	where the defendant was not given reasonable notice and a fair opportunity to present his or her case.

2	A judgement recognised under paragraph 1 shall be enforceable in each State Party as soon as the formalities required in that State have been complied with. The formalities shall not permit the merits of the case to be re-opened.

Article 11  Supersession Clause

This Convention shall supersede any Convention in force or open for signature, ratification or accession at the date on which this Convention is opened for signature, but only to the extent that such Convention would be in conflict with it; however, nothing in this article shall affect the obligations of States Parties to States not party to this Convention arising under such Convention.

Article 12  Signature, ratification, acceptance, approval and accession

1	This Convention shall be open for signature at the Headquarters of the Organization from 1 October 2001 until 30 September 2002 and shall thereafter remain open for accession.

2	States may express their consent to be bound by this Convention by:

(a)	signature without reservation as to ratification, acceptance or approval;

(b)	signature subject to ratification, acceptance or approval followed by ratification, acceptance or approval; or

(c)	accession.

3	Ratification, acceptance, approval or accession shall be effected by the deposit of an instrument to that effect with the Secretary-General.

4	Any instrument of ratification, acceptance, approval or accession deposited after the entry into force of an amendment to this Convention with respect to all existing State Parties, or after the completion of all measures required for the entry into force of the amendment with respect to those State Parties shall be deemed to apply to this Convention as modified by the amendment.

Article 13  States with more than one system of law

1	If a State has two or more territorial units in which different systems of law are applicable in relation to matters dealt with in this Convention, it may at the time of signature, ratification, acceptance, approval or accession declare that this Convention shall extend to all its territorial units or only to one or more of them and may modify this declaration by submitting another declaration at any time.

2	Any such declaration shall be notified to the Secretary-General and shall state expressly the territorial units to which this Convention applies.

3	In relation to a State Party which has made such a declaration:

(a)	in the definition of ''registered owner'' in article 1(4), references to a State shall be construed as references to such a territorial unit;

(b)	references to the State of a ship’s registry and, in relation to a compulsory insurance certificate, to the issuing or certifying State, shall be construed as referring to the territorial unit respectively in which the ship is registered and which issues or certifies the certificate;

(c)	references in this Convention to the requirements of national law shall be construed as references to the requirements of the law of the relevant territorial unit; and

(d)	references in articles 9 and 10 to courts, and to judgements which must be recognized in States Parties, shall be construed as references respectively to courts of, and to judgements which must be recognized in, the relevant territorial unit.

Article 14  Entry into Force

1	This Convention shall enter into force one year following the date on which eighteen States, including five States each with ships whose combined gross tonnage is not less than 1 million, have either signed it without reservation as to ratification, acceptance or approval or have deposited instruments of ratification, acceptance, approval or accession with the Secretary-General.

2	For any State which ratifies, accepts, approves or accedes to it after the conditions in paragraph 1 for entry into force have been met, this Convention shall enter into force three months after the date of deposit by such State of the appropriate instrument.

Article 15  Denunciation

1	This Convention may be denounced by any State Party at any time after the date on which this Convention comes into force for that State.

2	Denunciation shall be effected by the deposit of an instrument with the Secretary-General.

3	A denunciation shall take effect one year, or such longer period as may be specified in the instrument of denunciation, after its deposit with the Secretary-General.

Article 16  Revision or amendment

1	A conference for the purpose of revising or amending this Convention may be convened by the Organization.

2	The Organization shall convene a conference of the States Parties for revising or amending this Convention at the request of not less than one-third of the States Parties.

Article 17  Depositary

1	This Convention shall be deposited with the Secretary-General.

2	The Secretary-General shall:

(a)	inform all States which have signed or acceded to this Convention of:

(i)	each new signature or deposit of instrument together with the date thereof;

(ii)	the date of entry into force of this Convention;

(iii)	the deposit of any instrument of denunciation of this Convention together with the date of the deposit and the date on which the denunciation takes effect; and

(iv)	other declarations and notifications made under this Convention.

(b)	transmit certified true copies of this Convention to all Signatory States and to all States which accede to this Convention.

Article 18  Transmission to United Nations

As soon as this Convention comes into force, the text shall be transmitted by the Secretary-General to the Secretariat of the United Nations for registration and publication in accordance with Article 102 of the Charter of the United Nations.

Article 19  Languages

This Convention is established in a single original in the Arabic, Chinese, English, French, Russian and Spanish languages, each text being equally authentic.

DONE AT LONDON this twenty-third day of March, two thousand and one.

IN WITNESS WHEREOF the undersigned being duly authorised by their respective Governments for that purpose have signed this Convention.

ANNEX

CERTIFICATE OF INSURANCE OR OTHER FINANCIAL SECURITY  IN RESPECT OF CIVIL LIABILITY FOR BUNKER OIL POLLUTION DAMAGE  Issued in accordance with the provisions of article 7 of the International Convention on Civil Liability for Bunker Oil Pollution Damage, 2001


Name of Ship	Distinctive Number or letters	IMO Ship Identification Number	Port of Registry	Name and full address of the principal place of business of the registered owner.
				

This is to certify that there is in force in respect of the above-named ship a policy of insurance or other financial security satisfying the requirements of article 7 of the International Convention on Civil Liability for Bunker Oil Pollution Damage, 2001.

Type of Security ..............................................................................................................................

Duration of Security ......................................................................................................................

Name and address of the insurer(s)and/or guarantor(s)

Name................................................................................................................................................

Address ............................................................................................................................................
..........................................................................................................................................................

This certificate is valid until ...............................................................................................

Issued or certified by the Government of ...........................................................................
.............................................................................................................................................

(Full designation of the State)  OR

The following text should be used when a State Party avails itself of article 7(3)

The present certificate is issued under the authority of the Government of .....(full designation of the State) by..................(name of institution or organization)

At ...................................................................... On ........................................................................
(Place)	(Date)

...............................................................................................................
(Signature and Title of issuing or certifying official)

Explanatory Notes:

1.	If desired, the designation of the State may include a reference to the competent public authority of the country where the Certificate is issued.

2.	If the total amount of security has been furnished by more than one source, the amount of each of them should be indicated.

3.	If security is furnished in several forms, these should be enumerated.

4.	The entry ''Duration of Security'' must stipulate the date on which such security takes effect.

5.	The entry ''Address'' of the insurer(s) and/or guarantor(s) must indicate the principal place of business of the insurer(s) and/or guarantor(s). If appropriate, the place of business where the insurance or other security is established shall be indicated.

-----
[Source: http://www.austlii.edu.au/au/other/dfat/treaties/ATS/2009/14.html (last retrieved 2020-07-29)]
[CC BY 3.0 AU Australian Government, Department of Foreign Affairs and Trade]
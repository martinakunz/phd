INTERNATIONAL CONVENTION ON THE PREVENTION OF MARINE POLLUTION BY DUMPING OF WASTES AND OTHER MATTER



THE CONTRACTING PARTIES TO THIS CONVENTION,

RECOGNIZING that the marine environment and the living organisms which it supports are of vital importance to humanity, and all people have an interest in assuring that it is so managed that its quality and resources are not impaired;

RECOGNIZING that the capacity of the sea to assimilate wastes and render them harmless, and its ability to regenerate natural resources, is not unlimited;

RECOGNIZING that States have, in accordance with the Charter of the United Nations and the principles of international law, the sovereign right to exploit their own resources pursuant to their own environmental policies, and the responsibility to ensure that activities within their jurisdiction or control do not cause damage to the environment of other States or of areas beyond the limits of national jurisdiction;

RECALLING Resolution 2749(XXV) of the General Assembly of the United Nations on the principles governing the sea-bed and the ocean floor and the subsoil thereof, beyond the limits of national jurisdiction;

NOTING that marine pollution originates in many sources, such as dumping and discharges through the atmosphere, rivers, estuaries, outfalls and pipelines, and that it is important that States use the best practicable means to prevent such pollution and develop products and processes which will reduce the amount of harmful wastes to be disposed of;

BEING CONVINCED that international action to control the pollution of the sea by dumping can and must be taken without delay but that this action should not preclude discussion of measures to control other sources of marine pollution as soon as possible; and

WISHING to improve protection of the marine environment by encouraging States with a common interest in particular geographical areas to enter into appropriate agreements supplementary to this Convention;

HAVE AGREED as follows:

Article I

Contracting Parties shall individually and collectively promote the effective control of all sources of pollution of the marine environment, and pledge themselves especially to take all practicable steps to prevent the pollution of the sea by the dumping of waste and other matter that is liable to create hazards to human health, to harm living resources and marine life, to damage amenities or to interfere with other legitimate uses of the sea.

Article II

Contracting Parties shall, as provided for in the following Articles, take effective measures individually, according to their scientific, technical and economic capabilities, and collectively, to prevent marine pollution caused by dumping and shall harmonize their policies in this regard.

Article III

For the purposes of this Convention:

1.	(a)	"Dumping" means:

(i)	any deliberate disposal at sea of wastes or other matter from vessels, aircraft, platforms or other man-made structures at sea;

(ii)	any deliberate disposal at sea of vessels, aircraft, platforms or other man-made structures at sea.

(b)	"Dumping" does not include:

(i)	the disposal at sea of wastes or other matter incidental to, or derived from the normal operations of vessels, aircraft, platforms or other man-made structures at sea and their equipment, other than wastes or other matter transported by or to vessels, aircraft, platforms or other man-made structures at sea, operating for the purpose of disposal of such matter or derived from the treatment of such wastes or other matter on such vessels, aircraft, platforms or structures;

(ii)	placement of matter for a purpose other than the mere disposal thereof, provided that such placement is not contrary to the aims of this Convention.

(c)	The disposal of wastes or other matter directly arising from, or related to the exploration, exploitation and associated off-shore processing of sea-bed mineral resources will not be covered by the provisions of this Convention.

2.	"Vessels and aircraft" means waterborne or airborne craft of any type whatsoever. This expression includes air cushioned craft and floating craft, whether self-propelled or not.

3.	"Sea" means all marine waters other than the internal waters of States.

4.	"Wastes or other matter" means material and substance of any kind, form or description.

5.	"Special permit" means permission granted specifically on application in advance and in accordance with Annex II and Annex III.

6.	"General permit" means permission granted in advance and in accordance with Annex III.

7.	"The Organisation" means the Organisation designated by the Contracting Parties in accordance with Article XIV(2).

Article IV

1.	In accordance with the provisions of this Convention Contracting Parties shall prohibit the dumping of any wastes or other matter in whatever form or condition except as otherwise specified below:

(a)	the dumping of wastes or other matter listed in Annex I is prohibited;

(b)	the dumping of wastes or other matter listed in Annex II requires a prior special permit;

(c)	the dumping of all other wastes or matter requires a prior general permit.

2.	Any permit shall be issued only after careful consideration of all the factors set forth in Annex III, including prior studies of the characteristics of the dumping site, as set forth in Sections B and C of that Annex.

3.	No provision of this Convention is to be interpreted as preventing a Contracting Party from prohibiting, insofar as that Party is concerned, the dumping of wastes or other matter not mentioned in Annex I. That Party shall notify such measures to the Organisation.

Article V

1.	The provisions of Article IV shall not apply when it is necessary to secure the safety of human life or of vessels, aircraft, platforms or other man-made structures at sea in cases of force majeure caused by stress of weather, or in any case which constitutes a danger to human life or a real threat to vessels, aircraft, platforms or other man-made structures at sea, if dumping appears to be the only way of averting the threat and if there is every probability that the damage consequent upon such dumping will be less than would otherwise occur. Such dumping shall be so conducted as to minimise the likelihood of damage to human or marine life and shall be reported forthwith to the Organisation.

2.	A Contracting Party may issue a special permit as an exception to Article IV(1)(a), in emergencies, posing unacceptable risk relating to human health and admitting no other feasible solution. Before doing so the Party shall consult any other country or countries that are likely to be affected and the organisation which, after consulting other Parties, and international organisations as appropriate, shall, in accordance with Article XIV promptly recommend to the Party the most appropriate procedures to adopt. The Party shall follow these recommendations to the maximum extent feasible consistent with the time within which action must be taken and with the general obligation to avoid damage to the marine environment and shall inform the Organisation of the action it takes. The Parties pledge themselves to assist one another in such situations.

3.	Any Contracting Party may waive its rights under paragraph (2) at the time of, or subsequent to ratification of, or accession to this Convention.

Article VI

1.	Each Contracting Party shall designate an appropriate authority or authorities to:

(a)	issue special permits which shall be required prior to, and for, the dumping of matter listed in Annex II and in the circumstances provided for in Article V(2);

(b)	issue general permits which shall be required prior to, and for, the dumping of all other matter;

(c)	keep records of the nature and quantities of all matter permitted to be dumped and the location, time and method of dumping;

(d)	monitor individually, or in collaboration with other Parties and competent international organisations, the condition of the seas for the purposes of this Convention.

2.	The appropriate authority or authorities of a Contracting Party shall issue prior special or general permits in accordance with paragraph (1) in respect of matter intended for dumping:

(a)	loaded in its territory;

(b)	loaded by a vessel or aircraft registered in its territory or flying its flag, when the loading occurs in the territory of a State not party to this Convention.

3.	In issuing permits under sub-paragraphs (1)(a) and (b) above, the appropriate authority or authorities shall comply with Annex III, together with such additional criteria, measures and requirements as they may consider relevant.

4.	Each Contracting Party, directly or through a Secretariat established under a regional agreement, shall report to the Organisation, and where appropriate to other Parties, the information specified in sub-paragraphs (c) and (d) of paragraph (1) above, and the criteria, measures and requirements it adopts in accordance with paragraph (3) above. The procedure to be followed and the nature of such reports shall be agreed by the Parties in consultation.

Article VII

1.	Each Contracting Party shall apply the measures required to implement the present Convention to all:

(a)	vessels and aircraft registered in its territory or flying its flag;

(b)	vessels and aircraft loading in its territory or territorial seas matter which is to be dumped;

(c)	vessels and aircraft and fixed or floating platforms under its jurisdiction believed to be engaged in dumping.

2.	Each Party shall take in its territory appropriate measures to prevent and punish conduct in contravention of the provisions of this Convention.

3.	The Parties agree to co-operate in the development of procedures for the effective application of this Convention particularly on the high seas, including procedures for the reporting of vessels and aircraft observed dumping in contravention of the Convention.

4.	This Convention shall not apply to those vessels and aircraft entitled to sovereign immunity under international law. However each Party shall ensure by the adoption of appropriate measures that such vessels and aircraft owned or operated by it act in a manner consistent with the object and purpose of this Convention, and shall inform the Organisation accordingly.

5.	Nothing in this Convention shall affect the right of each Party to adopt other measures, in accordance with the principles of international law, to prevent dumping at sea.

Article VIII

In order to further the objectives of this Convention, the Contracting Parties with common interests to protect in the marine environment in a given geographical area shall endeavour, taking into account characteristic regional features, to enter into regional agreements consistent with this Convention for the prevention of pollution, especially by dumping. The Contracting Parties to the present Convention shall endeavour to act consistently with the objectives and provisions of such regional agreements, which shall be notified to them by the Organisation. Contracting Parties shall seek to co-operate with the Parties to regional agreements in order to develop harmonized procedures to be followed by Contracting Parties to the different conventions concerned. Special attention shall be given to co-operation in the field of monitoring and scientific research.

Article IX

The Contracting Parties shall promote, through collaboration within the Organisation and other international bodies, support for those Parties which request it for:

(a)	the training of scientific and technical personnel;

(b)	the supply of necessary equipment and facilities for research and monitoring;

(c)	the disposal and treatment of waste and other measures to prevent or mitigate pollution caused by dumping;

preferably within the countries concerned, so furthering the aims and purposes of this Convention.

Article X

In accordance with the principles of international law regarding State responsibility for damage to the environment of the other States or to any other area of the environment, caused by dumping of wastes and other matter of all kinds, the Contracting Parties undertake to develop procedures for the assessment of liability and the settlement of disputes regarding dumping.

Article XI

The Contracting Parties shall at their first consultative meeting consider procedures for the settlement of disputes concerning the interpretation and application of this Convention.

Article XII

The Contracting Parties pledge themselves to promote, within the competent specialised agencies and other international bodies, measures to protect the marine environment against pollution caused by:

(a)	hydrocarbons, including oil, and their wastes;

(b)	other noxious or hazardous matter transported by vessels for purposes other than dumping;

(c)	wastes generated in the course of operation of vessels, aircraft, platforms and other man-made structures at sea;

(d)	radio-active pollutants from all sources, including vessels;

(e)	agents of chemical and biological warfare;

(f)	wastes or other matter directly arising from, or related to the exploration, exploitation and associated off-shore processing of sea-bed mineral resources.

The Parties will also promote, within the appropriate international organisation, the codification of signals to be used by vessels engaged in dumping.

Article XIII

Nothing in this Convention shall prejudice the codification and development of the law of the sea by the United Nations Conference on the Law of the Sea convened pursuant to Resolution 2750C(XXV) of the General Assembly of the United Nations nor the present or future claims and legal views of any State concerning the law of the sea and the nature and extent of coastal and flag State jurisdiction. The Contracting Parties agree to consult at a meeting to be convened by the Organisation after the Law of the Sea Conference, and in any case not later than 1976, with a view to defining the nature and extent of the right and the responsibility of a coastal State to apply the Convention in a zone adjacent to its coast.

Article XIV

1.	The Government of the United Kingdom of Great Britain an Northern Ireland as a depositary shall call a meeting of the Contracting Parties not later than three months after the entry into force of this Convention to decide on organisational matters.

2.	The Contracting Parties shall designate a competent Organisation existing at the time of that meeting to be responsible for Secretariat duties in relation to this Convention. Any Party to this Convention not being a member of this Organisation shall make an appropriate contribution to the expenses incurred by the Organisation in performing these duties.

3.	The Secretariat duties of the Organisation shall include:

(a)	the convening of consultative meetings of the Contracting Parties not less frequently than once every two years and of special meetings of the Parties at any time on the request of two-thirds of the Parties;

(b)	preparing and assisting, in consultation with the Contracting Parties and appropriate International Organisations, in the development and implementation of procedures referred to in sub-paragraph (4)(e) of this Article;

(c)	considering enquiries by, and information from the Contracting Parties, consulting with them and with the appropriate International Organisations, and providing recommendations to the Parties on questions related to, but not specifically covered by the Convention;

(d)	conveying to the Parties concerned all notifications received by the Organisation in accordance with Articles IV(3), V(1) and (2), VI(4), XV, XX and XXI.

Prior to the designation of the Organisation these functions shall, as necessary, be performed by the depositary, who for this purpose shall be the Government of the United Kingdom of Great Britain and Northern Ireland.

4.	Consultative or special meetings of the Contracting Parties shall keep under continuing review the implementation of this Convention and may, inter alia:

(a)	review and adopt amendments to this Convention and its Annexes in accordance with Article XV;

(b)	invite the appropriate scientific body or bodies to collaborate with and to advise the Parties or the Organisations on any scientific or technical aspect relevant to this Convention, including particularly the content of the Annexes;

(c)	receive and consider reports made pursuant to Article VI(4);

(d)	promote co-operation with and between regional organisations concerned with the prevention of marine pollution;

(e)	develop or adopt, in consultation with appropriate International Organisations, procedures referred to in Article V(2), including basic criteria for determining exceptional and emergency situations, and procedures for consultative advice and the safe disposal of matter in such circumstances, including the designation of appropriate dumping areas, and recommend accordingly;

(f)	consider any additional action that may be required.

5.	The Contracting Parties at their first consultative meeting shall establish rules of procedure as necessary.

Article XV

1.	(a)	At meetings of the Contracting Parties called in accordance with Article XIV amendments to this Convention may be adopted by a two-thirds majority of those present. An amendment shall enter into force for the Parties which have accepted it on the sixtieth day after two-thirds of the Parties shall have deposited an instrument of acceptance of the amendment with the Organisation. Thereafter the amendment shall enter into force for any other Party 30 days after that Party deposits its instrument of acceptance of the amendment.

(b)	The Organisation shall inform all Contracting Parties of any request made for a special meeting under Article XIV and of any amendments adopted at meetings of the Parties and of the date on which each such amendment enters into force for each Party.

2.	Amendments to the Annexes will be based on scientific or technical considerations. Amendments to the Annexes approved by a two-thirds majority of those present at a meeting called in accordance with Article XIV shall enter into force for each Contracting Party immediately on notification of its acceptance to the Organisation and 100 days after approval by the meeting for all other Parties except for those which before the end of the 100 days make a declaration that they are not able to accept the amendment at that time. Parties should endeavour to signify their acceptance of an amendment to the Organisation as soon as possible after approval at a meeting. A Party may at any time substitute an acceptance for a previous declaration of objection and the amendment previously objected to shall thereupon enter into force for that Party.

3.	An acceptance or declaration of objection under this Article shall be made by the deposit of an instrument with the Organisation. The Organisation shall notify all Contracting Parties of the receipt of such instruments.

4.	Prior to the designation of the Organisation, the Secretarial functions herein attributed to it, shall be performed temporarily by the Government of the United Kingdom of Great Britain and Northern Ireland, as one of the depositaries of this Convention.

Article XVI

This Convention shall be open for signature by any State at London, Mexico City, Moscow and Washington from 29 December 1972 until 31 December 1973.

Article XVII

This Convention shall be subject to ratification. The instruments of ratification shall be deposited with the Governments of Mexico, the Union of Soviet Socialist Republics, the United Kingdom of Great Britain and Northern Ireland, and the United States of America.

Article XVIII

After 31 December 1973, this Convention shall be open for accession by any State. The instruments of accession shall be deposited with the Governments of Mexico, the Union of Soviet Socialist Republics, the United Kingdom of Great Britain and Northern Ireland, and the United States of America.

Article XIX

1.	This Convention shall enter into force on the thirtieth day following the date of deposit of the fifteenth instrument of ratification or accession.

2.	For each Contracting Party ratifying or acceding to the Convention after the deposit of the fifteenth instrument of ratification or accession, the Convention shall enter into force on the thirtieth day after deposit by such Party of its instrument of ratification or accession.

Article XX

The depositaries shall inform Contracting Parties:

(a)	of signatures to this Convention and of the deposit of instruments of ratification, accession or withdrawal, in accordance with Articles XVI, XVII, XVIII and XXI, and

(b)	of the date on which this Convention will enter into force, in accordance with Article XIX.

Article XXI

Any Contracting Party may withdraw from this Convention by giving six months' notice in writing to a depositary, which shall promptly inform all Parties of such notice.

Article XXII

The original of this Convention of which the English, French, Russian and Spanish texts are equally authentic, shall be deposited with the Governments of Mexico, the Union of Soviet Socialist Republics, and United Kingdom of Great Britain and Northern Ireland and the United States of America who shall send certified copies thereof to all States.

IN WITNESS WHEREOF the undersigned Plenipotentiaries, being duly authorised thereto by their respective Governments have signed the present Convention.

DONE in quadruplicate at London, Mexico City, Moscow and Washington, this twenty-ninth day of December 1972.

[Signatures not reproduced here.]

ANNEX 1

1.	Organohalogen compounds.

2.	Mercury and mercury compounds.

3.	Cadmium and cadmium compounds.

4.	Persistent plastics and other persistent synthetic materials, for example, netting and ropes, which may float or may remain in suspension in the sea in such a manner as to interfere materially with fishing, navigation or other legitimate uses of the sea.

5.	Crude oil, fuel oil, heavy diesel oil, and lubricating oils, hydraulic fluids, and any mixtures containing any of these, taken on board for the purpose of dumping.

6.	High-level radio-active wastes or other high-level radio-active matter, defined on public health, biological or other grounds, by the competent international body in this field, at present the International Atomic Energy Agency, as unsuitable for dumping at sea.

7.	Materials in whatever form (e.g. solids, liquids, semi-liquids, gases or in a living state) produced for biological and chemical warfare.

8.	The preceding paragraphs of this Annex do not apply to substances which are rapidly rendered harmless by physical, chemical or biological processes in the sea provided they do not:

(i)	make edible marine organisms unpalatable, or

(ii)	endanger human health or that of domestic animals.

The consultative procedure provided for under Article XIV should be followed by a Party if there is doubt about the harmlessness of the substance.

9.	This Annex does not apply to wastes or other materials (e.g. sewage sludges and dredged spoils) containing the matters referred to in paragraphs 1-5 above as trace contaminants. Such wastes shall be subject to the provisions of Annexes II and III as appropriate.

ANNEX II

The following substances and materials requiring special care are listed for the purposes of Article VI(1)(a).

A.	Wastes containing significant amounts of the matters listed below:

arsenic	}

lead	}

copper	}	and their compounds

zinc	}

organosilicon compounds

cynanides

fluorides

pesticides and their by-products not covered in Annex 1

B.	In the issue of permits for the dumping of large quantities of acids and alkalis, consideration shall be given to the possible presence in such wastes of the substances listed in paragraph A and to the following additional substances:

beryllium	}

chromium	}

nickel	}	and their compounds

vanadium	}

C.	Containers, scrap metal and other bulky wastes liable to sink to the sea bottom which may present a serious obstacle to fishing or navigation.

D.	Radio-active wastes or other radio-active matter not included in Annex I. In the issue of permits for the dumping of this matter, the Contracting Parties should take full account of the recommendations of the competent international body in this field, at present the International Atomic Energy Agency.

ANNEX III

Provisions to be considered in establishing criteria governing the issue of permits for the dumping of matter at sea, taking into account Article IV(2), include:

A - Characteristics and composition of the matter

1.	Total amount and average composition of matter dumped (e.g. per year).

2.	Form, e.g. solid, sludge, liquid, or gaseous.

3.	Properties: physical (e.g. solubility and density), chemical and biochemical (e.g. oxygen demand, nutrients) and biological (e.g. presence of viruses, bacteria, yeasts, parasites).

4.	Toxicity.

5.	Persistence: physical, chemical and biological.

6.	Accumulation and biotransformation in biological materials or sediments.

7.	Susceptibility to physical, chemical and biochemical changes and interaction in the aquatic environment with other dissolved organic and inorganic materials.

8.	Probability of production of taints or other changes reducing marketability of resources (fish, shellfish, etc.).

B - Characteristics of dumping site and method of deposit

1.	Location (e.g. coordinates of the dumping area, depth and distance from the coast), location in relation to other areas (e.g. amenity areas, spawning, nursery and fishing areas and exploitable resources).

2.	Rate of disposal per specific period (e.g. quantity per day, per week, per month).

3.	Methods of packaging and containment, if any.

4.	Initial dilution achieved by proposed method of release.

5.	Dispersal characteristics (e.g. effects of currents, tides and wind on horizontal transport and vertical mixing).

6.	Water characteristics (e.g. temperature, pH, salinity, stratification, oxygen indices of pollution - dissolved oxygen (DO), chemical oxygen demand (COD), biochemical oxygen demand (BOD) - nitrogen present in organic and mineral form including ammonia, suspended matter, other nutrients and productivity).

7.	Bottom characteristics (e.g. topography, geochemical and geological characteristics and biological productivity).

8.	Existence and effects of other dumpings which have been made in the dumping area (e.g. heavy metal background reading and organic carbon content).

9.	In issuing a permit for dumping, Contracting Parties should consider whether an adequate scientific basis exists for assessing the consequences of such dumping, as outlined in this Annex, taking into account seasonal variations.

C - General considerations and conditions

1.	Possible effects on amenities (e.g. presence of floating or stranded material, turbidity, objectionable odour, discolouration and foaming).

2.	Possible effects on marine life, fish and shellfish culture, fish stocks and fisheries, seaweed harvesting and culture.

3.	Possible effects on other uses of the sea (e.g. impairment of water quality for industrial use, underwater corrosion of structures, interference with ship operations from floating materials, interference with fishing or navigation through deposit of waste or solid objects on the sea floor and protection of areas of special importance for scientific or conservation purposes).

4.	The practical availability of alternative land-based methods of treatment, disposal or elimination, or of treatment to render the matter less harmful for dumping at sea.

-----
[Source: http://www.austlii.edu.au/au/other/dfat/treaties/ATS/1985/16.html (last retrieved 2020-07-29)]
[CC BY 3.0 AU Australian Government, Department of Foreign Affairs and Trade]
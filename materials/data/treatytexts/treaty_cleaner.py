import re, os, glob
import numpy as np
import pandas as pd
from datetime import datetime
import warnings

# ignore warnings (pandas FutureWarning)
warnings.filterwarnings("ignore")

scrapeddir = './scraped/'
cleandir = './cleaned/'
scrapedfns = glob.glob(scrapeddir + '*txt')
scraped = pd.DataFrame([s.split('/')[-1] for s in scrapedfns], columns=['fn'])
scraped['text'] = [open(f, 'r', encoding='utf-8').read() for f in scrapedfns]
scraped0 = scraped.copy()
scraped.text = scraped.text.str.replace('\ufeff','', regex=False)
scraped.text = scraped.text.str.replace(u'\xa0+\s+',u'\t', regex=True).str.strip()

# reconstruct MARPOL-1973
marpoldf = scraped[scraped.fn.str.contains('1973-PollutionFromShips')].copy().sort_values(by='fn')
marpoldf = marpoldf.iloc[[0]].append(marpoldf[marpoldf.fn.str.contains('Protocol')]).append(marpoldf[marpoldf.fn.str.contains('Annex')])
sources = marpoldf.text.str.split('-----(?=\n\[Source)').str[1]
marpoldf.text = marpoldf.text.str.split('(?=-----\n\[Source)').str[0]
marpoldf.text.iloc[0] = marpoldf.text.str.cat() + '-----' + sources.str.cat()
scraped = scraped[~scraped.fn.str.contains('1973-PollutionFromShips')].append(marpoldf.iloc[[0]], ignore_index=True)
scraped0 = scraped0[scraped0.fn.isin(scraped.fn)].reset_index(drop=True)

# edit start of AustLII texts
scraped.text = scraped.text.str.replace('(?is)\s+(?:Table of )?Contents.+Preamble.+(?=\n+Preamble)', '', regex=True)
scraped.text = scraped.text.str.replace('(?is)\s+Contents.+?(?=\n+The Contracting Governments)', '\n', regex=True)
scraped.text = scraped.text.str.replace('(?is)\s+Table of Contents.+?\n(?=\n+The States Parties)', '', regex=True)

# edit end of AustLII texts
end = '(?=\n\n-----\n\[Source)'
scraped.text = scraped.text.str.replace('(?s)\nPROTOCOL CONCERNING SPAIN.+'+ end, '\n', regex=True)
scraped.text = scraped.text.str.replace('(?s)[^\n]+ORGANIZATION, as amended to 1995.+'+end, '', regex=True)
scraped.text = scraped.text.str.replace('(?s)\nPROTOCOL TO [^\n]+, 1969.+'+ end, '', regex=True)
scraped.text = scraped.text.str.replace('(?s)\nAMENDMENTS .+'+ end, '\n', regex=True)
scraped.text = scraped.text.str.replace('(?s)\nFOOD AID CONVENTION, 1995.+'+ end, '', regex=True)
scraped.text = scraped.text.str.replace('(?s)\sDECLARATIONS BY AUSTRALIA.+'+ end, '\n', regex=True)
scraped.text = scraped.text.str.replace('(?s)\nRESOLUTION I.+'+ end, '', regex=True)

# add 'last modified' date
today = str(datetime.utcnow().isoformat().split("T")[0])
scraped.text[scraped.text != scraped0.text] = scraped.text[scraped.text != scraped0.text].str.replace('(?<=last retrieved \d{4}-\d{2}-\d{2})', ', last modified ' + today, regex=True)

# update 'cleaned' directory with new or modified texts
for i in range(len(scraped)):
  if os.path.exists(cleandir + scraped.fn[i]):
    with open(cleandir + scraped.fn[i], "r", encoding='utf-8') as f:
      oldt = f.read().split('\n\n-----\n[Source')[0]
    newt = scraped.text[i].split('\n\n-----\n[Source')[0]
    if (newt != oldt):
      with open(cleandir + scraped.fn[i], 'w', encoding='utf-8') as f:
        f.write(scraped.text[i])
  else:
    if (scraped.text[i] == scraped0.text[i]):
      # preserve original creation date
      os.system('cp -pu ' + scrapeddir + scraped.fn[i] + ' ' + cleandir + scraped.fn[i])
    else:
      with open(cleandir + scraped.fn[i], 'w', encoding='utf-8') as f:
        f.write(scraped.text[i])

print(f'Processed {len(scrapedfns)} files and combined them into {len(scraped)} treaty texts.') 

NEW [SECOND] REVISED TEXT OF THE INTERNATIONAL PLANT PROTECTION CONVENTION OF 6 DECEMBER 1951,  AS REVISED 28 NOVEMBER 1979

Preamble

THE CONTRACTING PARTIES,

-	RECOGNIZING the necessity for international cooperation in controlling pests of plants and plants products and in preventing their international spread, and especially their introduction into endangered areas;

-	RECOGNIZING that phytosanitary measures should be technically justified, transparent and should not be applied in such a way as to constitute either a means of arbitrary or unjustified discrimination or a disguised restriction, particularly on international trade;

-	DESIRING to ensure close coordination of measures directed to these ends;

-	DESIRING to provide a framework for the development and application of harmonized phytosanitary measures and the elaboration of international standards to that effect;

-	TAKING INTO ACCOUNT internationally approved principles governing the protection of plant, human and animal health, and the environment; and

-	NOTING the agreements concluded as a result of the Uruguay Round of Multilateral Trade Negotiations, including the Agreement on the Application of Sanitary and Phytosanitary Measures;

HAVE AGREED as follows:

Article I  Purpose and responsibility

1.	With the purpose of securing common and effective action to prevent the spread and introduction of pests of plants and plant products, and to promote appropriate measures for their control, the contracting parties undertake to adopt the legislative, technical and administrative measures specified in this Convention and in supplementary agreements pursuant to Article XVI.

2.	Each contracting party shall assume responsibility, without prejudice to obligations assumed under other international agreements, for the fulfilment within its territories of all requirements under this Convention.

3.	The division of responsibilities for the fulfilment of the requirements of this Convention between member organizations of FAO and their member states that are contracting parties shall be in accordance with their respective competencies.

4.	Where appropriate, the provisions of this Convention may be deemed by contracting parties to extend, in addition to plants and plant products, to storage places, packaging, conveyances, containers, soil and any other organism, object or material capable of harbouring or spreading plant pests, particularly where international transportation is involved.

Article II  Use of terms

1.	For the purpose of this Convention, the following terms shall have the meanings hereunder assigned to them:

"Area of low pest prevalence" - an area, whether all of a country, part of a country, or all or parts of several countries, as identified by the competent authorities, in which a specific pest occurs at low levels and which is subject to effective surveillance, control or eradication measures;

"Commission" - the Commission on Phytosanitary Measures established under Article XI;

"Endangered area" - an area where ecological factors favour the establishment of a pest whose presence in the area will result in economically important loss;

"Establishment" - perpetuation, for the foreseeable future, of a pest within an area after entry;

"Harmonized phytosanitary measures" - phytosanitary measures established by contracting parties based on international standards;

"International standards" - international standards established in accordance with Article X, paragraphs 1 and 2;

"Introduction" - the entry of a pest resulting in its establishment;

"Pest" - any species, strain or biotype of plant, animal or pathogenic agent injurious to plants or plant products;

"Pest risk analysis" - the process of evaluating biological or other scientific and economic evidence to determine whether a pest should be regulated and the strength of any phytosanitary measures to be taken against it;

"Phytosanitary measure" - any legislation, regulation or official procedure having the purpose to prevent the introduction and/or spread of pests;

"Plant products" - unmanufactured material of plant origin (including grain) and those manufactured products that, by their nature or that of their processing, may create a risk for the introduction and spread of pests;

"Plants" - living plants and parts thereof, including seeds and germplasm;

"Quarantine pest" - a pest of potential economic importance to the area endangered thereby and not yet present there, or present but not widely distributed and being officially controlled;

"Regional standards" - standards established by a regional plant protection organization for the guidance of the members of that organization;

"Regulated article" - any plant, plant product, storage place, packaging, conveyance, container, soil and any other organism, object or material capable of harbouring or spreading pests, deemed to require phytosanitary measures, particularly where international transportation is involved;

"Regulated non-quarantine pest" - a non-quarantine pest whose presence in plants for planting affects the intended use of those plants with an economically unacceptable impact and which is therefore regulated within the territory of the importing contracting party;

"Regulated pest" - a quarantine pest or a regulated non-quarantine pest;

"Secretary" - Secretary of the Commission appointed pursuant to Article XII;

"Technically justified" - justified on the basis of conclusions reached by using an appropriate pest risk analysis or, where applicable, another comparable examination and evaluation of available scientific information.

2.	The definitions set forth in this Article, being limited to the application of this Convention, shall not be deemed to affect definitions established under domestic laws or regulations of contracting parties.

Article III  Relationship with other international agreements

Nothing in this Convention shall affect the rights and obligations of the contracting parties under relevant international agreements.

Article IV  General provisions relating to the organizational arrangements for  national plant protection

1.	Each contracting party shall make provision, to the best of its ability, for an official national plant protection organization with the main responsibilities set out in this Article.

2.	The responsibilities of an official national plant protection organization shall include the following:

(a) the issuance of certificates relating to the phytosanitary regulations of the importing contracting party for consignments of plants, plant products and other regulated articles;

(b) the surveillance of growing plants, including both areas under cultivation (inter alia fields, plantations, nurseries, gardens, greenhouses and laboratories) and wild flora, and of plants and plant products in storage or in transportation, particularly with the object of reporting the occurrence, outbreak and spread of pests, and of controlling those pests, including the reporting referred to under Article VIII paragraph 1(a);

(c) the inspection of consignments of plants and plant products moving in international traffic and, where appropriate, the inspection of other regulated articles, particularly with the object of preventing the introduction and/or spread of pests;

(d) the disinfestation or disinfection of consignments of plants, plant products and other regulated articles moving in international traffic, to meet phytosanitary requirements;

(e) the protection of endangered areas and the designation, maintenance and surveillance of pest free areas and areas of low pest prevalence;

(f) the conduct of pest risk analyses;

(g) to ensure through appropriate procedures that the phytosanitary security of consignments after certification regarding compositing, substitution and reinfestation is maintained prior to export; and

(h) training and development of staff.

3.	Each contracting party shall make provision, to the best of its ability, for the following:

(a) the distribution of information within the territory of the contracting party regarding regulated pests and the means of their prevention and control;

(b) research and investigation in the field of plant protection;

(c) the issuance of phytosanitary regulations; and

(d) the performance of such other functions as may be required for the implementation of this Convention.

4.	Each contracting party shall submit a description of its official national plant protection organization and of changes in such organization to the Secretary. A contracting party shall provide a description of its organizational arrangements for plant protection to another contracting party, upon request.

Article V  Phytosanitary certification

1.	Each contracting party shall make arrangements for phytosanitary certification, with the objective of ensuring that exported plants, plant products and other regulated articles and consignments thereof are in conformity with the certifying statement to be made pursuant to paragraph 2(b) of this Article.

2.	Each contracting party shall make arrangements for the issuance of phytosanitary certificates in conformity with the following provisions:

(a) Inspection and other related activities leading to issuance of phytosanitary certificates shall be carried out only by or under the authority of the official national plant protection organization. The issuance of phytosanitary certificates shall be carried out by public offices who are technically qualified and duly authorized by the official national plant protection organization to act on its behalf and under its control with such knowledge and information available to those officers that the authorities of importing contracting parties may accept the phytosanitary certificates with confidence as dependable documents.

(b) Phytosanitary certificates, or their electronic equivalent where accepted by the importing contracting party concerned, shall be worded in the models set out in the Annex to this Convention. These certificates should be completed and issued taking into account relevant international standards.

(c) Uncertified alterations or erasures shall invalidate the certificates.

3.	Each contracting party undertakes not to require consignments of plants or plant products or other regulated articles imported into its territories to be accompanied by phytosanitary certificates inconsistent with the models set out in the Annex to this Convention. Any requirements for additional declarations shall be limited to those technically justified.

Article VI  Regulated pests

1.	Contracting parties may require phytosanitary measures for quarantine pests and regulated non-quarantine pests, provided that such measures are:

(a) no more stringent than measures applied to the same pests, if present within the territory of the importing contracting party; and

(b) limited to what is necessary to protect plant health and/or safeguard the intended use and can be technically justified by the contracting party concerned.

2.	Contracting parties shall not require phytosanitary measures for non-regulated pests.

Article VII  Requirements in relation to imports

1.	With the aim of preventing the introduction and/or spread of regulated pests into their territories, contracting parties shall have sovereign authority to regulate, in accordance with applicable international agreements, the entry of plants and plant products and other regulated articles and, to this end, may:

(a) prescribe and adopt phytosanitary measures concerning the importation of plants, plant products and other regulated articles, including, for example, inspection, prohibition on importation, and treatment;

(b) refuse entry or detain, or require treatment, destruction or removal from the territory of the contracting party, of plants, plant products and other regulated articles or consignments thereof that do not comply with the phytosanitary measures prescribed or adopted under subparagraph (a);

(c) prohibit or restrict the movement of regulated pests into their territories;

(d) prohibit or restrict the movement of biological control agents and other organisms of phytosanitary concern claimed to be beneficial into their territories.

2.	In order to minimize interference with international trade, each contracting party, in exercising its authority under paragraph 1 of this Article, undertakes to act in conformity with the following:

(a) Contracting parties shall not, under their phytosanitary legislation, take any of the measures specified in paragraph 1 of this Article unless such measures are made necessary by phytosanitary considerations and are technically justified.

(b) Contracting parties shall, immediately upon their adoption, publish and transmit phytosanitary requirements, restrictions and prohibitions to any contracting party or parties that they believe may be directly affected by such measures.

(c) Contracting parties shall, on request, make available to any contracting party the rationale for phytosanitary requirements, restrictions and prohibitions.

(d) If a contracting party requires consignments of particular plants or plant products to be imported only through specified points of entry, such points shall be so selected as not to unnecessarily impede international trade. The contracting party shall publish a list of such points of entry and communicate it to the Secretary, any regional plant protection organization of which the contracting party is a member, all contracting parties which the contracting party believes to be directly affected, and other contracting parties upon request. Such restrictions on points of entry shall not be made unless the plants, plant products or other regulated articles concerned are required to be accompanied by phytosanitary certificates or to be submitted to inspection or treatment.

(e) Any inspection or other phytosanitary procedure required by the plant protection organization of a contracting party for a consignment of plants, plant products or other regulated articles offered for importation, shall take place as promptly as possible with due regard to their perishability.

(f) Importing contracting parties shall, as soon as possible, inform the exporting contracting party concerned or, where appropriate, the re-exporting contracting party concerned, of significant instances of non-compliance with phytosanitary certification. The exporting contracting party or, where appropriate, the re-exporting contracting party concerned, should investigate and, on request, report the result of its investigation to the importing contracting party concerned.

(g) Contracting parties shall institute only phytosanitary measures that are technically justified, consistent with the pest risk involved and represent the least restrictive measures available, and result in the minimum impediment to the international movement of people, commodities and conveyances.

(h) Contracting parties shall, as conditions change, and as new facts become available, ensure that phytosanitary measures are promptly modified or removed if found to be unnecessary.

(i) Contracting parties shall, to the best of their ability, establish and update lists of regulated pests, using scientific names, and make such lists available to the Secretary, to regional plant protection organizations of which they are members and, on request, to other contracting parties.

(j) Contracting parties shall, to the best of their ability, conduct surveillance for pests and develop and maintain adequate information on pest status in order to support categorization of pests, and for the development of appropriate phytosanitary measures. This information shall be made available to contracting parties, on request.

3.	A contracting party may apply measures specified in this Article to pests which may not be capable of establishment in its territories but, if they gained entry, cause economic damage. Measures taken against these pests must be technically justified.

4.	Contracting parties may apply measures specified in this Article to consignments in transit through their territories only where such measures are technically justified and necessary to prevent the introduction and/or spread of pests.

5.	Nothing in this Article shall prevent importing contracting parties from making special provision, subject to adequate safeguards, for the importation, for the purpose of scientific research, education, or other specific use, of plants and plant products and other regulated articles, and of plant pests.

6.	Nothing in this Article shall prevent any contracting party from taking appropriate emergency action on the detection of a pest posing a potential threat to its territories or the report of such a detection. Any such action shall be evaluated as soon as possible to ensure that its continuance is justified. The action taken shall be immediately reported to contracting parties concerned, the Secretary, and any regional plant protection organization of which the contracting party is a member.

Article VIII  International cooperation

1.	The contracting parties shall cooperate with one another to the fullest practicable extent in achieving the aims of this Convention, and shall in particular:

(a) cooperate in the exchange of information on plant pests, particularly the reporting of the occurrence, outbreak or spread of pests that may be of immediate or potential danger, in accordance with such procedures as may be established by the Commission;

(b) participate, in so far as is practicable, in any special campaigns for combatting pests that may seriously threaten crop production and need international action to meet the emergencies; and

(c) cooperate, to the extent practicable, in providing technical and biological information necessary for pest risk analysis.

2.	Each contracting party shall designate a contact point for the exchange of information connected with the implementation of this Convention.

Article IX  Regional plant protection organizations

1.	The contracting parties undertake to cooperate with one another in establishing regional plant protection organizations in appropriate areas.

2.	The regional plant protection organizations shall function as the coordinating bodies in the areas covered, shall participate in various activities to achieve the objectives of this Convention and, where appropriate, shall gather and disseminate information.

3.	The regional plant protection organizations shall cooperate with the Secretary in achieving the objectives of the Convention and, where appropriate, cooperate with the Secretary and the Commission in developing international standards.

4.	The Secretary will convene regular Technical Consultations of representatives of regional plant protection organizations to:

(a) promote the development and use of relevant international standards for phytosanitary measures; and

(b) encourage inter-regional cooperation in promoting harmonized phytosanitary measures for controlling pests and in preventing their spread and/or introduction.

Article X  Standards

1.	The contracting parties agree to cooperate in the development of international standards in accordance with the procedures adopted by the Commission.

2.	International standards shall be adopted by the Commission.

3.	Regional standards should be consistent with the principles of this Convention; such standards may be deposited with the Commission for consideration as candidates for international standards for phytosanitary measures if more broadly applicable.

4.	Contracting parties should take into account, as appropriate, international standards when undertaking activities related to this Convention.

Article XI  Commission on Phytosanitary Measures

1.	Contracting parties agree to establish the Commission on Phytosanitary Measures within the framework of the Food and Agriculture Organization of the United Nations (FAO).

2.	The functions of the Commission shall be to promote the full implementation of the objectives of the Convention and, in particular, to:

(a) review the state of plant protection in the world and the need for action to control the international spread of pests and their introduction into endangered areas;

(b) establish and keep under review the necessary institutional arrangements and procedures for the development and adoption of international standards, and to adopt international standards;

(c) establish rules and procedures for the resolution of disputes in accordance with Article XIII;

(d) establish such subsidiary bodies of the Commission as may be necessary for the proper implementation of its functions;

(e) adopt guidelines regarding the recognition of regional plant protection organizations;

(f) establish cooperation with the other relevant international organizations on matters covered by this Convention;

(g) adopt such recommendations for the implementation of the Convention as necessary; and

(h) perform such other functions as may be necessary to the fulfilment of the objectives of this Convention.

3.	Membership in the Commission shall be open to all contracting parties.

4.	Each contracting party may be represented at sessions of the Commission by a single delegate who may be accompanied by an alternate, and by experts and advisers. Alternates, experts and advisers may take part in the proceedings of the Commission but may not vote, except in the case of an alternate who is duly authorized to substitute for the delegate.

5.	The contracting parties shall make every effort to reach agreement on all matters by consensus. If all efforts to reach consensus have been exhausted and no agreement is reached, the decision shall, as a last resort, be taken by a two-thirds majority of the contracting parties present and voting.

6.	A member organization of FAO that is a contracting party and the member states of that member organization that are contracting parties shall exercise their membership rights and fulfil their membership obligations in accordance, mutatis mutandis, with the Constitution and General Rules of FAO.

7.	The Commission may adopt and amend, as required, its own Rules of Procedure, which shall not be inconsistent with this Convention or with the Constitution of FAO.

8.	The Chairperson of the Commission shall convene an annual regular session of the Commission.

9.	Special sessions of the Commission shall be convened by the Chairperson of the Commission at the request of at leat one-third of its members.

10.	The Commission shall elect its Chairperson and no more than two Vice-Chairpersons, each of whom shall serve for a term of two years.

Article XII  Secretariat

1.	The Secretary of the Commission shall be appointed by the Director-General of FAO.

2.	The Secretary shall be assisted by such secretariat staff as may be required.

3.	The Secretary shall be responsible for implementing the policies and activities of the Commission and carrying out such other functions as may be assigned to the Secretary by this Convention and shall report thereon to the Commission.

4.	The Secretary shall disseminate:

(a) international standards to all contracting parties within sixty days of adoption;

(b) to all contracting parties, lists of points of entry under Article VII paragraph 2(d) communicated by contracting parties;

(c) lists of regulated pests whose entry is prohibited or referred to in Article VII paragraph 2(i) to all contracting parties and regional plant protection organizations;

(d) information received from contracting parties on phytosanitary requirements, restrictions and prohibitions referred to in Article VII paragraph 2(b), and descriptions of official national plant protection organizations referred to in Article IV paragraph 4.

5.	The Secretary shall provide translations in the official languages of FAO of documentation for meetings of the Commission and international standards.

6.	The Secretary shall cooperate with regional plant protection organizations in achieving the aims of the Convention.

Article XIII  Settlement of disputes

1.	If there is any dispute regarding the interpretation or application of this Convention, or if a contracting party considers that any action by another contracting party is in conflict with the obligations of the latter under Articles V and VII of this Convention, especially regarding the basis of prohibiting or restricting the imports of plants, plant products or other regulated articles coming from its territories, the contracting parties concerned shall consult among themselves as soon as possible with a view to resolving the dispute.

2.	If the dispute cannot be resolved by the means referred to in paragraph 1, the contracting party or parties concerned may request the Director-General of FAO to appoint a committee of experts to consider the question in dispute, in accordance with rules and procedures that may be established by the Commission.

3.	This Committee shall include representatives designated by each contracting party concerned. The Committee shall consider the question in dispute, taking into account all documents and other forms of evidence submitted by the contracting parties concerned. The Committee shall prepare a report on the technical aspects of the dispute for the purpose of seeking its resolution. The preparation of the report and its approval shall be according to rules and procedures established by the Commission, and it shall be transmitted by the Director-General to the contracting parties concerned. The report may also be submitted, upon its request, to the competent body of the international organization responsible for resolving trade disputes.

4.	The contracting parties agree that the recommendations of such a committee, while not binding in character, will become the basis for renewed consideration by the contracting parties concerned of the matter out of which the disagreement arose.

5.	The contracting parties concerned shall share the expenses of the experts.

6.	The provisions of this Article shall be complementary to and not in derogation of the dispute settlement procedures provided for in other international agreements dealing with trade matters.

Article XIV  Substitution of prior agreements

This Convention shall terminate and replace, between contracting parties, the International Convention respecting measures to be taken against the Phylloxera vastatrix of 3 November 1881, the additional Convention signed at Berne on 15 April 1889 and the International Convention for the Protection of Plants signed at Rome on 16 April 1929.

Article XV  Territorial application

1.	Any contracting party may at the time of ratification or adherence or at any time thereafter communicate to the Director-General of FAO a declaration that this Convention shall extend to all or any of the territories for the international relations of which it is responsible, and this Convention shall be applicable to all territories specified in the declaration as from the thirtieth day after the receipt of the declaration by the Director-General.

2.	Any contracting party which has communicated to the Director-General of FAO a declaration in accordance with paragraph 1 of this Article may at any time communicate a further declaration modifying the scope of any former declaration or terminating the application of the provisions of the present Convention in respect of any territory. Such modification or termination shall take effect as from the thirtieth day after the receipt of the declaration by the Director-General.

3.	The Director-General of FAO shall inform all contracting parties of any declaration received under this Article.

Article XVI  Supplementary agreements

1.	The contracting parties may, for the purpose of meeting special problems of plant protection which need particular attention or action, enter into supplementary agreements. Such agreements may be applicable to specific regions, to specific pests, to specific plants and plant products, to specific methods of international transportation of plants and plant products, or otherwise supplement the provisions of this Convention.

2.	Any such supplementary agreements shall come into force for each contracting party concerned after acceptance in accordance with the provisions of the supplementary agreements concerned.

3.	Supplementary agreements shall promote the intent of this Convention and shall conform to the principles and provisions of this Convention, as well as to the principles of transparency, non-discrimination and the avoidance of disguised restrictions, particularly on international trade.

Article XVII  Ratification and adherence

1.	This Convention shall be open for signature by all states until 1 May 1952 and shall be ratified at the earliest possible date. The instruments of ratification shall be deposited with the Director-General of FAO, who shall give notice of the date of deposit to each of the signatory states.

2.	As soon as this Convention has come into force in accordance with Article XXII it shall be open for adherence by non-signatory states and member organizations of FAO. Adherence shall be effected by the deposit of an instrument of adherence with the Director-General of FAO, who shall notify all contracting parties.

3.	When a member organization of FAO becomes a contracting party to this Convention, the member organization shall, in accordance with the provisions of Article II paragraph 7 of the FAO Constitution, as appropriate, notify at the time of its adherence such modifications or clarifications to its declaration of competence submitted under Article II paragraph 5 of the FAO Constitution as may be necessary in light of its acceptance of this Convention. Any contracting party to this Convention may, at any time, request a member organization of FAO that is a contracting party to this Convention to provide information as to which, as between the member organization and its member states, is responsible for the implementation of any particular matter covered by this Convention. The member organization shall provide this information within a reasonable time.

Article XVIII  Non-contracting parties

The contracting parties shall encourage any state or member organization of FAO, not a party to this Convention, to accept this Convention, and shall encourage any non-contracting party to apply phytosanitary measures consistent with the provisions of this Convention and any international standards adopted hereunder.

Article XIX  Languages

1.	The authentic languages of this Convention shall be all official languages of FAO.

2.	Nothing in this Convention shall be construed as requiring contracting parties to provide and to publish documents or to provide copies of them other than in the language(s) of the contracting party, except as stated in paragraph 3 below.

3.	The following documents shall be in at least one of the official languages of FAO:

(a) information provided according to Article IV paragraph 4;

(b) cover notes giving bibliographical data on documents transmitted according to Article VII paragraph 2(b);

(c) information provided according to Article VII paragraph 2(b), (d), (i) and (j);

(d) notes giving bibliographical data and a short summary of relevant documents on information provided according to Article VIII paragraph 1(a);

(e) requests for information from contact points as well as replies to such requests, but not including any attached documents;

(f) any document made available by contracting parties for meetings of the Commission.

Article XX  Technical assistance

The contracting parties agree to promote the provision of technical assistance to contracting parties, especially those that are developing contracting parties, either bilaterally or through the appropriate international organizations, with the objective of facilitating the implementation of this Convention.

Article XXI  Amendment

1.	Any proposal by a contracting party for the amendment of this Convention shall be communicated to the Director-General of FAO.

2.	Any proposed amendment of this Convention received by the Director-General of FAO from a contracting party shall be presented to a regular or special session of the Commission for approval and, if the amendment involves important technical changes or imposes additional obligations on the contracting parties, it shall be considered by an advisory committee of specialists convened by FAO prior to the Commission.

3.	Notice of any proposed amendment of this Convention, other than amendments to the Annex, shall be transmitted to the contracting parties by the Director-General of FAO not later than the time when the agenda of the session of the Commission at which the matter is to be considered is dispatched.

4.	Any such proposed amendment of this Convention shall require the approval of the Commission and shall come into force as from the thirtieth day after acceptance by two-thirds of the contracting parties. For the purpose of this Article, an instrument deposited by a member organization of FAO shall not be counted as additional to those deposited by member states of such an organization.

5.	Amendments involving new obligations for contracting parties, however, shall come into force in respect of each contracting party only on acceptance by it and as from the thirtieth day after such acceptance. The instruments of acceptance of amendments involving new obligations shall be deposited with the Director-General of FAO, who shall inform all contracting parties of the receipt of acceptance and the entry into force of amendments.

6.	Proposals for amendments to the model phytosanitary certificates set out in the Annex to this Convention shall be sent to the Secretary and shall be considered for approval by the Commission. Approved amendments to the model phytosanitary certificates set out in the Annex to this Convention shall become effective ninety days after their notification to the contracting parties by the Secretary.

7.	For a period of not more than twelve months from an amendment to the model phytosanitary certificates set out in the Annex to this Convention becoming effective, the previous version of the phytosanitary certificates shall also be legally valid for the purpose of this Convention.

Article XXII  Entry into force

As soon as this Convention has been ratified by three signatory states it shall come into force among them. It shall come into force for each state or member organization of FAO ratifying or adhering thereafter from the date of deposit of its instrument of ratification or adherence.

Article XXIII  Denunciation

1.	Any contracting party may at any time give notice of denunciation of this Convention by notification addressed to the Director-General of FAO. The Director-General shall at once inform all contracting parties.

2.	Denunciation shall take effect one year from the date of receipt of the notification by the Director-General of FAO.

ANNEX  Model Phytosanitary Certificate

No.

Plant Protection Organization of

TO: Plant Protection Organization(s) of

I. Description of Consignment

Name and address of exporter:

Declared name and address of consignee:

Number and description of packages:

Distinguishing marks:

Place of origin:

Declared means of conveyance:

Declared point of entry:

Name of produce and quantity declared:

Botanical name of plants:

This is to certify that the plants, plant products or other regulated articles described herein have been inspected and/or tested according to appropriate official procedures and are considered to be free from the quarantine pests specified by the importing contracting party and to conform with the current phytosanitary requirements of the importing contracting party, including those for regulated non-quarantine pests.

They are deemed to be practically free from other pests.*

II. Additional Declaration  III. Disinfestation and/or Disinfection Treatment

Date ______ Treatment _________ Chemical (active ingredient)

Duration and temperature

Concentration

Additional information


	Place of issue
(Stamp of Organization)	Name of authorized officer ___________________
	Date _____________

Model Phytosanitary Certificate

No.

Plant Protection Organization of

TO: Plant Protection Organization(s) of

I. Description of Consignment

Name and address of exporter:

Declared name and address of consignee:

Number and description of packages:

Distinguishing marks:

Place of origin:

Declared means of conveyance:

Declared point of entry:

Name of produce and quantity declared:

Botanical name of plants:

This is to certify that the plants, plant products or other regulated articles described herein have been inspected and/or tested according to appropriate official procedures and are considered to be free from the quarantine pests specified by the importing contracting party and to conform with the current phytosanitary requirements of the importing contracting party, including those for regulated non-quarantine pests.

They are deemed to be practically free from other pests.*

II. Additional Declaration  III. Disinfestation and/or Disinfection Treatment

Date ______ Treatment _________ Chemical (active ingredient)

Duration and temperature

Concentration

Additional information


	Place of issue
(Stamp of Organization)	Name of authorized officer ___________________
	Date _____________ _____________________
	(Signature)

No financial liability with respect to this certificate shall attach to ____________ (name of Plant Protection Organization) or to any of its officers or representatives.*

*	Optional clause.

Model Phytosanitary Certificate for Re-Export

No.

Plant Protection Organization

of 	 (contracting party of re-export)

TO: Plant Protection Organization(s)

of 	 (contracting party(ies) of import)

I. Description of Consignment

Name and address of exporter:

Declared name and address of consignee:

Number and description of packages:

Distinguishing marks:

Place of origin:

Declared means of conveyance:

Declared point of entry:

Name of produce and quantity declared:

Botanical name of plants:

This is to certify that the plants, plant products or other regulated articles described above ____________ were imported into (contracting party of re-export) ___________ from ________________ (contracting party of origin) covered by Phytosanitary Certificate No. ________, *original ¨ certified true copy ¨ of which is attached to this certificate; that they are packed ¨ repacked ¨ in original ¨ *new ¨ containers, that based on the original phytosanitary certificate ¨ and additional inspection ¨ , they are considered to conform with the current phytosanitary requirements of the importing contracting party, and that during storage in ________________ (contracting party of re-export), the consignment has not been subjected to the risk of infestation or infection.

*	Insert tick in appropriate ¨ boxes.

II. Additional Declaration  III. Disinfestation and/or Disinfection Treatment

Date ______ Treatment _________ Chemical (active ingredient)

Duration and temperature

Concentration

Additional information


	Place of issue
(Stamp of Organization)	Name of authorized officer ___________________
	Date _____________ ____________________
	(Signature)

No financial liability with respect to this certificate shall attach to __________ (name of Plant Protection Organization) or to any of its officers or representatives.**

**	Optional clause

-----
[Source: http://www.austlii.edu.au/au/other/dfat/treaties/ATS/2005/23.html (last retrieved 2020-07-27, last modified 2020-08-19)]
[CC BY 3.0 AU Australian Government, Department of Foreign Affairs and Trade]
ILO CONVENTION (No. 139) CONCERNING PREVENTION AND CONTROL OF OCCUPATIONAL HAZARDS CAUSED BY CARCINOGENIC SUBSTANCES AND AGENTS



THE GENERAL CONFERENCE OF THE INTERNATIONAL LABOUR ORGANISATION,

HAVING BEEN CONVENED at Geneva by the Governing Body of the International Labour Office, and having met in its Fifty-ninth Session on 5 June 1974, and

NOTING the terms of the Radiation Protection Convention and Recommendation, 1960, and of the Benzene Convention and Recommendation, 1971, and

CONSIDERING that it is desirable to establish international standards concerning protection against carcinogenic substances or agents, and

TAKING ACCOUNT of the relevant work of other international organisations, and in particular of the World Health Organisation and the International Agency for Research on Cancer, with which the International Labour Organisation collaborates, and

HAVING DECIDED upon the adoption of certain proposals regarding control and prevention of occupational hazards caused by carcinogenic substances and agents, which is the fifth item on the agenda of the session, and

HAVING DETERMINED that these proposals shall take the form of an international Convention,

ADOPTS this twenty-fourth day of June in the year of one thousand nine hundred and seventy-four the following Convention, which may be cited as the Occupational Cancer Convention, 1974:

Article 1

1.	Each Member which ratifies this Convention shall periodically determine the carcinogenic substances and agents to which occupational exposure shall be prohibited or made subject to authorisation or control, and those to which other provisions of this Convention shall apply.

2.	Exemptions from prohibition may only be granted by issue of a certificate specifying in each case the conditions to be met.

3.	In making the determinations required by paragraph 1 of this Article, consideration shall be given to the latest information contained in the codes of practice or guides which may be established by the International Labour Office, as well as to information from other competent bodies.

Article 2

1.	Each Member which ratifies this Convention shall make every effort to have carcinogenic substances and agents to which workers may be exposed in the course of their work replaced by non-carcinogenic substances or agents or by less harmful substances or agents; in the choice of substitute substances or agents account shall be taken of their carcinogenic, toxic and other properties.

2.	The number of workers exposed to carcinogenic substances or agents and the duration and degree of such exposure shall be reduced to the minimum compatible with safety.

Article 3

Each Member which ratifies this Convention shall prescribe the measures to be taken to protect workers against the risks of exposure to carcinogenic substances or agents and shall ensure the establishment of an appropriate system of records.

Article 4

Each Member which ratifies this Convention shall take steps so that workers who have been, are, or are likely to be exposed to carcinogenic substances or agents are provided with all the available information on the dangers involved and on the measures to be taken.

Article 5

Each Member which ratifies this Convention shall take measures to ensure that workers are provided with such medical examinations or biological or other tests or investigations during the period of employment and thereafter as are necessary to evaluate their exposure and supervise their state of health in relation to the occupational hazards.

Article 6

Each Member which ratifies this Convention -

(a)	shall, by laws or regulations or any other method consistent with national practice and conditions and in consultation with the most representative organisations of employers and workers concerned, take such steps as may be necessary to give effect to the provisions of this Convention;

(b)	shall, in accordance with national practice, specify the persons or bodies on whom the obligation of compliance with the provisions of this Convention rests;

(c)	undertakes to provide appropriate inspection services for the purpose of supervising the application of this Convention, or to satisfy itself that appropriate inspection is carried out.

Article 7

The formal ratifications of this Convention shall be communicated to the Director-General of the International Labour Office for registration.

Article 8

1.	This Convention shall be binding only upon those Members of the International Labour Organisation whose ratifications have been registered with the Director-General.

2.	It shall come into force twelve months after the date on which the ratifications of two Members have been registered with the Director-General.

3.	Thereafter, this Convention shall come into force for any Member twelve months after the date on which its ratification has been registered.

Article 9

1.	A Member which has ratified this Convention may denounce it after the expiration of ten years from the date on which the Convention first comes into force, by an act communicated to the Director-General of the International Labour Office for registration. Such denunciation shall not take effect until one year after the date on which it is registered.

2.	Each Member which has ratified this Convention and which does not, within the year following the expiration of the period of ten years mentioned in the preceding paragraph, exercise the right of denunciation provided for in this Article, will be bound for another period of ten years and, thereafter, may denounce this Convention at the expiration of each period of ten years under the terms provided for in this Article.

Article 10

1.	The Director-General of the International Labour Office shall notify all Members of the International Labour Organisation of the registration of all ratifications and denunciations communicated to him by the Members of the Organisation.

2.	When notifying the Members of the Organisation of the registration of the second ratification communicated to him, the Director-General shall draw the attention of the Members of the Organisation to the date upon which the Convention will come into force.

Article 11

The Director-General of the International Labour Office shall communicate to the Secretary-General of the United Nations for registration in accordance with Article 102 of the Charter of the United Nations full particulars of all ratifications and acts of denunciation registered by him in accordance with the provisions of the preceding Articles.

Article 12

At such times as it may consider necessary the Governing Body of the International Labour Office shall present to the General Conference a report on the working of this Convention and shall examine the desirability of placing on the agenda of the Conference the question of its revision in whole or in part.

Article 13

1.	Should the Conference adopt a new Convention revising this Convention in whole or in part, then, unless the new Convention otherwise provides -

(a)	the ratification by a Member of the new revising Convention shall ipso jure involve the immediate denunciation of this Convention, notwithstanding the provisions of Article 9 above, if and when the new revising Convention shall have come into force;

(b)	as from the date when the new revising Convention comes into force this Convention shall cease to be open to ratification by the Members.

2.	This Convention shall in any case remain in force in its actual form and content for those Members which have ratified it but have not ratified the revising Convention.

Article 14

The English and French versions of the text of this Convention are equally authoritative.

-----
[Source: http://www.austlii.edu.au/au/other/dfat/seldoc/1974/2307.html (last retrieved 2020-07-29)]
[CC BY 3.0 AU Australian Government, Department of Foreign Affairs and Trade]
CONVENTION CONCERNING THE PROTECTION OF WORKERS AGAINST IONIZING RADIATIONS


The General Conference of the International Labour Organization,

Having been convened at Geneva by the Governing Body of the International Labour Office, and having met in its Forty-fourth Session on 1 June 1960, and

Having decided upon the adoption of certain proposals with regard to the protection of workers against ionising radiations, which is the fourth item on the agenda of the session, and having determined that these proposals shall take the form of an international Convention,

adopts this twenty-second day of June of the year one thousand nine hundred and sixty the following Convention, which may be cited as the Radiation Protection Convention, 1960:

Part I

GENERAL PROVISIONS

Article 1

Each Member of the International Labour Organization which ratifies this Convention undertakes to give effect thereto by means of laws or regulations, codes of practice or other appropriate means. In applying the provisions of the Convention the competent authority shall consult with representatives of employers and workers.

Article 2

1.This Convention applies to all activities involving exposure of workers to ionising radiations in the course of their work.

2.This Convention does not apply to radioactive substances, whether sealed or unsealed, nor to apparatus generating ionising radiations which substances or apparatus, owing to the limited doses of ionising radiations which can be received from them, are exempted from its provisions by one of the methods of giving effect to the Convention mentioned in Article 1.

Article 3

1.In the light of knowledge available at the time, all appropriate steps shall be taken to ensure effective protection of workers, as regards their health and safety, against ionising radiations.

2.Rules and measures necessary for this purpose shall be adopted, and data essential for effective protection shall be made available.

3.With a view to ensuring such effective protection:

a)measures for the protection of workers against ionising radiations adopted after ratification of the Convention by the Member concerned shall comply with the provisions thereof;

b)the Member concerned shall modify, as soon as practicable, measures adopted by it prior to the ratification of the Convention, so as to comply with the provisions thereof, and shall promote such modification of other measures existing at the time of ratification;

c)the Members concerned shall communicate to the Director-General of the International Labour Office, when ratifying the Convention, a statement indicating the manner in which and the categories of workers to which the provisions of the Convention are applied, and shall indicate in its reports on the application of the Convention any further progress made in the matter;

d)at the expiration of three years from the date on which this Convention first enters into force the Governing Body of the International Labour Office shall submit to the Conference a special report concerning the application of sub-paragraph (b) of this paragraph and containing such proposals as it may think appropriate for further action in regard to the matter.

Part II

PROTECTIVE MEASURES

Article 4

The activities referred to in Article 2 shall be so arranged and conducted as to afford protection envisaged in this Part of the Convention.

Article 5

Every effort shall be made to restrict the exposure of workers to ionising radiations to the lowest practicable level, and any unnecessary exposure shall be avoided by all parties concerned.

Article 6

1.Maximum permissible doses of ionising radiations which may be received from sources external to or internal to the body and maximum permissible amounts of radioactive substances which can be taken into the body shall be fixed in accordance with Part I of this Convention for various categories of workers.

2.Such maximum permissible doses and amounts shall be kept under constant review in the light of current knowledge.

Article 7

1.Appropriate levels shall be fixed in accordance with Article 6 for workers who are directly engaged in radiation work and are:

a)aged 18 and over;

b)under the age of 18.

2.No worker under the age of 16 shall be engaged in work involving ionising radiations.

Article 8

Appropriate levels shall be fixed in accordance with Article 6 for workers who are not directly engaged in radiation work but who remain or pass where they may be exposed to ionising radiations or radioactive substances.

Article 9

1.Appropriate warnings shall be used to indicate the presence of hazards from ionising radiations.

Any information necessary in this connection shall be supplied to the workers.

2.All workers directly engaged in radiation work shall be adequately instructed, before and during such employment, in the precautions to be taken or their protection, as regards their health and safety, and the reasons therefor.

Article 10

Laws or regulations shall require the notification in a manner prescribed thereby of work involving exposure of workers to ionising radiations in the course of their work.

Article 11

Appropriate monitoring of workers and places of work shall be carried out in order to measure the exposure of workers to ionising radiations and radioactive substances, with a view to ascertaining that the applicable levels are respected.

Article 12

All workers directly engaged in radiation work shall undergo an appropriate medical examination prior to or shortly after taking up such work and subsequently undergo further medical examinations at appropriate intervals.

Article 13

Circumstances shall be specified, by one of the methods of giving effect to the Convention mentioned in Article 1, in which, because of the nature or degree of the exposure or a combination of both, the following action shall be taken promptly:

a)the worker shall undergo an appropriate medical examination;

b)the employer shall notify the competent authority in accordance with its requirements;

c)persons competent in radiation protection shall examine the conditions in which the worker's duties are performed;

d)the employer shall take any necessary remedial action on the basis of the technical findings and the medical advice.

Article 14

No worker shall be employed or shall continue to be employed in work by reason of which the worker could be subject to exposure to ionising radiations contrary to qualified medical advice.

Article 15

Each Member which ratifies this Convention undertakes to provide appropriate inspection services for the purpose of supervising the application of its provisions, or to satisfy itself that appropriate inspection is carried out.

Part III

FINAL PROVISIONS

Article 16

The formal ratifications of this Convention shall be communicated to the Director-General of the International Labour Office for registration.

Article 17

1.This Convention shall be binding only upon those Members of the International Labour Organization whose ratifications have been registered with the Director-General.

2.It shall come into force twelve months after the date on which the ratifications of two Members have been registered with the Director-General.

3.Thereafter, this Convention shall come into force for any member twelve months after the date on which its ratification has been registered.

Article 18

1.A Member which has ratified this Convention may denounce it after the expiration of five years from the date on which the Convention first comes into force, by an act communicated to the Director-General of the International Labour Office for registration. Such denunciation shall not take effect until one year after the date on which it is registered.

2.Each Member which has ratified this Convention and which does not, within the year following the expiration of the period of five years mentioned in the preceding paragraph, exercise the right of denunciation provided for in this Article will be bound for another period of five years and, thereafter, may denounce this Convention at the expiration of each period of five years under the terms provided for in this Article.

Article 19

1.The Director-General of the International Labour Office shall notify all Members of the International Labour Organization of the registration of all ratifications and denunciations communicated to him by the Members of the Organization.

2.When notifying the Members of the Organization of the registration of the second ratification communicated to him, the Director-General shall draw the attention of the Members of the Organization to the date upon which the Convention will come into force.

Article 20

The Director-General of the International Labour Office shall communicate to the Secretary-General of the United Nations for registration in accordance with Article 102 of the Charter of the United Nations full particulars of all ratifications and acts of denunciation registered by him in accordance with the provisions of the preceding Articles.

Article 21

At such times as it may consider necessary the Governing Body of the International Labour Office shall present to the General Conference a report on the working of this Convention and shall examine the desirability of placing on the agenda of the Conference the question of its revision in whole or in part.

Article 22

1.Should the Conference adopt a new Convention revising this Convention in whole or in part, then, unless the new Convention otherwise provides:

a)the ratification by a Member of the new revising Convention shall ipso jure involve the immediate denunciation of this Convention, notwithstanding the provisions of Article 18 above, if and when the new revising Convention shall have come into force;

b)as from the date when the new revising Convention comes into force this Convention shall cease to be open to ratification by the Members.

2.This Convention shall in any case remain in force in its actual form and content for those Members which have ratified it but have not ratified the revising Convention.

Article 23

The English and French versions of the text of this Convention are equally authoritative.

-----
[Source: https://iea.uoregon.edu/treaty-text/1960-protectionworkersagainstionizingradiationsentxt (last retrieved 2020-08-01, last modified 2020-08-19)]
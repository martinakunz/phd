PROTOCOL ON PREPAREDNESS, RESPONSE AND CO-OPERATION  TO POLLUTION INCIDENTS BY HAZARDOUS  AND NOXIOUS SUBSTANCES, 2000

THE PARTIES TO THE PRESENT PROTOCOL,

BEING PARTIES to the International Convention on Oil Pollution Preparedness, Response and Co-operation, done at London on 30 November 1990,

TAKING INTO ACCOUNT Resolution 10, on the expansion of the scope of the International Convention on Oil Pollution Preparedness, Response and Co-operation 1990, to include hazardous and noxious substances, adopted by the Conference on International Co-operation on Oil Pollution Preparedness and Response 1990,

FURTHER TAKING INTO ACCOUNT that pursuant to Resolution 10 of the Conference on International Co-operation on Oil Pollution Preparedness and Response 1990, the International Maritime Organization has intensified its work, in collaboration with all interested international organizations, on all aspects of preparedness, response and co-operation to pollution incidents by hazardous and noxious substances,

TAKING ACCOUNT of the "polluter pays" principle as a general principle of international environmental law,

BEING MINDFUL of the development of a strategy for incorporating the precautionary approach in the policies of the International Maritime Organization,

MINDFUL ALSO that, in the event of a pollution incident by hazardous and noxious substances, prompt and effective action is essential in order to minimize the damage which may result from such an incident,

HAVE AGREED as follows:

ARTICLE 1 General provisions

(1)	Parties undertake, individually or jointly, to take all appropriate measures in accordance with the provisions of this Protocol and the Annex thereto to prepare for and respond to a pollution incident by hazardous and noxious substances.

(2)	The Annex to this Protocol shall constitute an integral part of this Protocol and a reference to this Protocol constitutes at the same time a reference to the Annex.

(3)	This Protocol shall not apply to any warship, naval auxiliary or other ship owned or operated by a State and used, for the time being, only on government non-commercial service. However, each Party shall ensure by the adoption of appropriate measures not impairing the operations or operational capabilities of such ships owned or operated by it, that such ships act in a manner consistent, so far as is reasonable and practicable, with this Protocol.

ARTICLE 2 Definitions

For the purposes of this Protocol:

(1)	Pollution incident by hazardous and noxious substances (hereinafter referred to as "pollution incident") means any occurrence or series of occurrences having the same origin, including fire or explosion, which results or may result in a discharge, release or emission of hazardous and noxious substances and which poses or may pose a threat to the marine environment, or to the coastline or related interests of one or more States, and which requires emergency action or immediate response.

(2)	Hazardous and noxious substances means any substance other than oil which, if introduced into the marine environment is likely to create hazards to human health, to harm living resources and marine life, to damage amenities or to interfere with other legitimate uses of the sea.

(3)	Sea ports and hazardous and noxious substances handling facilities means those ports or facilities where such substances are loaded into or unloaded from ships.

(4)	Organization means the International Maritime Organization.

(5)	Secretary-General means the Secretary-General of the Organization.

(6)	OPRC Convention means the International Convention on Oil Pollution Preparedness, Response and Co-operation, 1990.

ARTICLE 3  Emergency plans and reporting

(1)	Each Party shall require that ships entitled to fly its flag have on-board a pollution incident emergency plan and shall require masters or other persons having charge of such ships to follow reporting procedures to the extent required. Both planning requirements and reporting procedures shall be in accordance with applicable provisions of the conventions developed within the Organization which have entered into force for that Party. On-board pollution incident emergency plans for offshore units, including Floating Production, Storage and Offloading Facilities and Floating Storage Units, should be dealt with under national provisions and/or company environmental management systems, and are excluded from the application of this article.

(2)	Each Party shall require that authorities or operators in charge of sea ports and hazardous and noxious substances handling facilities under its jurisdiction as it deems appropriate have pollution incident emergency plans or similar arrangements for hazardous and noxious substances that it deems appropriate which are co-ordinated with the national system established in accordance with article 4 and approved in accordance with procedures established by the competent national authority.

(3)	When the appropriate authorities of a Party learn of a pollution incident, they shall notify other States whose interests are likely to be affected by such incident.

ARTICLE 4 National and regional systems for preparedness and response

(1)	Each Party shall establish a national system for responding promptly and effectively to pollution incidents. This system shall include as a minimum:

(a)	the designation of:

(i)	the competent national authority or authorities with responsibility for preparedness for and response to pollution incidents;

(ii)	the national operational contact point or points; and

(iii)	an authority which is entitled to act on behalf of the State to request assistance or to decide to render the assistance requested;

(b)	a national contingency plan for preparedness and response which includes the organizational relationship of the various bodies involved, whether public or private, taking into account guidelines developed by the Organization.

(2)	In addition, each Party within its capabilities either individually or through bilateral or multilateral co-operation and, as appropriate, in co-operation with the shipping industries and industries dealing with hazardous and noxious substances, port authorities and other relevant entities, shall establish:

(a)	a minimum level of pre-positioned equipment for responding to pollution incidents commensurate with the risk involved, and programmes for its use;

(b)	a programme of exercises for pollution incident response organizations and training of relevant personnel;

(c)	detailed plans and communication capabilities for responding to a pollution incident. Such capabilities should be continuously available; and

(d)	a mechanism or arrangement to co-ordinate the response to a pollution incident with, if appropriate, the capabilities to mobilize the necessary resources.

(3)	Each Party shall ensure that current information is provided to the Organization, directly or through the relevant regional organization or arrangements, concerning:

(a)	the location, telecommunication data and, if applicable, areas of responsibility of authorities and entities referred to in paragraph (1)(a);

(b)	information on pollution response equipment and expertise in disciplines related to pollution incident response and marine salvage which may be made available to other States, upon request; and

(c)	its national contingency plan.

ARTICLE 5  International co-operation in pollution response

(1)	Parties agree that, subject to their capabilities and the availability of relevant resources, they will co-operate and provide advisory services, technical support and equipment for the purpose of responding to a pollution incident, when the severity of the incident so justifies, upon the request of any Party affected or likely to be affected. The financing of the costs for such assistance shall be based on the provisions set out in the Annex to this Protocol.

(2)	A Party which has requested assistance may ask the Organization to assist in identifying sources of provisional financing of the costs referred to in paragraph (1).

(3)	In accordance with applicable international agreements, each Party shall take necessary legal or administrative measures to facilitate:

(a)	the arrival and utilization in and departure from its territory of ships, aircraft and other modes of transport engaged in responding to a pollution incident or transporting personnel, cargoes, materials and equipment required to deal with such an incident; and

(b)	the expeditious movement into, through, and out of its territory of personnel, cargoes, materials and equipment referred to in subparagraph (a).

ARTICLE 6  Research and development

(1)	Parties agree to co-operate directly or, as appropriate, through the Organization or relevant regional organizations or arrangements in the promotion and exchange of results of research and development programmes relating to the enhancement of the state-of-the-art of preparedness for and response to pollution incidents, including technologies and techniques for surveillance, containment, recovery, dispersion, clean-up and otherwise minimizing or mitigating the effects of pollution incidents, and for restoration.

(2)	To this end, Parties undertake to establish directly or, as appropriate, through the Organization or relevant regional organizations or arrangements, the necessary links between Parties' research institutions.

(3)	Parties agree to co-operate directly or through the Organization or relevant regional organizations or arrangements to promote, as appropriate, the holding on a regular basis of international symposia on relevant subjects, including technological advances in techniques and equipment for responding to pollution incidents.

(4)	Parties agree to encourage, through the Organization or other competent international organizations, the development of standards for compatible hazardous and noxious substances pollution combating techniques and equipment.

ARTICLE 7 Technical co-operation

(1)	Parties undertake directly or through the Organization and other international bodies, as appropriate, in respect of preparedness for and response to pollution incidents, to provide support for those Parties which request technical assistance:

(a)	to train personnel;

(b)	to ensure the availability of relevant technology, equipment and facilities;

(c)	to facilitate other measures and arrangements to prepare for and respond to pollution incidents; and

(d)	to initiate joint research and development programmes.

(2)	Parties undertake to co-operate actively, subject to their national laws, regulations and policies, in the transfer of technology in respect of preparedness for and response to pollution incidents.

ARTICLE 8 Promotion of bilateral and multilateral co-operation in preparedness and response

Parties shall endeavour to conclude bilateral or multilateral agreements for preparedness for and response to pollution incidents. Copies of such agreements shall be communicated to the Organization which should make them available on request to the Parties.

ARTICLE 9 Relation to other conventions and other agreements

Nothing in this Protocol shall be construed as altering the rights or obligations of any Party under any other convention or international agreement.

ARTICLE 10 Institutional arrangements

(1)	Parties designate the Organization, subject to its agreement and the availability of adequate resources to sustain the activity, to perform the following functions and activities:

(a)	information services:

(i)	to receive, collate and disseminate on request the information provided by Parties and relevant information provided by other sources; and

(ii)	to provide assistance in identifying sources of provisional financing of costs;

(b)	education and training:

(i)	to promote training in the field of preparedness for and response to pollution incidents; and

(ii)	to promote the holding of international symposia;

(c)	technical services:

(i)	to facilitate co-operation in research and development;

(ii)	to provide advice to States establishing national or regional response capabilities; and

(iii)	to analyse the information provided by Parties and relevant information provided by other sources and provide advice or information to States;

(d)	technical assistance:

(i)	to facilitate the provision of technical assistance to States establishing national or regional response capabilities; and

(ii)	to facilitate the provision of technical assistance and advice, upon the request of States faced with major pollution incidents.

(2)	In carrying out the activities specified in this article, the Organization shall endeavour to strengthen the ability of States individually or through regional arrangements to prepare for and combat pollution incidents, drawing upon the experience of States, regional agreements and industry arrangements and paying particular attention to the needs of developing countries.

(3)	The provisions of this article shall be implemented in accordance with a programme developed and kept under review by the Organization.

ARTICLE 11  Evaluation of the Protocol

Parties shall evaluate within the Organization the effectiveness of the Protocol in the light of its objectives, particularly with respect to the principles underlying co-operation and assistance.

ARTICLE 12  Amendments
(1)	This Protocol may be amended by one of the procedures specified in the following paragraphs.


(2)	Amendment after consideration by the Organization:

(a)	Any amendment proposed by a Party to the Protocol shall be submitted to the Organization and circulated by the Secretary-General to all Members of the Organization and all Parties at least six months prior to its consideration.

(b)	Any amendment proposed and circulated as above shall be submitted to the Marine Environment Protection Committee of the Organization for consideration.

(c)	Parties to the Protocol, whether or not Members of the Organization, shall be entitled to participate in the proceedings of the Marine Environment Protection Committee.

(d)	Amendments shall be adopted by a two thirds majority of only the Parties to the Protocol present and voting.

(e)	If adopted in accordance with subparagraph (d), amendments shall be communicated by the Secretary-General to all Parties to the Protocol for acceptance.

(f)	(i)	An amendment to an article or the Annex of the Protocol shall be deemed to have been accepted on the date on which two thirds of the Parties have notified the Secretary-General that they have accepted it.

(ii)	An amendment to an appendix shall be deemed to have been accepted at the end of a period to be determined by the Marine Environment Protection Committee at the time of its adoption, in accordance with subparagraph (d), which period shall not be less than ten months, unless within that period an objection is communicated to the Secretary-General by not less than one third of the Parties.

(g)	(i)	An amendment to an article or the Annex of the Protocol accepted in conformity with subparagraph (f)(i) shall enter into force six months after the date on which it is deemed to have been accepted with respect to the Parties which have notified the Secretary-General that they have accepted it.

(ii)	An amendment to an appendix accepted in conformity with subparagraph (f)(ii) shall enter into force six months after the date on which it is deemed to have been accepted with respect to all Parties with the exception of those which, before that date, have objected to it. A Party may at any time withdraw a previously communicated objection by submitting a notification to that effect to the Secretary-General.

(3)	Amendment by a Conference:

(a)	Upon the request of a Party, concurred with by at least one third of the Parties, the Secretary-General shall convene a Conference of Parties to the Protocol to consider amendments to the Protocol.

(b)	An amendment adopted by such a Conference by a two thirds majority of those Parties present and voting shall be communicated by the Secretary-General to all Parties for their acceptance.

(c)	Unless the Conference decides otherwise, the amendment shall be deemed to have been accepted and shall enter into force in accordance with the procedures specified in paragraph (2)(f) and (g).

(4)	The adoption and entry into force of an amendment constituting an addition of an Annex or an appendix shall be subject to the procedure applicable to an amendment to the Annex.

(5)	Any Party which:

(a)	has not accepted an amendment to an article or the Annex under paragraph (2)(f)(i); or

(b)	has not accepted an amendment constituting an addition of an Annex or an appendix under paragraph (4); or

(c)	has communicated an objection to an amendment to an appendix under paragraph (2)(f)(ii)

shall be treated as a non-Party only for the purpose of the application of such amendment. Such treatment shall terminate upon the submission of a notification of acceptance under paragraph (2)(f)(i) or withdrawal of the objection under paragraph (2)(g)(ii).

(6)	The Secretary-General shall inform all Parties of any amendment which enters into force under this article, together with the date on which the amendment enters into force.

(7)	Any notification of acceptance of, objection to, or withdrawal of objection to, an amendment under this article shall be communicated in writing to the Secretary-General who shall inform Parties of such notification and the date of its receipt.

(8)	An appendix to the Protocol shall contain only provisions of a technical nature.


ARTICLE 13  Signature, ratification, acceptance, approval and accession

(1)	This Protocol shall remain open for signature at the Headquarters of the Organization from 15 March 2000 until 14 March 2001 and shall thereafter remain open for accession. Any State party to the OPRC Convention may become Party to this Protocol by:

(a)	signature without reservation as to ratification, acceptance or approval; or

(b)	signature subject to ratification, acceptance or approval, followed by ratification, acceptance or approval; or

(c)	accession.

(2)	Ratification, acceptance, approval or accession shall be effected by the deposit of an instrument to that effect with the Secretary-General.

ARTICLE 14  States with more than one system of law

(1)	If a State party to the OPRC Convention comprises two or more territorial units in which different systems of law are applicable in relation to matters dealt with in this Protocol, it may at the time of signature, ratification, acceptance, approval or accession declare that this Protocol shall extend to all its territorial units or only to one or more of them to which the application of the OPRC Convention has been extended, and may modify this declaration by submitting another declaration at any time.

(2)	Any such declarations shall be notified to the dispositary in writing and shall state expressly the territorial unit or units to which the Protocol applies. In the case of modification the declaration shall state expressly the territorial unit or units to which the application of the Protocol shall be further extended and the date on which such extension takes effect.

ARTICLE 15  Entry into force

(1)	This Protocol shall enter into force twelve months after the date on which not less than fifteen States have either signed it without reservation as to ratification, acceptance or approval or have deposited the requisite instruments of ratification, acceptance, approval or accession in accordance with article 13.

(2)	For States which have deposited an instrument of ratification, acceptance, approval or accession in respect of this Protocol after the requirements for entry into force thereof have been met but prior to the date of entry into force, the ratification, acceptance, approval or accession shall take effect on the date of entry into force of this Protocol or three months after the date of deposit of the instrument, whichever is the later date.

(3)	For States which have deposited an instrument of ratification, acceptance, approval or accession after the date on which this Protocol entered into force, this Protocol shall become effective three months after the date of deposit of the instrument.

(4)	After the date on which an amendment to this Protocol is deemed to have been accepted under article 12, any instrument of ratification, acceptance, approval or accession deposited shall apply to this Protocol as amended.

ARTICLE 16  Denunciation

(1)	This Protocol may be denounced by any Party at any time after the expiry of five years from the date on which this Protocol enters into force for that Party.

(2)	Denunciation shall be effected by notification in writing to the Secretary-General.

(3)	A denunciation shall take effect twelve months after receipt of the notification of denunciation by the Secretary-General or after the expiry of any longer period which may be indicated in the notification.

(4)	A Party denouncing the OPRC Convention also automatically denounces the Protocol.

ARTICLE 17  Depositary

(1)	This Protocol shall be deposited with the Secretary-General.

(2)	The Secretary-General shall:

(a)	inform all States which have signed this Protocol or acceded thereto of:

(i)	each new signature or deposit of an instrument of ratification, acceptance, approval or accession, together with the date thereof;

(ii)	any declaration made under article 14;

(iii)	the date of entry into force of this Protocol; and

(iv)	the deposit of any instrument of denunciation of this Protocol together with the date on which it was received and the date on which the denunciation takes effect;

(b)	transmit certified true copies of this Protocol to the Governments of all States which have signed this Protocol or acceded thereto.

(3)	As soon as this Protocol enters into force, a certified true copy thereof shall be transmitted by the depositary to the Secretary-General of the United Nations for registration and publication in accordance with Article 102 of the Charter of the United Nations.

ARTICLE 18  Languages

This Protocol is established in a single original in the Arabic, Chinese, English, French, Russian and Spanish languages, each text being equally authentic.

IN WITNESS WHEREOF the undersigned, being duly authorized by their respective Governments for that purpose, have signed this Protocol.

DONE AT London this fifteenth day of March two thousand.

ANNEX REIMBURSEMENT OF COSTS OF ASSISTANCE

(1) (a)	Unless an agreement concerning the financial arrangements governing actions of Parties to deal with pollution incidents has been concluded on a bilateral or multilateral basis prior to the pollution incident, Parties shall bear the costs of their respective actions in dealing with pollution in accordance with subparagraph (i) or subparagraph (ii).

(i)	If the action was taken by one Party at the express request of another Party, the requesting Party shall reimburse to the assisting Party the costs of its action. The requesting Party may cancel its request at any time, but in that case it shall bear the costs already incurred or committed by the assisting Party.

(ii)	If the action was taken by a Party on its own initiative, this Party shall bear the costs of its action.

(b)	The principles laid down in subparagraph (a) shall apply unless the Parties concerned otherwise agree in any individual case.

(2)	Unless otherwise agreed, the costs of action taken by a Party at the request of another Party shall be fairly calculated according to the law and current practice of the assisting Party concerning the reimbursement of such costs.

(3)	The Party requesting assistance and the assisting Party shall, where appropriate, co-operate in concluding any action in response to a compensation claim. To that end, they shall give due consideration to existing legal regimes. Where the action thus concluded does not permit full compensation for expenses incurred in the assistance operation, the Party requesting assistance may ask the assisting Party to waive reimbursement of the expenses exceeding the sums compensated or to reduce the costs which have been calculated in accordance with paragraph (2). It may also request a postponement of the reimbursement of such costs. In considering such a request, assisting Parties shall give due consideration to the needs of the developing countries.

(4)	The provisions of this Protocol shall not be interpreted as in any way prejudicing the rights of Parties to recover from third parties the costs of actions to deal with pollution or the threat of pollution under other applicable provisions and rules of national and international law.

***

-----
[Source: http://www.austlii.edu.au/au/other/dfat/treaties/ATS/2007/41.html (last retrieved 2020-07-27)]
[CC BY 3.0 AU Australian Government, Department of Foreign Affairs and Trade]
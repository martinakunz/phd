CONVENTION ON EARLY NOTIFICATION OF A NUCLEAR ACCIDENT



THE STATES PARTIES TO THIS CONVENTION,

AWARE that nuclear activities are being carried out in a number of States,

NOTING that comprehensive measures have been and are being taken to ensure a high level of safety in nuclear activities, aimed at preventing nuclear accidents and minimizing the consequences of any such accident, should it occur,

DESIRING to strengthen further international co-operation in the safe development and use of nuclear energy,

CONVINCED of the need for States to provide relevant information about nuclear accidents as early as possible in order that transboundary radiological consequences can be minimized,

NOTING the usefulness of bilateral and multilateral arrangements on information exchange in this area,

HAVE AGREED as follows:

Article 1  Scope of application

1.	This Convention shall apply in the event of any accident involving facilities or activities of a State Party or of persons or legal entities under its jurisdiction or control, referred to in paragraph 2 below, from which a release of radioactive material occurs or is likely to occur and which has resulted or may result in an international transboundary release that could be of radiological safety significance for another State.

2.	The facilities and activities referred to in paragraph 1 are the following:

(a)	any nuclear reactor wherever located;

(b)	any nuclear fuel cycle facility;

(c)	any radioactive waste management facility;

(d)	the transport and storage of nuclear fuels or radioactive wastes;

(e)	the manufacture, use, storage, disposal and transport of radioisotopes for agricultural, industrial, medical and related scientific and research purposes; and

(f)	the use of radioisotopes for power generation in space objects.

Article 2  Notification and information

In the event of an accident specified in article 1 (hereinafter referred to as a "nuclear accident"), the State Party referred to in that article shall:

(a)	forthwith notify, directly or through the International Atomic Energy Agency (hereinafter referred to as the "Agency"), those States which are or may be physically affected as specified in article 1 and the Agency of the nuclear accident, its nature, the time of its occurrence and its exact location where appropriate; and

(b)	promptly provide the States referred to in sub-paragraph (a), directly or through the Agency, and the Agency with such available information relevant to minimizing the radiological consequences in those States, as specified in article 5.

Article 3  Other nuclear accidents

With a view to minimizing the radiological consequences, States Parties may notify in the event of nuclear accidents other than those specified in article 1.

Article 4  Functions of the Agency

The Agency shall:

(a)	forthwith inform States Parties, Member States, other States which are or may be physically affected as specified in article 1 and relevant international intergovernmental organizations (hereinafter referred to as "international organizations") of a notification received pursuant to sub-paragraph (a) of article 2; and

(b)	promptly provide any State Party, Member State or relevant international organization, upon request, with the information received pursuant to sub-paragraph (b) of article 2.

Article 5  Information to be provided

1.	The information to be provided pursuant to sub-paragraph (b) of article 2 shall comprise the following data as then available to the notifying State Party:

(a)	the time, exact location where appropriate, and the nature of the nuclear accident;

(b)	the facility or activity involved;

(c)	the assumed or established cause and the foreseeable development of the nuclear accident relevant to the transboundary release of the radioactive materials;

(d)	the general characteristics of the radioactive release, including, as far as is practicable and appropriate, the nature, probable physical and chemical form and the quantity, composition and effective height of the radioactive release;

(e)	information on current and forecast meteorological and hydrological conditions, necessary for forecasting the transboundary release of the radioactive materials;

(f)	the results of environmental monitoring relevant to the transboundary release of the radioactive materials;

(g)	the off-site protective measures taken or planned;

(h)	the predicted behaviour over time of the radioactive release.

2.	Such information shall be supplemented at appropriate intervals by further relevant information on the development of the emergency situation, including its foreseeable or actual termination.

3.	Information received pursuant to sub-paragraph (b) of article 2 may be used without restriction, except when such information is provided in confidence by the notifying State Party.

Article 6  Consultations

A State Party providing information pursuant to sub-paragraph (b) of article 2 shall, as far as is reasonably practicable, respond promptly to a request for further information or consultations sought by an affected State Party with a view to minimizing the radiological consequences in that State.

Article 7  Competent authorities and points of contact

1.	Each State Party shall make know to the Agency and to other States Parties, directly or through the Agency, its competent authorities and point of contact responsible for issuing and receiving the notification and information referred to in article 2. Such points of contact and a focal point within the Agency shall be available continuously.

2.	Each State Party shall promptly inform the Agency of any changes that may occur in the information referred to in paragraph 1.

3.	The Agency shall maintain an up-to-date list of such national authorities and points of contact as well as points of contact of relevant international organizations and shall provide it to States Parties and Member States and to relevant international organizations.

Article 8  Assistance to States Parties

The Agency shall, in accordance with its Statute and upon a request of a State Party which does not have nuclear activities itself and borders on a State having an active nuclear programme but not Party, conduct investigations into the feasibility and establishment of an appropriate radiation monitoring system in order to facilitate the achievement of the objectives of this Convention.

Article 9  Bilateral and multilateral arrangements

In furtherance of their mutual interests, States Parties may consider, where deemed appropriate, the conclusion of bilateral or multilateral arrangements relating to the subject matter of this Convention.

Article 10  Relationship to other international agreements

This Convention shall not affect the reciprocal rights and obligations of States Parties under existing international agreements which relate to the matters covered by this Convention, or under future international agreements concluded in accordance with the object and purpose of this Convention.

Article 11  Settlement of disputes

1.	In the event of a dispute between States Parties, or between a State Party and the Agency, concerning the interpretation or application of this Convention, the parties to the dispute shall consult with a view to the settlement of the dispute by negotiation or by any other peaceful means of settling disputes acceptable to them.

2.	If a dispute of this character between States Parties cannot be settled within one year from the request for consultation pursuant to paragraph 1, it shall, at the request of any party to such dispute, be submitted to arbitration or referred to the International Court of Justice for decision. Where a dispute is submitted to arbitration, if, within six months from the date of the request, the parties to the dispute are unable to agree on the organization of the arbitration, a party may request the President of the International Court of Justice or the Secretary-General of the United Nations to appoint one or more arbitrators. In cases of conflicting requests by the parties to the dispute, the request to the Secretary-General of the United Nations shall have priority.

3.	When signing, ratifying, accepting, approving or acceding to this Convention, a State may declare that it does not consider itself bound by either or both of the dispute settlement procedures provided for in paragraph 2. The other States Parties shall not be bound by a dispute settlement procedure provided for in paragraph 2 with respect to a State Party for which such a declaration is in force.

4.	A State Party which has made a declaration in accordance with paragraph 3 may at any time withdraw it by notification to the depositary.

Article 12  Entry into force

1.	This Convention shall be open for signature by all States and Namibia, represented by the United Nations Council for Namibia, at the Headquarters of the International Atomic Energy Agency in Vienna and at the Headquarters of the United Nations in New York, from 26 September 1986 and 6 October 1986 respectively, until its entry into force or for twelve months, whichever period is longer.

2.	A State and Namibia, represented by the United Nations Council for Namibia, may express its consent to be bound by this Convention either by signature, or by deposit of an instrument of ratification, acceptance or approval following signature made subject to ratification, acceptance or approval, or by deposit of an instrument of accession. The instruments of ratification, acceptance, approval or accession shall be deposited with the depositary.

3.	This Convention shall enter into force thirty days after consent to be bound has been expressed by three States.

4.	For each State expressing consent to be bound by this Convention after its entry into force, this Convention shall enter into force for that State thirty days after the date of expression of consent.

5.	(a)	This Convention shall be open for accession, as provided for in this article, by international organizations and regional integration organizations constituted by sovereign States, which have competence in respect of the negotiation, conclusion and application of international agreements in matters covered by this Convention.

(b)	In matters within their competence such organizations shall, on their own behalf, exercise the rights and fulfil the obligations which this Convention attributes to States Parties.

(c)	When depositing its instrument of accession, such an organization shall communicate to the depositary a declaration indicating the extent of its competence in respect of matters covered by this Convention.

(d)	Such an organization shall not hold any vote additional to those of its Member States.

Article 13  Provisional application

A State may, upon signature or at any later date before this Convention enters into force for it, declare that it will apply this Convention provisionally.

Article 14  Amendments

1.	A State Party may propose amendments to this Convention. The proposed amendment shall be submitted to the depositary who shall circulate it immediately to all other States Parties.

2.	If a majority of the States Parties request the depositary to convene a conference to consider the proposed amendments, the depositary shall invite all States Parties to attend such a conference to begin not sooner than thirty days after the invitations are issued. Any amendment adopted at the conference by a two-thirds majority of all States Parties shall be laid down in a protocol which is open to signature in Vienna and New York by all States Parties.

3.	The protocol shall enter into force thirty days after consent to be bound has been expressed by three States. For each State expressing consent to be bound by the protocol after its entry into force, the protocol shall enter into force for that State thirty days after the date of expression of consent.

Article 15  Denunciation

1.	A State Party may denounce this Convention by written notification to the depositary.

2.	Denunciation shall take effect one year following the date on which the notification is received by the depositary.

Article 16  Depositary

1.	The Director General of the Agency shall be the depositary of this Convention.

2.	The Director General of the Agency shall promptly notify States Parties and all other States of:

(a)	each signature of this Convention or any protocol of amendment;

(b)	each deposit of an instrument of ratification, acceptance, approval or accession concerning this Convention or any protocol of amendment;

(c)	any declaration or withdrawal thereof in accordance with article 11;

(d)	any declaration of provisional application of this Convention in accordance with article 13;

(e)	the entry into force of this Convention and of any amendment thereto; and

(f)	any denunciation made under article 15.

Article 17  Authentic texts and certified copies

The original of this Convention, of which the Arabic, Chinese, English, French, Russian and Spanish texts are equally authentic, shall be deposited with the Director General of the International Atomic Energy Agency who shall send certified copies to States Parties and all other States.

IN WITNESS WHEREOF the undersigned, being duly authorized, have signed this Convention, open for signature as provided for in paragraph 1 of article 12.

ADOPTED by the General Conference of the International Atomic Energy Agency meeting in special session at Vienna on the twenty-six day of September one thousand nine hundred and eighty-six.

[Signatures not reproduced here.]

-----
[Source: http://www.austlii.edu.au/au/other/dfat/treaties/ATS/1987/14.html (last retrieved 2020-07-29, last modified 2020-08-19)]
[CC BY 3.0 AU Australian Government, Department of Foreign Affairs and Trade]
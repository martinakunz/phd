import os, re, time, random
from datetime import datetime
import numpy as np
import pandas as pd
from sklearn.model_selection import KFold, ParameterGrid
from sklearn.linear_model import LinearRegression, Ridge, Lasso
from sklearn.metrics import mean_absolute_error as mae
from sklearn.metrics import r2_score
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers, regularizers

##### Data loading & wrangling
rs = 7
df = pd.read_csv('textdf_chap5.csv', encoding='utf-8').dropna(how='all') \
     .sort_values(by='treatyAdoptionDate')
smdf = pd.read_csv('../treatytextsIE-python/smdf.csv', encoding='utf-8') \
       .dropna(how='all').set_index('vars')
provdf = pd.read_csv('../treatytextsIE-python/provdf.csv', encoding='utf-8') \
         .dropna(how='all')
provdfdisp = provdf.loc[provdf.optional.isna(),
                        ['Cluster','Variable','vars','type']].set_index(['Cluster','Variable'])
df['incubWeeks'] = df.incubDays/7
df['incubYears'] = df.incubDays/365.25
df['incubConsent'] = np.nan
smvars = list(smdf.index + 'B')
# binarise categorical variables
catcols = ['treatyAdoptionPlace','orgAuspices','compDispSett']
dummies = pd.get_dummies(df[catcols], prefix=catcols)
dummies = dummies[dummies.columns[dummies.sum() >=10]]
dummies.columns = dummies.columns.str.replace('-','')
df = pd.concat([df, dummies], axis=1)
# deal with missing values in numerical variables
df.EIFdelay.fillna(0, inplace=True)
df.denunNotifMinYrs.fillna(0, inplace=True)
df.denunEffectYrs.fillna(1, inplace=True)
df['incubConsent'] = df.incubYears - df.EIFdelay
cols = (['treatyAdoptionYear','nLangs'] + smvars +
        provdfdisp[provdfdisp.type!='str'].vars.values.tolist() + list(dummies.columns))
# exclude rows with missing target and key feature
dfX = df.loc[df.incubYears.notna() & df.EIFnConsent.notna(), cols]
# exclude cols with missing values
cols = [c for c in cols if dfX[c].count() == len(dfX)]
# exclude cols with variance = 0
cols = [c for c in cols if dfX[c].nunique() > 1]
dfX = dfX[cols]
# convert to float (for neural networks)
for c in cols:
  dfX[c] = dfX[c].astype(float)
# add random features
rng = np.random.default_rng(rs)
dfX['bin_var'] = rng.integers(low=0, high=2, size=len(dfX)).astype(float)
ncols = len(dfX.columns)
X = dfX.copy()
y = df.incubConsent[df.incubConsent.notna() & df.EIFnConsent.notna()].copy()
print(dfX.shape)


##### Comparison of model performance

starttime = datetime.now()
# define columns of results dataframe
resdf = pd.DataFrame(np.nan, columns=['rs','outer_k','inner_k','testSize','testIndex','regressor',
                                      'nInputs','numInputScale','binInputScale','hpGrid','hpRes',
                                      'bestHps','params','trainMAE','testMAE', 'trainR2','testR2',
                                      'testPreds','meanPredMAE','medianPredMAE'], index=[0])
resdf.index.name = 'datetime'
# convert list cols to object dtype
resdf.testIndex = resdf.testIndex.astype('object')
resdf.testPreds = resdf.testPreds.astype('object')
# number of random trials
n_repeats = 10
outer_k = inner_k = 5
# loop for each repeat
for i in range(n_repeats):
  random.seed(i)
  np.random.seed(i)
  tf.random.set_seed(i)
  # define cross-validation method for outer and inner loops
  outer_cv = KFold(n_splits=outer_k, shuffle=True, random_state=i)
  inner_cv = KFold(n_splits=inner_k, shuffle=True, random_state=i)
  for j, (outer_train_index, outer_test_index) in enumerate(outer_cv.split(X)):
    t = str(datetime.now())[:-4]
    resdf.loc[t, ['rs']] = i
    resdf.loc[t,'outer_k'] = j
    resdf.loc[t,'inner_k'] = -1
    test_ind = y.index[outer_test_index]
    resdf.loc[t,'testSize'] = len(test_ind)
    resdf.at[t,'testIndex'] = list(test_ind)
    X_train, X_test = X.drop(index=test_ind), X.loc[test_ind,:]
    y_train, y_test = y.drop(index=test_ind), y[test_ind]
    resdf.loc[t,'meanPredMAE'] = abs(y_test - y_train.mean()).mean()
    resdf.loc[t,'medianPredMAE'] = abs(y_test - np.median(y_train)).mean()
    # rescale continuous input vars (z-score)
    resdf.loc[t,'numInputScale'] = 'z-score'
    numcols = ['treatyAdoptionYear','EIFnConsent', 'EIFdelay', 'denunNotifMinYrs','denunEffectYrs']
    numcols = [c for c in numcols if c in X.columns]
    X_train_means = X_train[numcols].mean()
    X_train_stds = X_train[numcols].std()
    for c in numcols:
      X_train[c] = (X_train[c] - X_train_means[c]) / X_train_stds[c]
      X_test[c] = (X_test[c] - X_train_means[c]) / X_train_stds[c]
    resdf.loc[t,'binInputScale'] = '0|1'
    # OLS regression
    resdf.loc[t, 'regressor'] = 'OLS'
    resdf.loc[t, 'nInputs'] = len(X.columns)
    model = LinearRegression().fit(X_train, y_train)
    resdf.loc[t,'trainR2'] = model.score(X_train, y_train)
    resdf.loc[t,'testR2'] = model.score(X_test, y_test)
    resdf.at[t,'testPreds'] = model.predict(X_test).round(4)
    resdf.loc[t,'testMAE'] = mae(y_test, model.predict(X_test))
    resdf.at[t,'params'] = [{'coefs': model.coef_, 'intercept': model.intercept_.round(5)}]
    # hyperparameter tuning loop
    for k, (inner_train_index, inner_dev_index) in enumerate(inner_cv.split(X_train)):
      t = str(datetime.now())[:-4]
      resdf.loc[t,'inner_k'] = k
      dev_ind = y_train.index[inner_dev_index]
      resdf.loc[t,'testSize'] = len(dev_ind)
      resdf.at[t,'testIndex'] = list(dev_ind)
      # get unscaled X vars and standardise with inner training data
      X_dev = X.loc[dev_ind,:]
      inner_X_train = X.loc[X_train.index.drop(dev_ind),:]
      inner_y_train, y_dev = y_train.drop(dev_ind), y_train[dev_ind]
      resdf.loc[t,'meanPredMAE'] = abs(y_dev - inner_y_train.mean()).mean()
      resdf.loc[t,'medianPredMAE'] = abs(y_dev - np.median(inner_y_train)).mean()
      inner_X_train_means = inner_X_train[numcols].mean()
      inner_X_train_stds = inner_X_train[numcols].std()
      for c in numcols:
        inner_X_train[c] = (inner_X_train[c] - inner_X_train_means[c]) / inner_X_train_stds[c]
        X_dev[c] = (X_dev[c] - inner_X_train_means[c]) / inner_X_train_stds[c]
      # Ridge (L2 regularisation)
      resdf.loc[t, 'regressor'] = 'Ridge'
      hpGrid = {'alpha': [i.round(1) for i in np.arange(0.1, 20.2, 0.2)]}
      resdf.at[t, 'hpGrid'] = [hpGrid]
      hpres = {}
      for a in hpGrid['alpha']:
        model = Ridge(alpha=a, fit_intercept=True, tol=1e-3, solver='auto').fit(inner_X_train,
                                                                                inner_y_train)
        hpres[a] = mae(y_dev, model.predict(X_dev))
      resdf.at[t, 'hpRes'] = [hpres]
      bestHp = min(hpres, key=lambda x: hpres[x])
      resdf.at[t, 'bestHps'] = bestHp
      model = Ridge(alpha=bestHp, fit_intercept=True, tol=1e-3, solver='auto').fit(inner_X_train,
                                                                                   inner_y_train)
      resdf.loc[t,'trainR2'] = model.score(inner_X_train, inner_y_train)
      resdf.loc[t,'testR2'] = model.score(X_dev, y_dev)
      resdf.at[t,'testPreds'] = model.predict(X_dev).round(4)
      resdf.loc[t,'testMAE'] = mae(y_dev, model.predict(X_dev))
      resdf.at[t,'params'] = [{'coefs': model.coef_, 'intercept': model.intercept_.round(5)}]
      # Lasso (L1 regularisation)
      t = str(datetime.now())[:-4]
      resdf.loc[t, 'regressor'] = 'Lasso'
      hpGrid = {'alpha': [0.0001, 0.001, 0.005, 0.01, 0.05] + \
                [i.round(1) for i in np.arange(0.1, 2.1, 0.1)]}
      resdf.at[t, 'hpGrid'] = [hpGrid]
      hpres = {}
      for a in hpGrid['alpha']:
        model = Lasso(alpha=a, fit_intercept=True, max_iter=3000, tol=1e-3,
                      warm_start=False, selection='cyclic').fit(inner_X_train, inner_y_train)
        hpres[a] = mae(y_dev, model.predict(X_dev))
      resdf.at[t, 'hpRes'] = [hpres]
      bestHp = min(hpres, key=lambda x: hpres[x]) 
      resdf.at[t, 'bestHps'] = bestHp
      model = Lasso(alpha=bestHp, fit_intercept=True, max_iter=3000, tol=1e-3,
                    warm_start=False, selection='cyclic').fit(inner_X_train, inner_y_train)
      resdf.loc[t,'trainR2'] = model.score(inner_X_train, inner_y_train)
      resdf.loc[t,'testR2'] = model.score(X_dev, y_dev)
      resdf.at[t,'testPreds'] = model.predict(X_dev).round(4)
      resdf.loc[t,'testMAE'] = mae(y_dev, model.predict(X_dev))
      resdf.at[t,'params'] = [{'coefs': model.coef_, 'intercept': model.intercept_.round(5),
                               'dualgap': model.dual_gap_.round(5)}]
      # MLP regression
      keras.utils.set_random_seed(i)
      t = str(datetime.now())[:-4]
      resdf.loc[t, 'regressor'] = 'MLP'
      hpGrid = {'layers': [2,3,4], 'units': [64, 96, 128],
                'init_lr': np.arange(0.0002, 0.0008, 0.0001)}
      resdf.at[t, 'hpGrid'] = [hpGrid]
      hpres = []
      for hps in ParameterGrid(hpGrid):
        model = initializer = lr_schedule = stop_early = history = None
        initializer = keras.initializers.HeUniform(seed=i)
        model = keras.Sequential()
        model.add(keras.Input(shape=(len(X_train.columns),)))
        for hl in range(hps['layers']):
          model.add(layers.Dense(units=hps['units'], activation='relu',
                                 kernel_initializer=initializer))
        model.add(layers.Dense(1))
        lr_schedule = keras.optimizers.schedules.ExponentialDecay(hps['init_lr'], 20, 0.90,
                                                                  staircase=True)
        model.compile(optimizer=keras.optimizers.Adam(learning_rate=lr_schedule),
                      loss='mean_absolute_error')
        stop_early = keras.callbacks.EarlyStopping(monitor='val_loss', patience=50)
        history = model.fit(inner_X_train, inner_y_train, batch_size=32, epochs=300,
                            verbose=0, validation_data=(X_dev, y_dev), callbacks=[stop_early])
        hist = pd.DataFrame(history.history)
        hist['weighted_avg_loss'] = (hist.loss + hist.val_loss*2) / 3
        if hist[hist.loss<=hist.val_loss].size != 0:
          subdf = hist[hist.loss<=hist.val_loss]
          hps['weighted_min_loss'] = subdf.loc[subdf.val_loss<=subdf.val_loss.min()+0.02,
                                               'weighted_avg_loss'].min()
        else:
          hps['weighted_min_loss'] = hist.loc[hist.val_loss<=hist.val_loss.min()+0.02,
                                              'weighted_avg_loss'].min()
        hps['best_epoch'] = hist.index[hist.weighted_avg_loss==hps['weighted_min_loss']][0]
        hps['trainMAE'] = hist.loss[hps['best_epoch']]
        hps['testMAE'] = hist.val_loss[hps['best_epoch']]
        for floatval in ['init_lr','weighted_min_loss','trainMAE','testMAE']:
          hps[floatval] = round(hps[floatval], 5)
        hpres.append(hps)
      hpresdf = pd.DataFrame(hpres)
      resdf.at[t, 'hpRes'] = hpres
      bestHps = hpresdf[hpresdf.trainMAE<=hpresdf.testMAE].sort_values(by='testMAE') \
                                                          .head(1).to_dict('records')[0]
      resdf.at[t, 'bestHps'] = [bestHps]
      resdf.loc[t,'trainMAE'] = bestHps['trainMAE']
      resdf.loc[t,'testMAE'] = bestHps['testMAE']

    # model evaluation with best performing hps from inner loop
    # Ridge
    hpresdf = pd.DataFrame([el[0][0] for el in resdf.loc[(resdf.regressor=='Ridge'),
                                                         ['hpRes']].tail(inner_k).values])
    bestHp = hpresdf.mean().sort_values(ascending=True).index[0]
    t = str(datetime.now())[:-4]
    resdf.loc[t,'inner_k'] = -1
    resdf.loc[t, 'regressor'] = 'Ridge'
    resdf.at[t, 'bestHps'] = bestHp
    model = Ridge(alpha=bestHp, fit_intercept=True, tol=1e-3, solver='auto').fit(X_train, y_train)
    resdf.loc[t,'trainR2'] = model.score(X_train, y_train)
    resdf.loc[t,'testR2'] = model.score(X_test, y_test)
    resdf.at[t,'testPreds'] = model.predict(X_test).round(4)
    resdf.loc[t,'testMAE'] = mae(y_test, model.predict(X_test))
    resdf.at[t,'params'] = [{'coefs': model.coef_, 'intercept': model.intercept_.round(5)}]
    # wait to prevent this row from getting overwritten due to truncated datetime
    time.sleep(0.1)
    # Lasso
    hpresdf = pd.DataFrame([el[0][0] for el in resdf.loc[(resdf.regressor=='Lasso'),
                                                         ['hpRes']].tail(inner_k).values])
    bestHp = hpresdf.mean().sort_values(ascending=True).index[0]    
    t = str(datetime.now())[:-4]
    resdf.loc[t, 'regressor'] = 'Lasso'
    resdf.at[t, 'bestHps'] = bestHp
    model = Lasso(alpha=bestHp, fit_intercept=True, max_iter=3000, tol=1e-2,
                  warm_start=False, selection='cyclic').fit(X_train, y_train)
    resdf.loc[t,'trainR2'] = model.score(X_train, y_train)
    resdf.loc[t,'testR2'] = model.score(X_test, y_test)
    resdf.at[t,'testPreds'] = model.predict(X_test).round(5)
    resdf.loc[t,'testMAE'] = mae(y_test, model.predict(X_test))
    resdf.at[t,'params'] = [{'coefs': model.coef_, 'intercept': model.intercept_.round(5),
                             'dualgap': model.dual_gap_.round(5)}]
    time.sleep(0.1)
    # MLP
    keras.utils.set_random_seed(i)
    model = initializer = lr_schedule = stop_early = history = None
    subdf = resdf.hpRes[(resdf.regressor=='MLP')].tail(inner_k).values
    hpresdf = pd.DataFrame(np.nan, columns=range(len(subdf[0])), index=range(inner_k))
    for combindex in hpresdf.columns:
      hpresdf[combindex] = [gridsearch[combindex]['testMAE'] for gridsearch in subdf]
    bestHpsInd = hpresdf.mean().sort_values(ascending=True).index[0]
    bestHps = pd.DataFrame([listel[bestHpsInd] for listel in subdf])
    meanEpoch = round(bestHps.best_epoch.mean())
    bestHps = bestHps[['layers','units','init_lr','best_epoch']].head(1).to_dict('records')[0]
    bestHps['best_epoch'] = meanEpoch
    t = str(datetime.now())[:-4]
    resdf.loc[t, 'regressor'] = 'MLP'
    resdf.at[t, 'bestHps'] = [bestHps]
    initializer = keras.initializers.HeUniform(seed=i)
    model = keras.Sequential()
    model.add(keras.Input(shape=(len(X_train.columns),)))
    for hl in range(bestHps['layers']):
      model.add(layers.Dense(units=bestHps['units'],
                             activation='relu',
                             kernel_initializer=initializer))
    model.add(layers.Dense(1))
    lr_schedule = keras.optimizers.schedules.ExponentialDecay(bestHps['init_lr'], 20, 0.90,
                                                              staircase=True)
    model.compile(optimizer=keras.optimizers.Adam(learning_rate=lr_schedule),
                  loss='mean_absolute_error')
    history = model.fit(X_train, y_train, batch_size=32, epochs=bestHps['best_epoch']+1, verbose=0)
    hist = pd.DataFrame(history.history)
    resdf.loc[t, 'trainMAE'] = round(hist.loss.values[-1], 5)
    resdf.loc[t,'testMAE'] = round(model.evaluate(X_test, y_test), 5)
    resdf.at[t,'testPreds'] = model.predict(X_test).round(5).T[0]
    resdf.loc[t,'trainR2'] = r2_score(y_train, model.predict(X_train).T[0]).round(5)
    resdf.loc[t,'testR2'] = r2_score(y_test, model.predict(X_test).T[0]).round(5) 
  print(resdf.tail().T)
# clean-up & fillnas    
resdf.drop(index=0, inplace=True)
for col in ['rs','outer_k','inner_k','nInputs','numInputScale','binInputScale']:
  resdf[col].ffill(inplace=True)
for col in ['testSize','testIndex','meanPredMAE','medianPredMAE']:
  resdf.loc[resdf.inner_k==-1, col] = resdf.loc[resdf.inner_k==-1, col].ffill()
for col in ['testSize','testIndex','meanPredMAE','medianPredMAE']:
  resdf[col].ffill(inplace=True)
for col in ['rs','outer_k','inner_k','testSize','nInputs']:
  resdf[col] = resdf[col].astype(int)
resdf.to_csv(wd+'resdf.csv', encoding='utf-8', index=True)
print(resdf.shape)
print('Time elapsed:', str(datetime.now() - starttime)[:-4])


# Map MLP testPreds to testIndex
subdf = resdf[(resdf.regressor=='MLP')&(resdf.inner_k==-1)]
treatypreds = {}
for testi in y.index:
  treatypreds[testi] = []
for row in subdf.iterrows():
  for testi in row[1].testIndex:
    idx = row[1].testIndex.index(testi)
    treatypreds[testi].append(row[1].testPreds[idx])
treatypreds = pd.DataFrame(treatypreds)
treatypredsdf = treatypreds.describe().T
treatypredsdf['std'] = treatypreds.mad()
treatypredsdf.drop(columns=['count','25%','75%'], inplace=True)
treatypredsdf.rename(columns={'50%':'Median'}, inplace=True)
treatypredsdf.columns = treatypredsdf.columns.str.title() + 'Pred'
treatypredsdf.rename(columns={'StdPred':'PredMAD'}, inplace=True)
treatypredsdf.insert(0, 'treatyLabel', df.treatyLabel)
treatypredsdf.insert(1, 'incubConsent', y)
treatypredsdf['PredRange'] = treatypredsdf.MaxPred - treatypredsdf.MinPred.abs()
treatypredsdf['MeanPredErr'] = treatypredsdf.MeanPred - y
treatypredsdf['MAE'] = (treatypreds - y).abs().mean()
treatypredsdf['ErrMAD'] = abs(((treatypreds - y).abs() - treatypredsdf.MAE)).mean()
treatypredsdf['MinAbsErr'] = (treatypreds - y).abs().min()
treatypredsdf['MaxAbsErr'] = (treatypreds - y).abs().max()
treatypredsdf['ErrRange'] = (treatypreds - y).max() - (treatypreds - y).min()
treatypredsdf['AbsErrRange'] = treatypredsdf.MaxAbsErr - treatypredsdf.MinAbsErr


##### Feature permutation

## Feature permutation for MLPs
subdf = resdf[(resdf.regressor=='MLP')&(resdf.inner_k==-1)]
cols = X.columns[~X.columns.str.contains('(?:treatyAdoption|orgAuspices)', regex=True)]
binvars = [c for c in cols if dfX[c].nunique() == 2]
numvars = [c for c in cols if dfX[c].nunique() > 2]
# remove imbalanced features
floor = len(dfX) * 0.2
ceiling = len(dfX) * 0.8
binvars = [c for c in binvars if dfX[c].sum()>floor and dfX[c].sum()<ceiling]
testmlps = pd.DataFrame(np.nan, columns=['treatyInd','treatyLabel','incubConsent',
                                         'model_dt','model_id','origPred','origRank',
                                         'permutCol','permutVal','permutPred','predDiff'],
                        index=[0]).set_index(['treatyInd','treatyLabel','incubConsent',
                                              'model_dt','model_id','origPred','origRank',
                                              'permutCol','permutVal'])
for testi in subdf.index:
  model = initializer = lr_schedule = stop_early = history = None
  rs = int(resdf.rs[testi])
  outer_k = resdf.outer_k[testi]
  keras.utils.set_random_seed(rs)
  # reproduce datasets
  test_ind = resdf.testIndex[testi]
  X_train, X_test = X.drop(index=test_ind), X.loc[test_ind,:]
  y_train, y_test = y.drop(index=test_ind), y[test_ind]
  numcols = ['treatyAdoptionYear','EIFnConsent','EIFdelay','denunNotifMinYrs','denunEffectYrs']
  X_train_means = X_train[numcols].mean()
  X_train_stds = X_train[numcols].std()
  for c in numcols:
    X_train[c] = (X_train[c] - X_train_means[c]) / X_train_stds[c]
    X_test[c] = (X_test[c] - X_train_means[c]) / X_train_stds[c]
  # reproduce model
  bestHps = resdf.bestHps[testi][0]
  initializer = keras.initializers.HeUniform(seed=rs)
  model = keras.Sequential()
  model.add(keras.Input(shape=(len(X_train.columns),)))
  for hl in range(bestHps['layers']):
    model.add(layers.Dense(units=bestHps['units'],activation='relu',kernel_initializer=initializer))
  model.add(layers.Dense(1))
  lr_schedule = keras.optimizers.schedules.ExponentialDecay(bestHps['init_lr'], 20, 0.90,
                                                            staircase=True)
  model.compile(optimizer=keras.optimizers.Adam(learning_rate=lr_schedule),
                loss='mean_absolute_error')
  history = model.fit(X_train, y_train, batch_size=32, epochs=bestHps['best_epoch']+1,
                      verbose=0, validation_data=(X_test, y_test))
  hist = pd.DataFrame(history.history)
  testpreds = model.predict(X_test, verbose=0)
  if (testpreds.round(5).T[0] != resdf.testPreds[testi]).any():
    print(f'Predictions of model {testi} not reproduced!')
  permdf = pd.DataFrame(np.nan, columns=binvars, index=X_test.index)
  X_test_perm = X_test.copy()
  for c in binvars:
    X_test_perm.loc[X_test_perm[c] == 0., c] = 1.
    permdf.loc[:,c] = model.predict(X_test_perm, verbose=0)
    X_test_perm = X_test.copy()
  for c in binvars:
    permdf.loc[X_test[c] == 1., c] = np.nan
  permdf['treatyInd'] = test_ind
  permdf['treatyLabel'] = df.treatyLabel[test_ind]
  permdf['incubConsent'] = y_test
  permdf['origPred'] = testpreds
  permdf['origRank'] = 0
  permdf = pd.melt(permdf,id_vars=['treatyInd','treatyLabel','incubConsent','origPred','origRank'],
                   var_name='permutCol',value_name='permutPred').dropna()
  permdf['model_dt'] = testi
  permdf['model_id'] = f'model_{rs}.{outer_k}'
  permdf['permutVal'] = 1
  permdf = permdf.set_index(['treatyInd','treatyLabel','incubConsent','model_dt','model_id',
                             'origPred','origRank','permutCol','permutVal'])
  testmlps = pd.concat([testmlps,permdf], axis=0, join='outer')
testmlps = testmlps.reset_index(drop=False) 
testmlps['predDiff'] = testmlps.permutPred - testmlps.origPred
testmlps['origAbsErr'] = abs(testmlps.origPred - testmlps.incubConsent)
for t in testmlps.treatyLabel.unique():
  ranking = sorted(testmlps.loc[testmlps.treatyLabel==t, 'origAbsErr'].unique())
  testmlps.loc[testmlps.treatyLabel==t, 'origRank'] = \
    testmlps.origAbsErr[testmlps.treatyLabel==t].apply(lambda x: ranking.index(x))
testmlps.dropna(how='all', inplace=True)
for col in ['treatyInd','origRank']:
  testmlps[col] = testmlps[col].astype(int)
testmlps.to_csv(wd+'testmlps.csv', encoding='utf-8', index=False)
print(testmlps.shape)  

## Feature permutation for Ridge regressors
subdf = resdf[(resdf.regressor=='Ridge')&(resdf.inner_k==-1)]
cols = X.columns[~X.columns.str.contains('(?:treatyAdoption|orgAuspices)', regex=True)]
binvars = [c for c in cols if dfX[c].nunique() == 2]
numvars = [c for c in cols if dfX[c].nunique() > 2]
# remove imbalanced features
floor = len(dfX) * 0.2
ceiling = len(dfX) * 0.8
binvars = [c for c in binvars if dfX[c].sum()>floor and dfX[c].sum()<ceiling]
ridgemodels = pd.DataFrame(np.nan, columns=['treatyInd','treatyLabel','incubConsent',
                                            'model_dt','model_id','origPred','origRank',
                                            'permutCol','permutVal','permutPred','predDiff'],
                           index=[0]).set_index(['treatyInd','treatyLabel','incubConsent',
                                                 'model_dt','model_id','origPred','origRank',
                                                 'permutCol','permutVal'])
for testi in subdf.index:
  model = None
  rs = int(resdf.rs[testi])
  outer_k = resdf.outer_k[testi]
  keras.utils.set_random_seed(rs)
  # reproduce datasets
  test_ind = resdf.testIndex[testi]
  X_train, X_test = X.drop(index=test_ind), X.loc[test_ind,:]
  y_train, y_test = y.drop(index=test_ind), y[test_ind]
  numcols = ['treatyAdoptionYear','EIFnConsent','EIFdelay','denunNotifMinYrs','denunEffectYrs']
  X_train_means = X_train[numcols].mean()
  X_train_stds = X_train[numcols].std()
  for c in numcols:
    X_train[c] = (X_train[c] - X_train_means[c]) / X_train_stds[c]
    X_test[c] = (X_test[c] - X_train_means[c]) / X_train_stds[c]
  # reproduce model
  bestHp = resdf.bestHps[testi]
  model = Ridge(alpha=bestHp, fit_intercept=True, tol=1e-3, solver='auto').fit(X_train, y_train)
  testpreds = model.predict(X_test)
  if (testpreds.round(4) != resdf.testPreds[testi]).any():
    print(f'Predictions of model {testi} not reproduced!')
  permdf = pd.DataFrame(np.nan, columns=binvars, index=X_test.index)
  X_test_perm = X_test.copy()
  for c in binvars:
    X_test_perm.loc[X_test_perm[c] == 0., c] = 1.
    permdf.loc[:,c] = model.predict(X_test_perm)
    X_test_perm = X_test.copy()
  for c in binvars:
    permdf.loc[X_test[c] == 1., c] = np.nan
  permdf['treatyInd'] = test_ind
  permdf['treatyLabel'] = df.treatyLabel[test_ind]
  permdf['incubConsent'] = y_test
  permdf['origPred'] = testpreds
  permdf['origRank'] = 0
  permdf = pd.melt(permdf, id_vars=['treatyInd','treatyLabel','incubConsent','origPred','origRank'],
                   var_name='permutCol', value_name='permutPred').dropna()
  permdf['model_dt'] = testi
  permdf['model_id'] = f'model_{rs}.{outer_k}'
  permdf['permutVal'] = 1
  permdf = permdf.set_index(['treatyInd','treatyLabel','incubConsent','model_dt','model_id',
                             'origPred','origRank','permutCol','permutVal'])
  ridgemodels = pd.concat([ridgemodels,permdf], axis=0, join='outer')
ridgemodels = ridgemodels.reset_index(drop=False) 
ridgemodels['predDiff'] = ridgemodels.permutPred - ridgemodels.origPred
ridgemodels['origAbsErr'] = abs(ridgemodels.origPred - ridgemodels.incubConsent)
for t in ridgemodels.treatyLabel.unique():
  ranking = sorted(ridgemodels.loc[ridgemodels.treatyLabel==t, 'origAbsErr'].unique())
  ridgemodels.loc[ridgemodels.treatyLabel==t, 'origRank'] = \
    ridgemodels.origAbsErr[ridgemodels.treatyLabel==t].apply(lambda x: ranking.index(x))
ridgemodels.dropna(how='all', inplace=True)
for col in ['treatyInd','origRank']:
  ridgemodels[col] = ridgemodels[col].astype(int)
ridgemodels.to_csv(wd+'RidgeTestModels.csv', encoding='utf-8', index=False)
print(ridgemodels.shape)

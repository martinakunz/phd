import glob, re, os
import numpy as np
import pandas as pd
import regex
from datetime import datetime
import warnings

# ignore warnings
warnings.filterwarnings("ignore")

starttime = datetime.now()
pd.set_option('display.width', 300)
pd.set_option('display.max_columns', 20)
pd.set_option('max_colwidth', 50) 

number = {'one':1, 'two':2, 'second':2, 'three':3, 'third':3, 'four':4, 'fourth':4, 'five':5, 'fifth':5,
          'six':6, 'sixth':6, 'seven':7, 'eight':8, 'nine':9, 'ten':10, 'tenth':10, 'eleven':11, 'twelve':12,
          'thirteen':13, 'fourteen':14, 'fifteen':15, 'fifteenth':15, 'sixteen':16, 'sixteenth':16,
          'seventeen':17, 'eighteen':18, 'nineteen':19, 'twenty':20, 'twentieth':20, '20th':20, 'twenty-first':21,
          'twenty-two':22, 'twenty-second':22, 'twenty-five':25, 'twenty-fifth':25, 'twenty-six':26,
          'twenty-eight':28, 'thirty':30, 'thirtieth':30, 'thirty-fifth':35, 'forty':40, 'fortieth':40, '40th':40,
          'fifty':50, 'fiftieth':50, 'sixty':60, 'sixtieth':60, '60th':60, '65th':65, 'ninety':90, 'ninetieth':90,
          'one hundred and eighty':180}

def word2yrs (nbstr):
  if re.match('\d', nbstr) is None:
    nb = number[re.match('(.+) (?:day|month|year)', nbstr).group(1)]
    nbstr = re.sub(r'(.+)(?= (?:day|month|year)[s]?)', str(nb), nbstr)

  if re.search('year', nbstr) is not None:
    return float(re.match('(.+) year', nbstr).group(1))
  elif re.search('month', nbstr) is not None:
    return round(float(re.match('(.+) month', nbstr).group(1))/12, 2)
  elif re.search('day[s]?$', nbstr) is not None:
    return round(float(re.match('(.+) day', nbstr).group(1))/365.25, 2)
  else: 
    return np.nan

def getgroup(match, g=1):
  if match is None:
    return np.nan
  else:
    return match.group(g)

# end of section pattern for provision IE  
eos = r'(?=\n\n(?:Article|Regulation|Chapter|Part|In Witness Whereof|Annex|Appendix|[A-Z]+ ?\d? ?[.-] [A-Z]{3,}[^.:;\n]*\n))'
  
# load texts
textdir = '../data/treatytexts/cleaned/'
textpaths = glob.glob(textdir + '*txt')
textfns = [p.split('/')[-1] for p in textpaths]
textseries = pd.Series([open(f, 'r', encoding='utf-8').read() for f in textpaths],
                       index=textfns).sort_index()
# load baseline textdf
textdf0 = pd.read_csv('textdf.csv', encoding='utf-8').dropna(how='all').set_index('fn').sort_index()
# load metadata
treatymeta = pd.read_csv('treatymeta.csv', encoding='utf-8').dropna(how='all').set_index('fn').sort_index()

# load variables (full and display version)
provdf = pd.read_csv('provdf.csv', encoding='utf-8').dropna(how='all')
provdfdisp = provdf.loc[provdf.optional.isna(),
                        ['Cluster','Variable','vars','type']].set_index(['Cluster','Variable'])
provcols = provdf.vars.tolist()
provcolsdisp = provdfdisp.vars.tolist()
provdf = provdf.set_index('vars')
smdf = pd.read_csv('smdf.csv', encoding='utf-8').dropna(how='all').set_index('vars')
smcols = []
for v in smdf.index.tolist():
  smcols.extend([v + s for s in list('NFPB')])

# select cols
idcols = ['treatyLabel','shortTitle','treatyTitle','treatyAdoptionDate','treatyAdoptionYear','treatyAdoptionPlace',
          'treatyEIFdate','treatyEIFprovInfo','incubDays','treatyDepositary','treatyRegDate',
          'treatyLangLists','nLangs','treatySubjectTerms']
textdf = treatymeta[idcols].copy()
textdf.incubDays = textdf.incubDays.astype(float)
textdf['wordcount'] = textseries.str.split().str.len()

# add info on supplementary agreements (for inherited provs)
textdf['supplAgTo'] = np.nan
textdf.loc[textdf.treatyLabel.isin(['OzoneLayerProt']), 'supplAgTo'] = 'OzoneLayerConv'
textdf.loc[textdf.treatyLabel.isin(['KyotoProt','ParisAgreement']), 'supplAgTo'] = 'UNFCCC'
textdf.loc[textdf.treatyLabel.isin(['CBD-BiosafetyProt', 'CBD-ABSprot']), 'supplAgTo'] = 'CBD'
textdf.loc[textdf.treatyLabel.isin(['CBD-BiosafetyP-LiabSP']), 'supplAgTo'] = 'CBD-BiosafetyProt'
textdf.loc[textdf.treatyLabel.isin(['HazWasteLiabProt']), 'supplAgTo'] = 'HazardousWasteConv'
textdf.loc[textdf.treatyLabel.isin(['IntlHealthRegs05']), 'supplAgTo'] = 'WHOconst'

textdf = textdf.reindex(columns=idcols + ['supplAgTo', 'wordcount'] + provcols + smcols)
print(len(textdf.columns), 'columns')
print(len(textseries), 'treaties')
del textpaths, textfns


##### Treaty provisions ######

### Simple binary variables 
for target in ['provisAppProv','amendProv','annexProtProv','denunProv','termProv','legalPers','secretariat',
               'natRepProv','verifProv','reviewProv','ncpProv','sanctionsProv','disputeProv']:
  for pat in ['namedArt','unnamedArt']:
    if pd.notna(provdf.loc[target, pat]):
      pattern = regex.compile(provdf.loc[target, pat])
      colname = re.search('\?P<(\w+)>', provdf.loc[target, pat]).group(1)
      new = textseries.apply(lambda s: getgroup(regex.search(pattern, s))).dropna().to_frame(name=colname)
      new[target] = True
      textdf.update(new, overwrite=False)
      del pattern, colname, new
  # residual matching
  if pd.notna(provdf.loc[target,'otherExtract']):
    new = textseries[textdf[target].isna()].str.extract(provdf.loc[target,'otherExtract']).dropna()
    new[target] = True
    textdf.update(new, overwrite=False)

### Entry into force conditions
# P1 [23 T]
pattern = (r"(?i)\n\n(?P<EIFprovStr>Article[^.]*.?\s?" + 
  r"Th\w+(?: present)? (?!amendment)(?:\w+ )?\w+ shall \w+ into force " +
  r"(?:on the )?(?P<EIFdelay>[^.]+? (?:day|month|year)[s]?) (?:after|following) " +
  r"(?:the \w+ \w+ which (?:the )?(?:Governments of )?)?(?:not less than|at least|the)? ?" +
  r"(?P<EIFnConsent>[^.]{0,20}?) (?:States|Governments|Parties)[^.]*? have " +
  r"(?:become parties|deposited \w* ?instrument|[^.]*?(?:ratif|consent to be bound))[^.]+[.])")
new = textseries.str.extract(pattern).dropna()
new.EIFnConsent = new.EIFnConsent.transform(lambda x: number[x] if pd.notna(x) and re.search('[A-z]', x) is not None else x).astype(float)
new.EIFdelay = new.EIFdelay.transform(lambda x: word2yrs(x) if pd.notna(x) else x)
new.loc[new.EIFprovStr.str.contains('(?:constitut\w+ not less than|account\w+ in total for|provided that|nuclear capacity|gross ton\w+)'), 'EIFaddlCond'] = True 
textdf.update(new, overwrite=True)

# P2 [41 T]
pattern = r"(?P<EIFprovStr>(?:Subject to the prov\w+ of Art\w+ \d+.\d, t|T)h\w+(?: present)? (?!(?:amendment|governing))(?:\w+ )?\w+ (?:shall \w+|enters) into force (?:on the same day as the \w+ or )?(?:on the )?(?P<EIFdelay>[^.]+? (?:day|month|year)[s]?) (?:after|following|from) (?:the (?:date|day) of )?(?:the )?deposit[^.]{0,50}? of (?!its )(?:the )?(?P<EIFnConsent>[^.]{0,20}?) ?(?<!an )instrument[s]? of ratification[^.]*[.])"
new = textseries.str.extract(pattern).dropna()
new.loc[new.EIFnConsent=='', 'EIFnConsent'] = np.nan
new.EIFnConsent = new.EIFnConsent.str.replace(' ','-').transform(lambda x: number[x] if x is not np.nan and re.search('[A-z]', x) is not None else x)
# deal with special cases
new.EIFdelay = new.EIFdelay.str.replace('.+(?:third month|month after the 60th day)', '2.5 months', regex=True)
new.EIFdelay = new.EIFdelay.str.replace('.+month following the exp\w+ of one year', '12.5 months', regex=True)
# transform all time units to years
new.EIFdelay = new.EIFdelay.transform(lambda x: word2yrs(x) if x is not np.nan else x)
new.loc[new.EIFprovStr.str.contains('(?:in no case earlier than|provided that|each having|first day of the)'), 'EIFaddlCond'] = True 
textdf.update(new, overwrite=True)

# P3 [1 T]
pattern = r"(?P<EIFprovStr>Th\w+(?: present)? (?!(?:amendment|governing))(?:\w+ )?\w+ shall \w+ into force (?:on the )?(?P<EIFdelay>[^.]+? (?:day|month|year)[s]?) (?:after|following|from) (?:the (?:date|day) of )?(?:the )?deposit of an instrument of ratification by at least (?P<EIFnConsent>\w+ \w+ State[s]? and \w+ \w+) State[^.]*[.])"
new = textseries.str.extract(pattern).dropna()
new.EIFnConsent = new.EIFnConsent.str.replace('one \w+ State and one \w+', 'two', regex=True)
new.EIFnConsent = new.EIFnConsent.transform(lambda x: number[x] if x is not np.nan and re.search('[A-z]', x) is not None else x)
new.EIFdelay = new.EIFdelay.transform(lambda x: word2yrs(x) if x is not np.nan else x)
new.loc[new.EIFprovStr.str.contains('licencing State'), 'EIFaddlCond'] = True 
textdf.update(new, overwrite=True)

# P4 (containing dot) [13 T]
pattern = r"(?i)\n(?P<EIFprovStr>(?:Article .+)\n+\d[.]\s?This (?!amendment)(?:\w+ )?\w+ shall be binding only upon[^.]+[.]\n+\d.\s?It shall \w+ into force (?P<EIFdelay>\w+ (?:day|month)[s]?) (?:after|following) the date on which the \w+ of (?P<EIFnConsent>\w+) Members[^.]+[.])"
new = textseries.str.extract(pattern).dropna()
new.EIFnConsent = new.EIFnConsent.transform(lambda x: number[x] if x is not np.nan and re.search('[A-z]', x) is not None else x).astype(float)
new.EIFdelay = new.EIFdelay.transform(lambda x: word2yrs(x) if x is not np.nan else x)
textdf.update(new, overwrite=True)

# P5 (containing dot) [4 T]
pattern = r"(?is)(?P<EIFprovStr>This (?!amendment)(?:\w+ )?\w+ shall enter into force (?:on the )?(?P<EIFdelay>\w+ (?:day|month)[s]?) (?:after|following) the date on which(?: the following|:).{,40}\s(?:not less than|at least) (?P<EIFnConsent>\w+) States[^.]+?(?: and\n+\d[.][^.]+|\n+[.]\d[^\n]+)*[.])"
new = textseries.str.extract(pattern).dropna()
new.EIFnConsent = new.EIFnConsent.transform(lambda x: number[x] if x is not np.nan and re.search('[A-z]', x) is not None else x).astype(float)
new.EIFdelay = new.EIFdelay.transform(lambda x: word2yrs(x) if x is not np.nan else x)
new.loc[new.EIFprovStr.str.contains('(?:tonnage| tons |included in the number)', regex=True), 'EIFaddlCond'] = True 
textdf.update(new, overwrite=True)

# P6 '...has been deposited/expressed.' [7 T]
pattern = r"(?i)(?P<EIFprovStr>(?:Article .+\n+\d*.\s)?This \w+ shall enter into force(?:, with respect to [^.]{0,100}?)?(?: on the)? (?P<EIFdelay>\w+ (?:day|month)[s]?(?: of the \w+ month)?) after (?:consent to be bound has been expressed by|(?:the \w+ .n which )?the)? ?(?P<EIFnConsent>\w+) (?:States|instrum\w+ of ratif[^.]+)[.])"
new = textseries.str.extract(pattern).dropna()
new.EIFnConsent = new.EIFnConsent.transform(lambda x: number[x] if x is not np.nan and re.search('[A-z]', x) is not None else x).astype(float)
new.EIFdelay = new.EIFdelay.str.replace('first day of the sixth month', '5.5 months', regex=False)
new.EIFdelay = new.EIFdelay.transform(lambda x: word2yrs(x) if x is not np.nan else x)
textdf.update(new, overwrite=True)

# P7 (no EIF delay) [9 T]
pattern = r"(?P<EIFprovStr>(?:Article .+\n+)?\d*.?\s?Th\w+(?: present)? (?!amendment)(?:\w+ )?\w+ shall \w+ into force (?:immed\w+ )?(?:on|at|as from|upon|after) (?:the )?(?:date (?:of|on which))? ?(?:the )?(?:deposit(?: of inst\w+ of)?|receipt by|ratif|sig\w+ or acc)[^.]*? ?(?:of|by|from)(?: at least)? ?(?:the )?(?P<EIFnConsent>[^.]{3,60}?) (?:instrument|Member|Governments|States(?! in category II and))[^.]*[.])"
new = textseries.str.extract(pattern).dropna()
new.EIFnConsent = new.EIFnConsent.str.replace('6 .+ 6 .+ and 24', '36', regex=True)
new.EIFnConsent = new.EIFnConsent.transform(lambda x: number[x] if x is not np.nan and re.search('[A-z]', x) is not None else x).astype(float)
new['EIFdelay'] = 0.0
new.loc[new.EIFprovStr.str.contains('(?:including the Governments|provided that)', regex=True), 'EIFaddlCond'] = True 
textdf.update(new, overwrite=True)

# P8 (no EIF delay, target EIF date) [5 T]
pattern = r"(?P<EIFprovStr>(?:Article .+\n+)?\d*.?\s?This \w+ shall \w+ into force(?: defin\w+)? on [^.]+? (?:provided that|if) (?:by (?:that date|\d+ \w+ \d{4}))?(?:at least|\w+ \w+ include th\w+ of)? ?(?P<EIFnConsent>(?:\d* ?Governments[^.]+ and(?: at least)? \d+ \w* ?(?:Governments|Members))|\w{3,})[^.]*? (?:instruments of ratif|Signatories have depos|have ratified|countries)[^.]+?[.])"
new = textseries.str.extract(pattern).dropna()
new.EIFnConsent = new.EIFnConsent.str.replace('12 Gov.+ and 10 Gov\w+', '22', regex=True)
new.EIFnConsent = new.EIFnConsent.str.replace('.+20 exporting Mem.+ and at least 10 importing Mem.+', '30', regex=True)
new.EIFnConsent = new.EIFnConsent.transform(lambda x: number[x] if x is not np.nan and re.search('[A-z]', x) is not None else x).astype(float)
new.loc[new.EIFprovStr.str.contains('(?:(?:representing|holding|accounting for) at least|if by \d+ \w+ \d{4})', regex=True), 'EIFaddlCond'] = True 
textdf.update(new, overwrite=True)

# P9 (no EIF delay, target EIF date) [5 T]
pattern = r"(?P<EIFprovStr>(?:Article .+\n+)?\d*.?\s?(?:This \w+ shall \w+ into force(?: defin\w+)? on \d+ \w+ \d{4}[^.]* if,? [^.]+(?: deposited on behalf of Gov\w+ holding|and Gov\w+ repr\w+ \w+ countries having at least|on behalf of Gov\w+ listed in)|[^.]+ shall enter into force (?:on the date det\w+ by Ministers in acc\w+ with|\d+ months after the date of notif\w+ ref\w+ to))[^.]+[.])"
new = textseries.str.extract(pattern).dropna()
new.loc[new.EIFprovStr.str.contains('(?:representing at least|holding|det\w+ by Ministers)', regex=True), 'EIFaddlCond'] = True 
textdf.update(new, overwrite=True)

# P10 (no EIF delay) [6 T]
pattern = r"(?i)(?P<EIFprovStr>(?:Article .+\n+)?\d*.?\s?(?:Upon the receipt by the \w+ \w+ of|As soon as this \w+ has been ratif\w+ by|Th\w+ \w* ?\w+(?:, apart from the Annex,)? shall(?:, when \w+ of ratif\w+ have been depo\w+ by| \w+ into force(?: on the date)? when)(?: at least)?) (?P<EIFnConsent>\w+) (?:notif\w+ of acceptance[^.]+ come into force immediat|signatory [^.]+ into force|States[^.]+(?:ratif|become parties))[^.]*.)"
new = textseries.str.extract(pattern).dropna()
new.EIFnConsent = new.EIFnConsent.transform(lambda x: number[x] if x is not np.nan and re.search('[A-z]', x) is not None else x).astype(float)
new['EIFdelay'] = 0.0
new.loc[new.EIFprovStr.str.contains('(?:nations specified in Annex|which shall include the Gov|including the Host State|gross tons|provided that)', regex=True), 'EIFaddlCond'] = True 
textdf.update(new, overwrite=True)

# P11 (no EIF delay) [4 T]
pattern = r"(?im)^(?P<EIFprovStr>(?:Article .+\n+)?\d*.?\s?Th\w+ \w* ?\w+ shall \w+ into force(?: among the states which have depos\w+ inst\w+ of ratif\w+)? (?:on the deposit of the|when(?: it has been accepted by)?|after its ratif\w+ by the) (?P<EIFnConsent>\w+-?\w*(?:, the Gov\w+[^.]+designated Deposit[^.]+ and \w+ other)?) (?:such instrument|of its signatories|States signatory to|Members of the U\w+ N\w+ have become parties)[^.]*.)"
new = textseries.str.extract(pattern).dropna()
new.EIFnConsent = new.EIFnConsent.str.replace('.+Depositaries.+ and forty other', '43', regex=True)
new.EIFnConsent = new.EIFnConsent.transform(lambda x: number[x] if x is not np.nan and re.search('[A-z]', x) is not None else x).astype(float)
new['EIFdelay'] = 0.0
new.loc[new.EIFprovStr.str.contains('designated Depositaries'), 'EIFaddlCond'] = True 
textdf.update(new, overwrite=True)

# P12 (simultaneous EIF for all) [1 T]
pattern = r"(?i)(?P<EIFprovStr>This (?!amendment)(?:\w+ )?\w+ shall \w+ into force (?:on|after) (?:the|its) (?:date (?:of|upon which))? ?(?:instruments|ratif)[^.]*? ?(?:of|by) all the (?:Original Parties|Contracting Governments)[^.]+[.])"
new = textseries.str.extract(pattern).dropna()
new['EIFdelay'] = 0.0
new.loc[new.EIFprovStr.str.contains('(?:all the Original Parties|Whaling Convention)', regex=True), 'EIFaddlCond'] = True 
textdf.update(new, overwrite=True, errors='ignore')

# update binary variable
textdf.loc[textdf.EIFprovStr.notna(), 'EIFprov'] = True


### Simplified amendment

# P1 tacit acceptance of amendments
pattern = (r'(?im)^(?P<simplAmendProvStr>.*(?:amend|annex|reg).* shall \w+ (?:into force|effect\w*) ' +
  r'(?:as from the|(?:for|with respect to) (?:all|each[^.]+ for all other)|\w*on \w* ?\w* ?approval).* ' +
  r'(?:not submitted a notification|notif.+ non-acceptance|objection|rejection|declaration' +
  r'|months after the\w*(?: date o.)? (?:adoption|.+deemed to have been acc)' +
  r'|two.thirds \w* ?of(?: all)? the \w* ?(?:Members|Parties.+?\n*.+?new obligations)' +
  r'|Parties casting a positive vote' +
  r'|fundamental alterations in the aims).+(?:\n+.+(?:object|reject|notif).+)*)')
new = textseries.str.extract(pattern).dropna()
textdf.update(new, overwrite=True)

# P2 tacit acceptance (>=1 para)
pattern = (r'(?im)^(?P<simplAmendProvStr>.+amendment.* shall \w+ (?:into force|effect\w*) (?:for|with respect to) all [^.]*' +
  r'(?:not submitted a notification|notif.+ non-acceptance|objection|rejection|reservation|declaration' +
  r'|months after \w*(?: date o.)? ?(?:adoption|notif\w+ of approval)' +
  r'|two-thirds \w* ?of(?: all)? the Members' +
  r'|Parties casting a positive vote' +
  r'|upon the deposit of instruments' +
  r'|on their approval by a majority)[^.]*[.])')
new = textseries.str.extract(pattern).dropna()
textdf.update(new, overwrite=True)

# P3 no ratification of amendments
pattern = (r'(?im)^(?P<simplAmendProvStr>.+(?:amendment|revision).* shall \w+ (?:into force|effect\w*)(?! for those Parties)' +
  r'[^.]* (?:after|from) (?:the date of )?\w* ?adoption[^.]*[.].*)')
new = textseries.str.extract(pattern).dropna()
textdf.update(new, overwrite=True)

# P4 simplified/postal procedure
pattern = (r'(?im)^(?P<simplAmendProvStr>.*(?:amend|revis|adjust).+(?:of (?:certain prov|annex \w\n|limits\n+Art.+\n+)' +
  r'|(?:simplified|postal) procedure' +
  r'|change in \w+ share' +
  r'|(?<=revise )any annexes' +
  r'|revise it by special vote).*' +
  r'(?:\n+.+(?:amend|revis|proposal|annex|adjust).+)*)')
new = textseries.str.extract(pattern).dropna()
textdf.update(new, overwrite=True)

# P5 tacit amendment
pattern = (r'(?im)^(?P<simplAmendProvStr>.*(?:shall be deemed to be listed' +
  r'|entitled to apply the \w+ as amended' +
  r'|may declare to the \w+ before that date that it does not accept).*' +
  r'(?:\n+.+(?:amend|revis|annex|adjust|modif|updat|change).+)*)')
new = textseries.str.extract(pattern).dropna()
textdf.update(new, overwrite=False)

# P6 additions based on amendProvStr
pattern = r"(?i)(?P<simplAmendProvStr>.*(?:(?:simplified|postal)|(?:deemed|considered)(?: to)? ?(?:be|have been)? (?:accepted|approved)|other amendments shall (?:\w+ into force|\w+ effect\w*) on adoption by).+)"
new = textdf[textdf.simplAmendProvStr.isna()].amendProvStr.str.extract(pattern).dropna()
textdf.update(new, overwrite=True)

# Update binary variable
textdf.loc[textdf.simplAmendProvStr.notna(), 'simplAmendProv'] = True

# Update supplementary agreements
# based on protocol text
new = textdf.loc[textdf.amendProvStr.notna() & textdf.amendProvStr.str.contains('shall apply mutatis mutandis'), ['treatyLabel']]
new['supplAgTo'] = textdf.supplAgTo[new.index]
new['simplAmendProv'] = new.supplAgTo.apply(lambda x: textdf.simplAmendProv[textdf.treatyLabel == x].values[0])
textdf.update(new, overwrite=False)
# based on convention text
new = textdf.loc[textdf.simplAmendProvStr.notna() & textdf.simplAmendProvStr.str.contains('(?i)for all parties to this \w+ or to any protocol concerned'), ['treatyLabel']]
textdf.loc[textdf.supplAgTo.isin(new.treatyLabel), ['simplAmendProv']] = True


### Withdrawal conditions
## denunNotifMinYrs
# Anytime (waiting 0 years)
new = textdf.denunProvStr.str.extract(provdf.loc["denunNotifAnytimeProvStr","otherExtract"]).dropna()
new['denunNotifMinYrs'] = 0.
textdf.update(new, overwrite=True)

# Min. years specified
# P1 [18 T]
pattern = r'(?im)^(.*(?:at any time )?after (?:the expir\w+ of [a]?)? ?(?P<denunNotifMinYrsw>[^.]+?) from the date [^.]+? (?:into force|take[s]? effect)[^.]+? (?:may (?:(?:be )?denounce[d]?|withdraw from)|notify [^.]+? of its withdrawal)[^.]*[.])'
new = textdf.denunProvStr.str.extract(pattern).dropna()
# transform number words into numbers
new['denunNotifMinYrs'] = new.denunNotifMinYrsw.str.replace(' year[s]?', '', regex=True).transform(lambda x: number[x] if x is not np.nan else x)
textdf.update(new, overwrite=True)

# P2 [32 T]
pattern = r'(?im)^((?:Article \w+\s*\W? ?(?:\w+ ?){0,5}\n+)?[^\n]*(?:may (?:(?:be )?denounce[d]?|withdraw)|(?:notif[^.]+?|give notice of ?\w*) withdrawal)[^.]*? after (?:(?:the expir\w+|a period) of )?(?P<denunNotifMinYrsw>[^.]+?) (?:f\w+ the date (?:\w*on which [ti][^.]+? into force|of its acceptance)|of membership)[^.]*[.])'
new = textdf.denunProvStr.str.extract(pattern).dropna()
# transform number words into numbers
new['denunNotifMinYrs'] = new.denunNotifMinYrsw.transform(lambda x: word2yrs(x) if x is not np.nan else x)
textdf.update(new, overwrite=True)

# P3 [5 T]
pattern = r'(?im)^((?:Article \w+\s*\W? ?(?:\w+ ?){0,5}\n+)?[^\n]*may give notice of ?\w* withdrawal from the [^.]*? (?P<denunNotifMinYrsw>\w+ year\w?) after its entry into force[^.]*[.])'
new = textdf.denunProvStr.str.extract(pattern).dropna()
# transform number words into numbers
new['denunNotifMinYrs'] = new.denunNotifMinYrsw.str.replace(' year[s]?', '',regex=True).transform(lambda x: number[x] if x is not np.nan else x)
textdf.update(new, overwrite=True)

# P4 [2 T]
pattern = r'(?im)^((?:Article \w+\s*\W? ?(?:\w+ ?){0,5}\n+)?[^\n]*(?:upon expir\w+ of a period of)?\s(?P<denunNotifMinYrsw>\w+ years) after \w* ?\w+ (?:into force|takes effect)[^.]+? (?:may (?:(?:be )?denounce[d]?|withdraw[n]? from))[^.]*[.])'
new = textdf.denunProvStr.str.extract(pattern).dropna()
# transform number words into numbers
new['denunNotifMinYrs'] = new.denunNotifMinYrsw.str.replace(' year[s]?', '',regex=True).str.lower().transform(lambda x: number[x] if x is not np.nan else x)
textdf.update(new, overwrite=True)

## denunCondOther
# P1 [11 T]
pattern = (r'(?im)(?:^|[.])\s*([^.]+?(?:withdraw from|denounce) th[^.]+(?:supreme interests' +
  r'|.+\n*.+ shall (?:not take effect|continue to be bound)[^.]* end of the armed conflict)[^.]*[.])')
textdf.loc[textseries.str.contains(pattern, regex=True), 'denunCondOther'] = True

# P2 [17 T]
pattern = (r'(?i)((?:Except as provided in (?:para|art|section)[^.]* may withdraw from th\w+ \w+ by' +
  r'|withdrawal shall apply, except with re\w+ to parties referred to in' +
  r'|any state not a member of \w+ which has become a party to \w+ \w+ may at any time withdraw' +
  r'|subject to [^.]+ paid its annual contribution for each year' +
  r'|will be bound for another period of \w+ years)[^.]*[.])')
textdf.loc[textdf.denunProvStr.notna() & textdf.denunProvStr.str.contains(pattern), 'denunCondOther'] = True

# P3 [5 T]
pattern = r'(?i)((?:Any \w+ \w* ?may (?:denounce|withdraw from) \w+ \w+ (?:at the end of any (?:fiscal )?year|on \w+ \w+ of any year|[^.]+(?:after|following) the first revision conf|within the year following the expiration of the period.+denunciation)).*)'
textdf.loc[textseries.str.contains(pattern, regex=True), 'denunCondOther'] = True

## denunEffectYrs (postNotif)
# P1 [81 T]
pattern = r'(?i)(\w+ (?:shall [^.]*?|\w+) (?:effect\w*|place) (?:[^.]*expir\w+ of|until|on the)? ?(?P<denunEffectYrsw>(?:\w+ ){0,4}?(?:day|month|year)[s]?) (?:after|from|following) (?:the date on which \w+ (?:instrument )?)?(?:\w+ ){0,8}(?:recei\w+|deposit|regist|communication|noti)[^.]*[.])'
new = textdf.denunProvStr.str.extract(pattern).dropna()
# transform number words into numbers
new['denunEffectYrs'] = new.denunEffectYrsw.transform(lambda x: word2yrs(x) if x is not np.nan else x)
textdf.update(new, overwrite=True)

# P2 [23 T]
pattern = r"(?im)(?:^|[.])([^.]*(?:withdraw|den[o]?unc)[^.]* (?P<denunEffectYrsw>(?:\w+ )+?(?:day|month|year)[s]?)[',]?[s]? (?:in advance|prior|(?:written )?notice|or such longer period [^.]+ (?:recei|deposit))[^.]*[.])"
new = textdf.denunProvStr.str.extract(pattern).dropna()
# transform number words to numbers
new['denunEffectYrs'] = new.denunEffectYrsw.transform(lambda x: word2yrs(x) if x is not np.nan else x)
textdf.update(new, overwrite=True)

# P3 [10 T]
pattern = r"(?im)^(.*(?:shall take effect|in no event less than|at least) (?P<denunEffectYrsw>(?:\w+ )(?:day|month|year)[s]?)'? (?:notice[^.]+terminate the application of this \w+ to itself|after[^.]*(?:deposit|received|communic))[^.]*[.])"
new = textdf.denunProvStr.str.extract(pattern).dropna()
# transform number words into numbers
new['denunEffectYrs'] = new.denunEffectYrsw.transform(lambda x: word2yrs(x) if x is not np.nan else x)
textdf.update(new, overwrite=True)

## denunEffectOther
pattern = (r'(?i)(?:(?:withdrawal|denunc\w+)(?: under this art\w+)? (?:shall [^.]*?|\w+) (?:effect\w*|place) (?:at the end of the (?:calendar )?year|on the first day of the month|\w*on 1 January)' +
  r'|notice of (?:withdrawal|denunciation), so that the \w+ shall cease to be in force on)[^.]*.')
textdf.loc[textseries.str.contains(pattern, regex=True), 'denunEffectOther'] = True

## special case of incorporation
pattern = r"(?i)\n(Article.+Withdrawal\n+.+the provisions of Art\w+ \w+ of the Convention re\w+ to withdrawal shall apply, except.+)"
new = textseries.str.extract(pattern).dropna()
new['supplAgTo'] = textdf.supplAgTo[new.index]
new['denunNotifMinYrs'] = new.supplAgTo.apply(lambda x: textdf.denunNotifMinYrs[textdf.treatyLabel == x].values[0])
textdf.update(new, overwrite=False)


### Automatic termination
## Automatic termination based on a membership threshold
new = textseries.str.extract(provdf.otherExtract['autoTermMemThreshProvStr']).dropna()
# transform number words to numbers
new['autoTermMemThresh'] = new.autoTermMemThreshw.transform(lambda x: number[x] if x is not np.nan and re.match('[A-z]', x) is not None else x)
textdf.update(new, overwrite=True)

## Automatic termination after a certain period
pattern = r"(?i)(?P<autoExpiryProvStr>This \w+ shall remain in force (?:for a period of|until the end of the) (?P<autoExpiryw>\w+ (?:\w* ?\w+ )?year.?) (?:\w+ ){,5}(?:entry into force|unless extended)[^.]+[.])"
new = textseries.str.extract(pattern).dropna()
# transform number words to numbers
new.autoExpiryw = new.autoExpiryw.str.replace('full cocoa ', '')
new['autoExpiryYrs'] = new.autoExpiryw.transform(lambda x: word2yrs(x) if x is not np.nan else x)
new['autoExpiry'] = True
textdf.update(new, overwrite=True)

## Automatic termination by a certain date
pattern = r"(?i)(?P<autoExpiryProvStr>This \w+ shall remain in force until \w+ \w+ \d+.? unless (?:it is )?(?:extended|prolonged) [^.]*[.])"
new = textseries.str.extract(pattern).dropna()
new['autoExpiry'] = True
textdf.update(new, overwrite=True)


### Organisational auspices

# ILO
OrgAusp = r"(?i)\n(?P<orgAuspStr>The General Conference of the International Lab\w+ Org\w+,)\n"
new = textseries.str.extract(OrgAusp).dropna()
new['orgAuspices'] = 'ILO'
textdf.update(new, overwrite=True)
for col in ['secretariat','natRepProv', 'verifProv', 'reviewProv', 'ncpProv', 'sanctionsProv', 'disputeProv']:
  textdf.loc[textdf.orgAuspices=='ILO', col] = True

# IMO
OrgAusp = r'(?i)\s(?P<orgAuspStr>(?:\S?Org\w+.?(?: means the (?:Inter\S+ Maritime \w* ?Org.+|Org\w+ designated by the \w+ Parties in acc\w+ with Art\w+ XIV.+)|, have the same meaning as in Art\w+ I of the Liab\w+ Conv\w+)|(?<!establish )the Inter\S+ Maritime \w+ Org\w+ .hereinafter[^)]+\)))'
new = textseries.str.extract(OrgAusp).dropna()
new['orgAuspices'] = 'IMO'
new['secretariat'] = True
textdf.update(new, overwrite=True)

# IAEA
IAEA = 'International Atomic Energy Agency'
OrgAusp = f"(?i)\n(?P<orgAuspStr>.+(?:shall be deposited with the D\w+ G\w+ of the {IAEA}|{IAEA}[^.]+ shall provide the secretariat|conference.+shall be convened by.+ the {IAEA}).+)"
new = textseries.str.extract(OrgAusp).dropna()
new['orgAuspices'] = 'IAEA'
new['secretariat'] = True
textdf.update(new, overwrite=True)

# FAO
OrgAusp = r"(?i)\n(?P<orgAuspStr>.+(?:to conclude a[^\n]+ under Article \w+ of the FAO Constitution|to establish .+ within the framework of .+FAO|open for signature at FAO ).*)"
new = textseries.str.extract(OrgAusp).dropna()
new['orgAuspices'] = 'FAO'
new['secretariat'] = True
new.loc[new.orgAuspStr.str.contains('FAO Constitution'), 'natRepProv'] = True
textdf.update(new, overwrite=True)

# UNESCO
OrgAusp = r"(?i)\n\n(?P<orgAuspStr>The General Conference of the United Nations Educational,.+)\n\n"
new = textseries.str.extract(OrgAusp).dropna()
new['orgAuspices'] = 'UNESCO'
new['secretariat'] = True
textdf.update(new, overwrite=True)

# WHO
OrgAusp = r"(?i)\n(?P<orgAuspStr>.+(?:secretariat.+provided by the World Health Org|.WHO. means the World Health Org).+)"
new = textseries.str.extract(OrgAusp).dropna()
new['orgAuspices'] = "WHO"
new['secretariat'] = True
textdf.update(new, overwrite=True)

# WIPO
OrgAusp = r"(?i)\n(?P<orgAuspStr>.+.Organization. means the World Intellectual Property Org.+)"
new = textseries.str.extract(OrgAusp).dropna()
new['orgAuspices'] = "WIPO"
textdf.update(new, overwrite=True)

# UNEP
OrgAusp = r"(?i)\n(?P<orgAuspStr>.+secretariat [^.]+ by (?:the )?(?:Executive Director of the )?(?:United Nations Environment Programme|UNEP).+)"
new = textseries.str.extract(OrgAusp).dropna()
new['orgAuspices'] = "UNEP"
new['secretariat'] = True
textdf.update(new, overwrite=True)

# Joint auspices
OrgAusp = r"(?i)\n(?P<orgAuspStr>.+secretariat [^.]+ jointly by (?:the )?(?:Executive Director of \w* ?)?(?:United Nations Environment Programme|UNEP) and .+ FAO.+)"
new = textseries.str.extract(OrgAusp).dropna()
new['orgAuspices'] = "UNEP, FAO"
new['secretariat'] = True
textdf.update(new, overwrite=True)

# UN 
# Treaty title starts with 'United Nations' 
OrgAusp = r"(?i)^(?P<orgAuspStr>United Nations \w+ \w+)"
new = textseries.str.extract(OrgAusp).dropna()
new['orgAuspices'] = 'UN'
textdf.update(new, overwrite=True)
# Text-based matching
UN = r'(?:United Nations|UN)'
UNGA = f'\\b(?:{UN} General Assembly|General Assembly of the {UN}|UNGA)\\b'
UNSG = f'\\b(?:{UN} Secretar\w+(?:.General)?|Secretar\w+(?:.General)? of the {UN}|UNSG)\\b'
UNSC = f'\\b(?:{UN} Security Council|Security Council of the {UN}|UNSC)\\b'
OrgAusp = f"(?is)\n(?P<orgAuspStr>[^\n]*(?:shall be included in the \w* ?agenda of the {UNGA}|a conf[^.]+? convened [^\n]+\n*[^\n]*?the (?:{UNSG}|Depositary(?=.+?{UNSG}))|the {UNSG} shall convene a conference|request the {UNSG}[^\n]+ to convene a \w* ?conference|conference under the auspices of the {UN}| the budget of the Org\w+ by the {UNGA}|inform the launching authority and the {UNSG}|agree to inform the {UNSG}|bring the issue[^.]+to the attention of the {UN}|International Law Commission[^\n]+\n+\w+ \w+ \w+ {UNGA} resolution[^\n]+\n+Have agreed|In conformity with res\w+ of the {UNGA} \w+ for the conclusion|Hereby establish the \w+ \w+ \w+ Org\w+ of the {UN}|shall keep UNCTAD \w+ informed of its activities|the {UN} Conference[^\n]+ informed of its activities|report\w* on its activities \w+ to the {UNGA}|this \w+ at the {UN} Conference on the establishment|Secretariat of UNIDO for the [^\n]+ establish|hereby establish[^\n]+ as a specialized agency[^\n]+{UN}|verification[^\n]+?within the framework of the {UN}|lodge a complaint with the {UNSC}|violations[^\n]+with the {UN})[^\n]*)"
new = textseries.str.extract(OrgAusp).dropna()
new['orgAuspices'] = 'UN'
textdf.update(new, overwrite=False)

# Inherited orgAuspices (by supplementary agreements)
new = textdf.loc[textdf.supplAgTo.notna(), ['treatyLabel','supplAgTo','orgAuspices']]
new['orgAuspices'] = new.supplAgTo.apply(lambda x: textdf.orgAuspices[textdf.treatyLabel == x].values[0])
textdf.update(new, overwrite=False)
# run lookup again to propagate results of first lookup (for supplementary protocols)
new['orgAuspices'] = new.supplAgTo.apply(lambda x: textdf.orgAuspices[textdf.treatyLabel == x].values[0])
textdf.update(new, overwrite=False)

# Filling missing values with weaker UN pattern
OrgAusp = r"(?i)\n.+(?P<orgAuspStr>open(?: for signature)?[^.]*? at \w* ?United Nations Headquarters)"
new = textseries[textdf.orgAuspices.isna()].str.extract(OrgAusp).dropna()
new['orgAuspices'] = 'UN'
textdf.update(new, overwrite=False)


### Conference of the parties (COP)

# P1 (named arts) [47 T]
COP = r"(?is)\n(?P<copProvStr>(?:Article \w+\n\n)?[^\n]*(?:(?:(?<!Review )Conference|(?<!Preparatory )Meeting[s]?(?=(?:[^\n]|\n+[^\n]+convene a meeting of the \w+ \w* ?Parties))|Assembly)(?: of \w+ \w* ?Parties(?: to th\w+ \w+)?)?|Congress|(?:Composition|Constitution|Governing) (?:of the [^\n]*)?Council|Board of Governors|Governing Body|Food Assistance Committee)\n\n(?!Adopts).+?)" + eos
new = textseries.str.extract(COP).dropna()
textdf.update(new, overwrite=True)

# P2 [2 T]
COP = r"(?is)(?<!(?:Assembly|Congress))\n\n(?P<copProvStr>Article \w+\n+[^\n]*shall serve as the (?P<copName>\w+ of the Parties) to this.+?)" + eos
new = textseries.str.extract(COP).dropna()
new.copName = new.copName.str[0].str.upper() + new.copName.str[1:]
textdf.update(new, overwrite=True)

# P3 [3 T]
COP = r"(?is)(?<!(?:Assembly|Congress))\n\n(?P<copProvStr>Article \w+(?: +\w+)*\n\n[^\n]*(?:The|a) (?P<copName>(?:Ministerial )?\w+)(?: shall)? (?:consist|(?:be )?composed) of(?: all)?(?: the)? representatives of(?: all)? the member[s]?.+?)(?=\n\n(?:Article|Chapter|Part|\d[.]\tThere shall be a \w+ \w+ composed of))"
new = textseries.str.extract(COP).dropna()
textdf.update(new, overwrite=True)

# P4 [1 T]
COP = r"(?is)(?<!(?:Assembly|Congress))\n\n(?P<copProvStr>Article \w+(?: +\w+)*\n+[^\n]*The \w+ \w* ?shall(?:, as the necessity arises,)? convene (?P<copName>\w+) on .+?\sThe representatives of the \w* ?parties at such \w+ should include .+?)" + eos
new = textseries.str.extract(COP).dropna()
textdf.update(new, overwrite=True)

# P5 [27 T]
COP = r"(?i)\n(?P<copProvStr>.+(?:placing on the agenda of the|regular or special session of the|(?:request the \w+ to|shall) (?:convene|call) a|a(?= Conf\w+)) (?P<copName>\w+(?: of(?: the)? \w* ?Parties)?) (?:the question of its revision|for approval|to review and assess|with a view to assessing|.+to review the scope and operation|.+to decide on org\w+ matters|.*?shall be (?:held|convened) .+ review).+)"
new = textseries.str.extract(COP).dropna()
new.copName = new.copName.str[0].str.upper() + new.copName.str[1:]
textdf.update(new, overwrite=False)

# P6 (unnamed art/para, no COP name extracted) [9 T]
COP = f"(?i)\n(?P<copProvStr>(?:Article \w+\n+)?.*(?:This \w+ may be revised by the General Conference|Health Assembly shall have authority to|establish.+ Commission.+ composed of one member from each|review.+shall be included in the \w* ?agenda of the {UNGA}|submitted to the {UNGA} for approval|report to the Health Assembly on the implement|meeting of the states parties.+general assembly).+(?:\n+.+(?:members|voting|majority).+)*)"
new = textseries.str.extract(COP).dropna()
textdf.update(new, overwrite=False)

# update binary var
textdf.loc[textdf.copProvStr.notna(), 'copProv'] = True


### Compulsory dispute settlement
# opt-in compDispSett
pattern = r"(?i)(?:declare \w* ?(?:writing|a written \w+)?(?: \w* ?to the (?:Depositary|\w+.General))? ?that[^\n]* it (?:(?:accepts|recogni.es) .+ (?:means of dispute sett\w+|arbitration) as compulsory|\w+,? as compulsory[^.]+ (?:dispute|court of justice|arbitr\w+)))"
textdf.loc[textdf.disputeProvStr.notna() & textdf.disputeProvStr.str.contains(pattern), 'compDispSett'] = 'opt-in'

# opt-out compDispSett (in same art)
pattern = r"(?i)(?:shall,? at the request of (?:one|any)(?: of the.?)?(?: part\w+ to \w+ dispute)?, be (?:referred|submitted) to.+\n*.+ may(?:,? at the time of sig.+)? declare that it does not consider itself bound by)"
textdf.loc[textdf.disputeProvStr.notna() & textseries.str.contains(pattern), 'compDispSett'] = 'opt-out'

# opt-out compDispSett (in separate arts)
pattern = r"(?i)(?:shall,? at the request of (?:one|any)(?: of the.?)?(?: part\w+ to \w+ dispute)?, be (?:referred|submitted) to|be referred, at the request of any of the \w* ?Parties \w+, to the International Court of Justice)"
new = textdf.loc[textdf.disputeProvStr.notna() & textdf.disputeProvStr.str.contains(pattern), ['treatyLabel','compDispSett','disputeProvStr']]
new['artNb'] = new.disputeProvStr.str.extract('(?s)^Article (\w+)')
new['optout'] = textseries[new.index].str.extract(f"declare that it does not consider itself bound by Article (\w+)")
new.loc[new.artNb == new.optout, 'compDispSett'] = 'opt-out'
new = new[new.optout.notna()]
textdf.update(new, overwrite=True)

# compulsory (no opt-out other than treaty withdrawal)
pattern = r"(?i)(?:shall,? at the request of one of the parties to the dispute, be referred|be submitted,? \w+ \w* ?request of any \w* ?(?:them|\w* ?Part\w+) ?(?:to the dispute|concerned[^.]*)?,? to \w* ?(?:arbitration|court)|shall be (?:referred to (?:an independent arb|the Int\w+ Court of Justice)|settled.+by means of the Arbitral).+unless the parties|Part XV of the[^.]* Convention(?: on the Law of the Sea.+)? apply mutatis mutandis[^.]*, whether or not (?:the States|they are also Parties)|either Party may request the President of the International Court.+final and binding|right of recourse in the final instance to the International Court|have recourse to the DSU)"
new = textdf.loc[textdf.disputeProvStr.notna() & textseries.str.contains(pattern), ['treatyLabel','compDispSett','disputeProvStr']]
new['compDispSett'] = 'comp'
textdf.update(new, overwrite=True)

# inherited values
pattern = r"(?im)(?P<disputeProvStr>(?:^Article \w+\n+.+ on settlement of disputes shall apply mutatis mutandis|shall be separate from, and without prejudice to, the dispute settlement procedures and mechanisms under Article \d+ of the Convention).*)"
new = textseries.str.extract(pattern).dropna()
new['disputeProv'] = True
new['supplAgTo'] = textdf.supplAgTo[new.index]
new['compDispSett'] = new.supplAgTo.apply(lambda x: textdf.compDispSett[textdf.treatyLabel == x].values[0])
textdf.update(new, overwrite=False)


### Treaty provisions wrap-up

# generic inherited values
pattern = r"(?i)(?:the provisions of the Convention .{0,30} shall apply(?:,? mut\w+ mut\w+,?)? to th\w+ \w* ?Protocol)"
new = textdf.loc[textseries.str.contains(pattern), ['treatyLabel','supplAgTo']]
cols = [c for c in provcolsdisp if c != 'verifProv']
for col in cols:
  new[col] = new.supplAgTo.apply(lambda x: textdf[col][textdf.treatyLabel == x].values[0])
textdf.update(new, overwrite=False)
# run again for supplementary protocols
for col in cols:
  new[col] = new.supplAgTo.apply(lambda x: textdf[col][textdf.treatyLabel == x].values[0])
textdf.update(new, overwrite=False)

# Fill missing values and set data types
textdf.loc[textdf.EIFprovStr.notna(), 'EIFprov'] = True
textdf.loc[textdf.simplAmendProv==True, 'amendProv'] = True
textdf.compDispSett.fillna(value='No', inplace=True)
for col in provdfdisp.vars[provdfdisp.type=='bool'].values:
  textdf[col].fillna(value=False, inplace=True)
  textdf[col] = textdf[col].astype(bool)
for col in provdf.index[provdf.type.isin(['int','float'])].values:
  textdf[col] = textdf[col].astype(float)


##### Treaty subject matter #####

for target in smdf.index.tolist():
  try:
    textdf[target + 'N'] = textseries.str.count(smdf.regexp[target])
  except:
    pattern = regex.compile(smdf.regexp[target])
    textdf[target + 'N'] = textseries.apply(lambda text: len(regex.findall(pattern, text)))
  textdf[target + 'F'] = textdf[target + 'N'] / textdf.wordcount
  textdf[target + 'P'] = round(textdf[target + 'F'] * 10000, 1)
  textdf[target + 'B'] = textdf[target + 'N'].apply(lambda x:0 if x == 0 else 1)

# adjust landDegrad variable (add counts of subcategories)
textdf.landDegradN = textdf.landDegradN + textdf.deforestN + textdf.desertifN
textdf.landDegradF = textdf.landDegradN / textdf.wordcount
textdf.landDegradP = round(textdf.landDegradF * 10000, 1)
textdf.landDegradB = textdf.landDegradN.apply(lambda x:0 if x == 0 else 1)

# add number of characters in regex
smdf['regexpLen'] = smdf.regexp.str.len()
# add number of treaties with a match
sums = textdf[smdf.index + 'B'].sum().T
sums.index = sums.index.str[:-1]
smdf['nTreaties'] = sums
# add number of matches per regex
sums = textdf[smdf.index + 'N'].sum().T
sums.index = sums.index.str[:-1]
smdf['nMentions'] = sums


##### Results #####

# drop incomparable variables (due to data type issues)
freqs = smdf.index+'F'
drop = freqs.tolist() + ['treatyLangLists','nLangs','incubDays']
print('Successfully reproduced textdf?:', textdf.drop(columns=drop).equals(textdf0.drop(columns=drop)))
elapsed = str(datetime.now() - starttime).split(".")[0]
print(f'Time elapsed: {elapsed}.')

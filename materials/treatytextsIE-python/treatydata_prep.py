import numpy as np
import pandas as pd
from titlecase import titlecase

def titlelow(word, **kwargs):
  if word.lower() in 'from with their that those into regard concerning relating including against':
    return word.lower()
  
# load manually edited dataset
tm = pd.read_csv('../treaty-selection/IEA_Dataset_Agreements_332_MK.csv', encoding='utf-8').dropna(how='all')
cols = ['ieaid', 'fn','SignatureDate', 'Entryintoforce(EIF)Date', 'TreatyName',
        'shortTitle', 'treatyLabel', 'UNTStreatyRecordURL', 'ILMref', 'OrgRef']
tm = tm.loc[tm.selected==True, cols]

# load UNTS data output from java pipeline
untsmeta = pd.read_csv('../java-pipeline/UNTSmeta.csv', encoding='utf-8')
untsmeta.treaty = untsmeta.treaty.str.split('#').str[1]
untsmeta['treatyTitleUNTS'] = untsmeta.treatyTitle

# generate treatymeta.csv
treatymeta = tm.merge(untsmeta, how='left', on='UNTStreatyRecordURL')
# fill missing dates with IEAdb data
treatymeta.treatyAdoptionDate.fillna(treatymeta.SignatureDate, inplace=True)
treatymeta.treatyEIFdate.fillna(treatymeta['Entryintoforce(EIF)Date'], inplace=True)
# special case of provisional EIF
treatymeta.loc[treatymeta.treatyLabel=='CocoaAg93', 'treatyEIFdate'] = '1994-02-22'
# recalculate incubDays
treatymeta['incubDays'] = pd.to_datetime(treatymeta.treatyEIFdate) - pd.to_datetime(treatymeta.treatyAdoptionDate)
treatymeta.incubDays = treatymeta.incubDays.dt.days
# extract treatyAdoptionYear
treatymeta['treatyAdoptionYear'] = treatymeta.treatyAdoptionDate.str[:4]
# add nLangs
treatymeta['treatyLangLists'] = treatymeta.treatyLangs.str.split('\t').str[:-1]
treatymeta['nLangs'] = treatymeta.treatyLangLists.apply(lambda x: (len(x) if x is not np.nan else np.nan)).astype(float)
# streamline treatyTitle
treatymeta.treatyTitle.fillna(treatymeta.TreatyName, inplace=True)
treatymeta.treatyTitle = treatymeta.treatyTitle.str.strip(' .*').str.replace('  ',' ',regex=False)
treatymeta.treatyTitle = treatymeta.treatyTitle.str.replace(' ?[(](?!Prot)[^(]+[)]$','',regex=True)
treatymeta.loc[~treatymeta.treatyLabel.str.contains('\d', regex=True)
               & treatymeta.treatyTitle.str.contains(', \d{4}$', regex=True),
               'treatyTitle'] = treatymeta.treatyTitle.str.replace(', \d{4}$','', regex=True)
treatymeta.loc[treatymeta.treatyLabel.str.contains('\d')
               & ~treatymeta.treatyTitle.str.contains(', \d{4}$')
               & ~treatymeta.treatyTitle.str.startswith('Protocol'),
               'treatyTitle'] = treatymeta.treatyTitle + ', ' + treatymeta.treatyAdoptionYear
treatymeta.loc[treatymeta.treatyLabel.str.contains('\d') & ~treatymeta.treatyTitle.str.contains(', \d{4}$') & treatymeta.treatyTitle.str.startswith('Protocol'),
               'treatyTitle'] = treatymeta.treatyAdoptionYear + ' ' + treatymeta.treatyTitle
treatymeta.treatyTitle = treatymeta.treatyTitle.str.replace('(?<=\d) and relating', ', and relating', regex=True)
treatymeta.treatyTitle = treatymeta.treatyTitle.apply(lambda x: titlecase(x, callback=titlelow))

# more cols for treaty references
treatymeta.loc[treatymeta.fn.str.startswith('treaties-ATS'),
               'ATSref'] = treatymeta.fn.str.extract('treaties-(ATS-\d+-\d+)[.]txt', expand=False)\
                                        .str.replace('-',' ',regex=False)
treatymeta['shortTitleBib'] = treatymeta.shortTitle.str.replace('Conv.','Convention',regex=False)\
                                                   .str.replace('Prot.','Protocol',regex=False)\
                                                   .str.replace('Agmt','Agreement',regex=False)
treatymeta['UNTSvolNbBib'] = treatymeta.UNTSvolRef.str.extract('(\d+)',expand=False)
treatymeta['UNTSvolPageBib'] = treatymeta.UNTSvolRef.str.extract('\d+ .p.(\d+)',expand=False)
# update wrongly extracted page nb
treatymeta.loc[treatymeta.treatyLabel=='SOLAS74', 'UNTSvolPageBib'] = '278'
# supplement ATS refs
treatymeta.loc[treatymeta.treatyLabel == 'AntiFoulSysConv', 'ATSref'] = 'ATS 2008 15'
treatymeta.loc[treatymeta.treatyLabel == 'ParisAgreement', 'ATSref'] = 'ATS 2016 24'
# reorder columns and rows
treatymeta = treatymeta.sort_values(by=['treatyAdoptionDate','treatyTitle']).reset_index(drop=True)
treatymeta = treatymeta.loc[:,['fn', 'treatyLabel', 'shortTitle', 'shortTitleBib', 'treatyTitle', 'treatyTitleUNTS', 
                               'treatyAdoptionYear','treatyAdoptionDate','treatyAdoptionPlace',
                               'treatyEIFdate', 'treatyEIFprovInfo', 'incubDays',
                               'ieaid', 'TreatyName', 'SignatureDate', 'Entryintoforce(EIF)Date',
                               'treaty', 'UNTStreatyRecordURL', 'UNTStrLastR', 'treatyRegDate', 'treatyDepositary',
                               'treatySubjectTerms', 'treatyLangs', 'treatyLangLists', 'nLangs',
                               'UNTSvolRef', 'UNTSvolNbBib', 'UNTSvolPageBib','ATSref','OrgRef','ILMref']]
# save to file
treatymeta.to_csv('treatymeta.csv', encoding='utf-8', index=False)

# generate treatyrefs.csv
treatyrefs = treatymeta.loc[treatymeta.UNTSvolPageBib.notna()
                            | treatymeta.ATSref.notna()
                            | treatymeta.OrgRef.notna()
                            | treatymeta.ILMref.notna(),
                            ['treatyLabel','shortTitleBib','treatyTitle','treatyTitleUNTS',
                             'treatyAdoptionDate','treatyEIFdate',
                             'UNTSvolRef','UNTStreatyRecordURL','UNTStrLastR',
                             'ATSref','OrgRef','ILMref']]

treatyrefs.rename(columns={'UNTStrLastR':'UNTStrLastRetrieved'}, inplace=True)
treatyrefs.UNTStrLastRetrieved = treatyrefs.UNTStrLastRetrieved.str.split(' ').str[0].fillna('')
treatyrefs.to_csv('../treaty-references/treatyrefs.csv', encoding='utf-8', index=False)

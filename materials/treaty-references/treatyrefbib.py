import numpy as np
import pandas as pd

treatyrefs = pd.read_csv('treatyrefs.csv', encoding='utf-8').dropna(how='all')
treatyrefs.treatyEIFdate.fillna('NaT', inplace=True)

treatyrefs['UNTSvol'] = treatyrefs.UNTSvolRef.str.extract('(\d+)', expand=False)
treatyrefs['UNTSpage'] = treatyrefs.UNTSvolRef.str.extract('\d+ .p.(\d+)', expand=False)
# update wrongly extracted page nb
treatyrefs.loc[treatyrefs.treatyLabel=='SOLAS74', 'UNTSpage'] = '278'

treatyrefs['ILMvol'] = treatyrefs.ILMref.str.extract(' (\d+) ILM', expand=False)
treatyrefs['ILMpage'] = treatyrefs.ILMref.str.extract('ILM (\d+)', expand=False)

# escape ampersand
treatyrefs.shortTitleBib = treatyrefs.shortTitleBib.str.replace('&','\&', regex=False)


entries = ''
# UNTS
for i in treatyrefs[treatyrefs.UNTSpage.notna()].index:
  entry = '@legal{'+treatyrefs.treatyLabel[i]+''',
  entrysubtype = {piltreaty},
  title        = {'''+treatyrefs.treatyTitle[i]+'''},
  execution    = {adopted='''+treatyrefs.treatyAdoptionDate[i]+' and inforce='+treatyrefs.treatyEIFdate[i]+'''},
  pagination   = {article},
  reporter     = {U.N.T.S.},
  volume       = {'''+treatyrefs.UNTSvol[i]+'''},
  pages        = {'''+treatyrefs.UNTSpage[i]+'''},
  note         = {},
  shorttitle   = {'''+treatyrefs.shortTitleBib[i]+'''},
}

'''
  entries += entry

# ATS
for i in treatyrefs[treatyrefs.UNTSpage.isna() & treatyrefs.ATSref.notna()].index:
  entry = '@legal{'+treatyrefs.treatyLabel[i]+''',
  entrysubtype = {piltreaty},
  title        = {'''+treatyrefs.treatyTitle[i]+'''},
  execution    = {adopted='''+treatyrefs.treatyAdoptionDate[i]+' and inforce='+treatyrefs.treatyEIFdate[i]+'''},
  pagination   = {article},
  reporter     = {A.T.S.},
  series       = {'''+treatyrefs.ATSref[i][4:8]+'''},
  pages        = {'''+treatyrefs.ATSref[i][9:]+'''},
  note         = {},
  shorttitle   = {'''+treatyrefs.shortTitleBib[i]+'''},
}

'''
  entries += entry

# Org
for i in treatyrefs[treatyrefs.UNTSpage.isna() & treatyrefs.ATSref.isna() & treatyrefs.OrgRef.notna()].index:
  entry = '@legal{'+treatyrefs.treatyLabel[i]+''',
  entrysubtype = {piltreaty},
  title        = {'''+treatyrefs.treatyTitle[i]+'''},
  execution    = {adopted='''+treatyrefs.treatyAdoptionDate[i]+' and inforce='+treatyrefs.treatyEIFdate[i]+'''},
  pagination   = {article},
  series       = {'''+treatyrefs.OrgRef[i]+'''},
  note         = {},
  shorttitle   = {'''+treatyrefs.shortTitleBib[i]+'''},
}

'''
  entries += entry

# ILM
for i in treatyrefs[treatyrefs.UNTSpage.isna() & treatyrefs.ATSref.isna() & treatyrefs.OrgRef.isna() & treatyrefs.ILMref.notna()].index:
  entry = '@legal{'+treatyrefs.treatyLabel[i]+''',
  entrysubtype = {piltreaty},
  title        = {'''+treatyrefs.treatyTitle[i]+'''},
  execution    = {adopted='''+treatyrefs.treatyAdoptionDate[i]+' and inforce='+treatyrefs.treatyEIFdate[i]+'''},
  pagination   = {article},
  journaltitle = {I.L.M.},
  year         = {'''+treatyrefs.ILMref[i][1:5]+'''},
  volume       = {'''+treatyrefs.ILMvol[i]+'''},
  pages        = {'''+treatyrefs.ILMpage[i]+'''},
  note         = {},
  shorttitle   = {'''+treatyrefs.shortTitleBib[i]+'''},
}

'''
  entries += entry  

entries = entries.replace(' and inforce=NaT','')
  
with open('treatyrefs.bib', 'w', encoding='utf-8') as f:
  f.write(entries)

print(f'Generated {len(treatyrefs)} treaty entries for use with oscola-biblatex and saved to treatyrefs.bib.')

FROM docker.io/debian:11.7-slim

WORKDIR /usr/src/phd

RUN apt-get update && apt-get install -y --no-install-recommends \
    biber \
    comparepdf \
    emacs \
    fonts-dejavu-core \
    fonts-liberation2 \
    inkscape \
    latexmk \
    lmodern \
    openjdk-11-jdk \
    p7zip \
    python3.9 \
    python3-matplotlib \
    python3-pip \
    texlive-bibtex-extra \
    texlive-latex-extra \
    texlive-plain-generic \
    wkhtmltopdf \
&& rm -rf /var/lib/apt/lists/*

COPY . .

# Local font install (to avoid installing texlive-fonts-extra)
RUN mv ./fonts/bera/* $(kpsewhich --var-value TEXMFLOCAL) \
&& texhash && updmap-sys --enable Map=bera.map

RUN pip3 install --no-cache-dir -r requirements.txt

CMD ["/bin/bash"]

##### Usage instructions #####
# Build the image with
# (substitute podman with docker if preferred)
# > podman build --tag phd .
# Run a container interactively with
# > podman run -it phd
# Run replication script with
# > bash replication.sh

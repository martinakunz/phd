#!/bin/bash

echo 'Starting replication...'
echo '-------------------------------'

echo '1. Treaty selection:'
cd materials/treaty-selection
python3 treaty_selection.py
python3 generate_treaty_urls.py
echo '-------------------------------'

echo '2. Cleaning treaty texts:'
cd ../data/treatytexts
python3 treaty_cleaner.py
echo '-------------------------------'

echo '3. Processing UNTS metadata:'
cd ../../java-pipeline
cp UNTSmeta.csv UNTSmeta_backup.csv
export JENA_HOME=../../java/apache-jena-3.4.0
$JENA_HOME/bin/sparql --data=pilo_UNTS.ttl --query=UNTSmeta.rq --results=CSV > UNTSmeta.csv
diff -s UNTSmeta.csv UNTSmeta_backup.csv
echo 'Deleting UNTSmeta_backup.csv...'
rm UNTSmeta_backup.csv
echo '-------------------------------'

echo '4. Treaty texts IE with Python:'
cd ../treatytextsIE-python
python3 treatydata_prep.py
python3 treatytextIE_minimal.py
echo '-------------------------------'

echo '5. Generating treaty references:'
cd ../treaty-references
python3 treatyrefbib.py
python3 treatyreflist.py
echo '-------------------------------'

echo '6. Generating treaty profiles:'
cd ../..
emacs --batch -l ./emacs/init.el --visit ./materials/treaty-profiles/treaty-profile.org -f org-latex-export-to-pdf
echo '-------------------------------'

echo 'Skipping treaty EIF predictions to save time...'
echo '-------------------------------'

echo '7. Reproducing thesis figures, tables and final PDF:'
cp ./thesis/thesis.pdf ./thesis/thesis_backup.pdf
emacs --batch -l ./emacs/init.el --visit ./thesis/thesis.org -f org-latex-export-to-pdf
comparepdf -v=2 -c=a ./thesis/thesis.pdf ./thesis/thesis_backup.pdf
rm ./thesis/thesis_backup.pdf
echo '-------------------------------'

echo 'Replication completed.'
